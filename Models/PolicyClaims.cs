﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class PolicyClaims
    {
        public JObject PolicyData = new JObject();
        public string PolicyID { set; get; }
    }
}