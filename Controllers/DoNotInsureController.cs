﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class DoNotInsureController : Controller
    {
        // GET: DoNotInsure
        public ActionResult Index()
        {
            DoNotInsure DontInsure = new DoNotInsure();
            return View(DontInsure);
        }


        [HttpPost]
        public ActionResult Index(DoNotInsure DontInsure)
        {
            string UnderWriterResponse = KeyManagement.PerformRequest("epic_mwVehDoNotInsure",
            KeyManagement.RequestType.Parameter, "All", DontInsure);
            JObject MyResponse = JObject.Parse(UnderWriterResponse);

            if (MyResponse["do_not_insure"] != null)
            {
                DontInsure.do_not_insure = (bool)(MyResponse["do_not_insure"]);
                DontInsure.success = (bool)(MyResponse["success"]);
            }

            return RedirectToAction("IndexResult", DontInsure);

       }

        public ActionResult IndexResult(DoNotInsure DontInsure)
        {
            return View(DontInsure);
        }

    }
}
