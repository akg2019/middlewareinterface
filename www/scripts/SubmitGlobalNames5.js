﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

var ReqKey;
var LoggedInUsername, LoggedInPassword;
var ClearFixes = "<div class=\"ClearFixes\"></div>";


    //Retrieves a Global Name
    $(document).on("click", ".GetGlobalName", function () {
        
        //if (document.getElementById("global_name_number").value == "") {
            document.getElementById("ResultSet").innerHTML = LoadANimation;
            GrabGlobalData();
            window.location.href = "#"
        //} else { alert("Invalid Global ID."); };
    })

   
    // Get Data from form controls
    function GrabData() {

        //Global Names Collection

        var global_name_number = document.getElementById("global_name_number").value;
        var company_name = document.getElementById("company_name").value;
        var first_name = document.getElementById("first_name").value;
        var maiden_name = document.getElementById("maiden_name").value;
        var middle_name = document.getElementById("middle_name").value;
        var last_name = document.getElementById("last_name").value;
        var dob = document.getElementById("dob").value;
        var drivers_licence_country = document.getElementById("drivers_licence_country").value;
        var drivers_licence_date_issued = document.getElementById("drivers_licence_date_issued").value;
        var drivers_licence_first_issued = document.getElementById("drivers_licence_first_issued").value;
        var drivers_licence_number = document.getElementById("drivers_licence_number").value;
        var email_address = document.getElementById("email_address").value;
        var employment_type = document.getElementById("employment_type").value;
        var gender = document.getElementById("gender").value;
        var is_a_company = document.getElementById("is_a_company").value;
        var is_a_service_provider = document.getElementById("is_a_service_provider").value;
        var mailing_name = document.getElementById("mailing_name").value;
        var marital_status = document.getElementById("marital_status").value;
        var national_id = document.getElementById("national_id").value;
        var national_id_type = document.getElementById("national_id_type").value;
        var nationality = document.getElementById("nationality").value;
        var notes = document.getElementById("notes").value;
        var occupation = document.getElementById("occupation").value;
        var place_of_birth = document.getElementById("place_of_birth").value;
        var salutation_name = document.getElementById("salutation_name").value;
        var service_type = document.getElementById("service_type").value;
        var tax_id_number = document.getElementById("tax_id_number").value;
        var title = document.getElementById("title").value;

        //Phone Numbers
        var type = document.getElementById("type").value;
        var carrier = document.getElementById("carrier").value;
        var description = document.getElementById("description").value;
        var extension_or_range = document.getElementById("extension_or_range").value;
        var phone_number = document.getElementById("phone_number").value;

        //Address/Location

        var is_mailing_address = document.getElementById("is_mailing_address").value;
        var is_main_location = document.getElementById("is_main_location").value;
        var aaltitude = document.getElementById("aaltitude").value;
        var ablock = document.getElementById("ablock").value;
        var abuilding_number = document.getElementById("abuilding_number").value;
        var acomplex_name = document.getElementById("acomplex_name").value;
        var acountry = document.getElementById("acountry").value;
        var ageneral_area = document.getElementById("ageneral_area").value;
        var ainternational_area = document.getElementById("ainternational_area").value;
        var ais_international_address = document.getElementById("ais_international_address").value;
        var aisland = document.getElementById("aisland").value;
        var alatitude = document.getElementById("alatitude").value;
        var alongitude = document.getElementById("alongitude").value;
        var aparish = document.getElementById("aparish").value;
        var apostal_zip_code = document.getElementById("apostal_zip_code").value;
        var astreet_name = document.getElementById("astreet_name").value;
        var astreet_number = document.getElementById("astreet_number").value;
        var astreet_type = document.getElementById("astreet_type").value;
        var atown = document.getElementById("atown").value;
        var aunit_number = document.getElementById("aunit_number").value;

        //Contact info at location

        var typeLoc = document.getElementById("typeLoc").value;
        var carrierLoc = document.getElementById("carrierLoc").value;
        var descriptionLoc = document.getElementById("descriptionLoc").value;
        var extension_or_rangeLoc = document.getElementById("extension_or_rangeLoc").value;
        var phone_numberLoc = document.getElementById("phone_numberLoc").value;

        //Global Names JSON
        var GlobalNamesSUbmitJson =
                     {
                         "resquestkey": { "key": ReqKey },
                         "global_name_number": global_name_number,
                         "company_name": company_name,
                         "dob": dob,
                         "drivers_licence_country": drivers_licence_country,
                         "drivers_licence_date_issued": drivers_licence_date_issued,
                         "drivers_licence_first_issued": drivers_licence_first_issued,
                         "drivers_licence_number": drivers_licence_number,
                         "email_address": email_address,
                         "employment_type": employment_type,
                         "first_name": first_name,
                         "gender": gender,
                         "is_a_company": is_a_company,
                         "is_a_service_provider": is_a_service_provider,
                         "last_name": last_name,
                         "locations": [
                             {
                                 "is_main_location": is_main_location,
                                 "is_mailing_address": is_mailing_address,
                                 "altitude": alatitude,
                                 "block": ablock,
                                 "building_number": abuilding_number,
                                 "country": acountry,
                                 "general_area": ageneral_area,
                                 "international_area": ainternational_area,
                                 "is_international_address": ais_international_address,
                                 "island": aisland,
                                 "latitude": alatitude,
                                 "longitude": alongitude,
                                 "parish": aparish,
                                 "postal_zip_code": apostal_zip_code,
                                 "street_name": astreet_name,
                                 "street_number": astreet_number,
                                 "street_type": astreet_type,
                                 "town": atown,
                                 "unit_number": aunit_number,
                                 "phone_numbers": [
                                     {
                                         "type": type,
                                         "carrier": carrier,
                                         "description": description,
                                         "extension_or_range": extension_or_range,
                                         "phone_number": phone_number
                                     }
                                 ]
                             }
                         ],
                         "maiden_name": maiden_name,
                         "mailing_name": mailing_name,
                         "marital_status": marital_status,
                         "middle_name": middle_name,
                         "national_id": national_id,
                         "national_id_type": national_id_type,
                         "nationality": nationality,
                         "notes": notes,
                         "occupation": occupation,
                         "place_of_birth": place_of_birth,
                         "salutation_name": salutation_name,
                         "service_type": service_type,
                         "tax_id_number": tax_id_number,
                         "title": title,
                         "phone_numbers": [
                             {
                                 "type": typeLoc,
                                 "carrier": carrierLoc,
                                 "description": descriptionLoc,
                                 "extension_or_range": extension_or_rangeLoc,
                                 "phone_number": phone_numberLoc
                             }
                         ]
                     };

        SubmitGlobalNames(JSON.stringify(GlobalNamesSUbmitJson));
    };




    //Loading Animation
    var LoadANimation = "<div class=\"cssload-dots\">"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                    + "</div>"
                    + "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
                      + "<defs>"
                          + "<filter id=\"goo\">"
                              + "<feGaussianBlur in=\"SourceGraphic\" result=\"blur\" stdDeviation=\"12\" ></feGaussianBlur>"
                              + "<feColorMatrix in=\"blur\" mode=\"matrix\" values=\"1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7\" result=\"goo\" ></feColorMatrix>"
                              + "<!--<feBlend in2=\"goo\" in=\"SourceGraphic\" result=\"mix\" ></feBlend>-->"
                          + "</filter>"
                      + "</defs>"
                    + "</svg>"
                    + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>";


    //Submit Global Names
    function SubmitGlobalNames(GlobalNames) {
        $.ajax({
            type: "POST",
            url: "http://localhost:55170/RequestQuote/epic_mwGlobalNameSubmit",
            data: GlobalNames,
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessGlobalNames,
            error: ErrorGlobalNames
        });
    }

    function SuccessGlobalNames(response) {
        //Global Name Number
        if (response.success == false) {
            var GlobalNameResponse = "<p class=\"VaribaleLables\">Global Name Number: </p><p>" + response.error_message + "</p>"
                                 + "<p class=\"VaribaleLables\">Successful Submission: </p><p>" + response.success + "</p>";
            document.getElementById("ResultSet").innerHTML = ClearFixes + "<div class=\"alert alert-danger\" role=\"alert\">" + GlobalNameResponse + "</div>";
        } else {
            //Write relevant response

        };
    };

    function ErrorGlobalNames(response) {
        document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">Failed to connect to UnderWriter (:<p>" + response.responseText + "</p></div>";
    };



    //Get Global Names Data
    function GrabGlobalData() {
        var global_name_number = document.getElementById("global_name_number").value;
        var GlobalNamesParam = { "resquestkey": { "key": ReqKey }, "globalNameNumbers": { globalNameNumber: global_name_number } };

        GlobalNamesParam = { GlobalNameNumber: global_name_number };
        GetGlobalNames(JSON.stringify(GlobalNamesParam));

    };

    //Switch false and true to yes/no
    function SwitchBoolean(BooleanValue) {
        var ReturnItem = "";
        if (BooleanValue == true) { ReturnItem = "Yes" } else
        if (BooleanValue == false) { ReturnItem = "No" } else
        if (BooleanValue = undefined) { ReturnItem = "No" };

        return ReturnItem
    }


    //GetGlobalNames Global Names
    function GetGlobalNames(GlobalNameNumber) {
        $.ajax({
            type: "POST",
            url: "AllMethods/GlobalNameSearch",
            data: GlobalNameNumber,
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessGetGlobalNames,
            error: ErrorGetGlobalNames
        });
    }

    function SuccessGetGlobalNames(response) {

        if (response.success == true) {
            //Global Name Number

            var SeparatorTitle = "<div class=\"panel panel-warning\"><div class=\"panel-heading\">#title#</div><div class=\"panel-body\">";
            var ResultsPrefix = "<div class=\"ClearFix\"></div>"
                + "<div class=\"alert alert-success\" role=\"alert\"><h5>Your result</h5></div>";

            var MainDocument, Location, PhoneNumber, LocationPhoneNumbers;
            MainDocument = "";
            Location = "";
            PhoneNumber = "";
            LocationPhoneNumbers = "";


            if (response.global_name.locations.length > 0) {
                Location = SeparatorTitle.replace("#title#", "Address") + "<table><tr class = \"TablePadding\"><td class = \"TablePadding\">Altitude </td><td class = \"MainCaption\">" + response.global_name.locations[0].altitude + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Block </td><td>" + response.global_name.locations[0].block + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Building# </td><td>" + response.global_name.locations[0].building_number + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Country </td><td>" + response.global_name.locations[0].country + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">General Area </td><td>" + response.global_name.locations[0].general_area + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">International Area </td><td>" + response.global_name.locations[0].international_area + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Is International Area </td><td class = \"MainCaption\"> " + SwitchBoolean(response.global_name.locations[0].is_international_address) + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Is Mailing Address </td><td class = \"MainCaption\">" + SwitchBoolean(response.global_name.locations[0].is_mailing_address) + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Is Main Location </td><td class = \"MainCaption\">" + SwitchBoolean(response.global_name.locations[0].is_main_location) + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Island </td><td>" + response.global_name.locations[0].island + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Latitude </td><td>" + response.global_name.locations[0].latitude + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Longitude </td><td>" + response.global_name.locations[0].longitude + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Parish </td><td>" + response.global_name.locations[0].parish + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Postal/Zip Code </td><td>" + response.global_name.locations[0].postal_zip_code + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Street Name </td><td>" + response.global_name.locations[0].street_name + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Street# </td><td>" + response.global_name.locations[0].street_number + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Street Type </td><td>" + response.global_name.locations[0].street_type + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Town </td><td>" + response.global_name.locations[0].town + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Unit# </td><td>" + response.global_name.locations[0].unit_number + "</td></tr></tr></table></div></div>"
            };
            //Phone at this location
            if (response.global_name.locations.length > 0) {
                if (response.global_name.locations[0].phone_numbers.length > 0) {
                    for (var i = 0; i < response.global_name.locations[0].phone_numbers.length; i++) {
                    LocationPhoneNumbers += SeparatorTitle.replace("#title#", "Contact information at this location") + "<table><tr><td>Type </td><td>" + response.global_name.locations[0].phone_numbers[0].type + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td>Carrier </td><td>" + response.global_name.locations[0].phone_numbers[0].carrier + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td>Extension or Range </td><td>" + response.global_name.locations[0].phone_numbers[0].extension_or_range + "</td></tr>"
                            + "<tr class = \"TablePadding\"><td>Phone# </td><td>" + response.global_name.locations[0].phone_numbers[0].phone_number + "</td></tr></table></div></div><hr>"
                    }
                }
            };

            if (response.global_name.phone_numbers.length > 0) {
                for (var i = 0; i < response.global_name.phone_numbers.length; i++) {
              
                PhoneNumber += SeparatorTitle.replace("#title#", "Contact information") + "<table><tr><td class = \"TablePadding\">Type </td><td>" + response.global_name.phone_numbers[0].type + "</td></tr>"
                        + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Carrier </td><td>" + response.global_name.phone_numbers[0].carrier + "</td></tr>"
                        + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Extension or Range </td><td>" + response.global_name.phone_numbers[0].extension_or_range + "</td></tr>"
                        + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Phone# </td><td>" + response.global_name.phone_numbers[0].phone_number + "</td></tr></table></div></div><hr>"
                }
            }


            MainDocument = SeparatorTitle.replace("#title#", "Global Name") + "<table class = \"TablePadding\"><tr><td class = \"TablePadding\">Global Name# </td><td class = \"MainCaption\">" + response.global_name.global_name_number + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Title </td><td>" + response.global_name.title + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Salutation Name </td><td>" + response.global_name.salutation_name + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">First Name </td><td>" + response.global_name.first_name + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Maiden Name </td><td>" + response.global_name.maiden_name + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Last name </td><td>" + response.global_name.last_name + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Mailing Name </td><td>" + response.global_name.mailing_name + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Marital Status </td><td>" + response.global_name.marital_status + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Gender </td><td>" + response.global_name.gender + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Dob </td><td>" + response.global_name.dob + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Place of Birth </td><td>" + response.global_name.place_of_birth + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Occupation </td><td>" + response.global_name.occupation + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Company Name </td><td>" + response.global_name.company_name + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Email Address </td><td>" + response.global_name.email_address + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Tax ID# </td><td>" + response.global_name.tax_id_number + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Service Type </td><td>" + response.global_name.service_type + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">National ID Type </td><td>" + response.global_name.national_id_type + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">National ID# </td><td>" + response.global_name.national_id + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Nationality </td><td>" + response.global_name.nationality + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Notes</td><td>" + response.global_name.notes + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Company </td><td class = \"MainCaption\">" + SwitchBoolean(response.global_name.is_a_company) + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Service Provider </td><td class = \"MainCaption\">" + SwitchBoolean(response.global_name.is_a_service_provider) + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Date Driver's Licence Issued </td><td>" + response.global_name.drivers_licence_date_issued + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Date Driver's Licence First Issued </td><td>" + response.global_name.drivers_licence_first_issued + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Date Driver's Licence# </td><td>" + response.global_name.drivers_licence_number + "</td></tr>"
                           + "<tr class = \"TablePadding\"><td class = \"TablePadding\">Date Driver's Licence Country </td><td>" + response.global_name.drivers_licence_country + "</td></tr></table></div></div><hr>"


            var CombinedItems = MainDocument + PhoneNumber + Location + LocationPhoneNumbers;

            var GlobalNameResponse = "<p class=\"VaribaleLables\">Quotation#: </p><p>" + response.global_name.global_name_number + "</p>"
                                 + "<p class=\"VaribaleLables\">Quotation#: </p><p>" + response.success + "</p>";
            document.getElementById("ResultSet").innerHTML = CombinedItems //ResultsPrefix + "<div class=\"alert alert-warning\" role=\"alert\">" + GlobalNameResponse + "</div>";
        } else {
            var GlobalNameResponse = "<p class=\"VaribaleLables\">Message: </p><p>" + response.error_message + "</p>"
                                 + "<p class=\"VaribaleLables\">Success: </p><p>" + response.success + "</p>";
            document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">" + GlobalNameResponse + "</div>"
        }
        };

    function ErrorGetGlobalNames(response) {
        document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">Failed to connect to UnderWriter (:<p>" + response.error_message + "</p></div>";
    };

   