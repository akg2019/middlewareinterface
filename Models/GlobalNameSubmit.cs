﻿using System;
using System.Collections.Generic;
using Ucon.Models;
using System.Linq;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class GlobalNameSubmit
    {
        public resquestkey resquestkey = new resquestkey();
        public string global_name_id { set; get; }
	    public string global_name_number { set; get; }
        public string company_name { set; get; }
        public string dob { set; get; }
        public string drivers_licence_country { set; get; }
        public string drivers_licence_date_issued { set; get; }
        public string drivers_licence_first_issued { set; get; }
        public string drivers_licence_number { set; get; }
        public string email_address { set; get; }
        public string employment_type { set; get; }
        public string first_name { set; get; }
        public string gender { set; get; }
        public string is_a_company { set; get; }
        public string is_a_service_provider { set; get; }
        public string last_name { set; get; }
        public string maiden_name { set; get; }
        public string mailing_name { set; get; }
        public string marital_status { set; get; }
        public string middle_name { set; get; }
        public string national_id { set; get; }
        public string national_id_type { set; get; }
        public string nationality { set; get; }
        public string notes { set; get; }
        public string occupation { set; get; }
        public string place_of_birth { set; get; }
        public string salutation_name { set; get; }
        public string service_type { set; get; }
        public string tax_id_number { set; get; }
        public string title { set; get; }
        public List<phone_numbers> phone_numbers = new List<phone_numbers>();
        public List<locations> locations = new List<locations>();

    }

    public class locations
    {
        public string altitude { set; get; }
        public string block { set; get; }
        public string building_number { set; get; }
        public string categorization { set; get; }
        public string complex_name { set; get; }
        public string country { set; get; }
        public string general_area { set; get; }
        public string international_area { set; get; }
        public string is_international_address { set; get; }
        public string island { set; get; }
        public string latitude { set; get; }
        public string locaction_class { set; get; }
        public string longitude { set; get; }
        public string parish { set; get; }
        public string postal_zip_code { set; get; }
        public string street_name { set; get; }
        public string street_number { set; get; }
        public string street_type { set; get; }
        public string town { set; get; }
        public string unit_number { set; get; }
        public List<phone_numbers> phone_numbers = new List<phone_numbers>();
    }

  
    public class GlobalNameObject
    {
        public JObject GlobalNameID = new JObject();
    }

}