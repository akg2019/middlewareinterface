﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ucon.Models
{
    public class DoNotInsure
    {
		    public bool onceInTable { set; get; }
	        public string make { set; get; }
	        public string model { set; get; }
	        public string model_type { set; get; }
	        public string extension { set; get; }
	        public int year { set; get; }
            public bool success { set; get; }
            public bool do_not_insure { set; get; }
    }
       
}