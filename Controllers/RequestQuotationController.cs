﻿using System.Web.Mvc;
using System.IO;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using Ucon.Models;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Ucon.Controllers
{
    public class RequestQuotationController : Controller
    {
        //Gets input stream
        private JObject GetInputStream()
        {
            Stream ReqStream = Request.InputStream;
            ReqStream.Seek(0, SeekOrigin.Begin);
            string IncommingData = new StreamReader(ReqStream).ReadToEnd();
            return JObject.Parse(IncommingData);
        }


        quotation QuotationManager = new quotation();
        static Temprisk Qrisk = new Temprisk();
        static Veh Loc = new Veh();
        static Tempquotation Tempquotations = new Tempquotation();
        static address Address = new address();
        public static object GlobalQuoteDetails;
        public static string CustomerNames;
        string ErrorsInfoName = "";
        // GET: RequestQuotation
        public ActionResult Index()
        {
            SubmitQuotation.Phone_Numbers.Clear();
            SubmitQuotation.ManualRates.Clear();
            SubmitQuotation.Drivers.Clear();
            GlobalQuoteDetails = "";
            CustomerNames = "";
            quotation Q = new quotation();         
            return View(Q);
        }


        //Returns Motor AUthorized Drivers List
        [HttpPost]
        public object GetNamedDrivers(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehAuthDrivers", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }


        //Returns Motor Vehicle Color
        [HttpPost]
        public object GetVehicleColor()
        {
            return JObject.Parse( KeyManagement.PerformRequest("epic_mwGetVehColour", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        //Returns Motor Vehicle Color
        [HttpPost]
        public object GetUsages(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetUsages", KeyManagement.RequestType.SignleParameter, PolicyPrefix.ToUpper(), new object()));
        }

        //Returns Motor Vehicle Color
        [HttpPost]
        public object GetOccupations()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetOccupations", KeyManagement.RequestType.SignleParameter, "All", new object()));
        }


        //Returns Motor Vehicle Import Types
        [HttpPost]
        public object VehImportType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehImportType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Returns Motor Vehicle Roof Types
        [HttpPost]
        public object VehRoofType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehRoofType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Returns Motor Vehicle Roof Types
        [HttpPost]
        public object PropRoofType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPropRoofType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Returns Motor Vehicle Roof Types
        [HttpPost]
        public object Parishes()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCParishTowns", KeyManagement.RequestType.SignleParameter, "", new object()));
        }

        public static List<string> Parish()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCParishTowns", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach(JToken P in Parish["data"].Children())
            {
                _Parish.Add(Parish["data"][ParishCount]["parish"].ToString()); ParishCount += 1;
            }

            return _Parish;
        }


        public static List<string> CountryList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCCountries", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                _Parish.Add(P.ToString()); ParishCount += 1;
            }

            return _Parish;
        }


        public static List<string> OccupationList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetOccupations", KeyManagement.RequestType.SignleParameter, "All", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P["occupation"].ToString().Trim() != "")
                {
                    _Parish.Add(P["occupation"].ToString()); ParishCount += 1;
                }
                
            }

            return _Parish;
        }



        public static List<string> TransmissionList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehTransmissionType", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P.ToString().Trim() != "")
                {
                    _Parish.Add(P.ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }


        public static List<string> ColorList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehColour", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P.ToString().Trim() != "")
                {
                    _Parish.Add(P.ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }


        public static List<string> ImportList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehImportType", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P.ToString().Trim() != "")
                {
                    _Parish.Add(P.ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }

        public static List<string> PolicyList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyPrefixes", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["policy_prefixes"].Children())
            {
                if (P["policy_prefix"].ToString().Trim() != "")
                {
                    _Parish.Add(P["policy_prefix"].ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }


        public static List<string> RoofList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehRoofType", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P.ToString().Trim() != "")
                {
                    _Parish.Add(P.ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }


       public static List<string> CurrencyList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetCurrency", KeyManagement.RequestType.SignleParameter, "", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P.ToString().Trim() != "")
                {
                    _Parish.Add(P.ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }


        public static List<string> MakeList()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehMakeModels", KeyManagement.RequestType.SignleParameter, "motor", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                if (P["make"].ToString().Trim() != "")
                {
                    _Parish.Add(P["make"].ToString()); ParishCount += 1;
                }

            }

            return _Parish;
        }




        public object Country(string CountryT)
        {

            int CountryCount = 0;
            List<string> _Country = new List<string>();
            JObject Country = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCParishTowns", KeyManagement.RequestType.SignleParameter, CountryT, new object()));
            foreach (JToken P in Country["data"].Children())
            {
                _Country.Add(P.ToString()); CountryCount += 1;
            }

            return Country;
        }

        //Returns Motor Vehicle Transmission Types
        [HttpPost]
        public object VehTransmissionType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehTransmissionType", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        [HttpPost]
        public object GetDropDownData(string Make)
        {
            SubmitQuotation.DropDownListData.Clear();
            
            //Gets Makes and Models
            JObject MM =JObject.Parse(JsonConvert.SerializeObject(GetModels()));
            JObject DropData = new JObject();
            int MakeCount,ModelCount;
            MakeCount = 0;ModelCount = 0;
            List<string> Makes = new List<string>();
            JArray Model = new JArray();
            JArray ModelType = new JArray();
            JArray BodyShape = new JArray();
            JArray BodyTypes = new JArray();
            JArray Extensions = new JArray();
            try
            {
                foreach (JToken T in MM["data"].Children())
                {
                    if (MM["data"][MakeCount]["make"].ToString() == Make)
                    {
                        //Gets Models
                        foreach (JToken Models in MM["data"][MakeCount]["models"].Children())
                        {
                            Model.Add(MM["data"][MakeCount]["models"][ModelCount]["model"].ToString());
                            ModelType.Add(MM["data"][MakeCount]["models"][ModelCount]["model_type"].ToString());
                            BodyShape.Add(MM["data"][MakeCount]["models"][ModelCount]["body_shape"].ToString());
                            BodyTypes.Add(MM["data"][MakeCount]["models"][ModelCount]["body_type"].ToString());
                            Extensions.Add(MM["data"][MakeCount]["models"][ModelCount]["extension"].ToString());

                            ModelCount += 1;

                        }
                    }
                    MakeCount += 1;
                }

                
                DropData.Add("Models", Model);
                DropData.Add("ModelTypes", ModelType);
                DropData.Add("BodyShapes", BodyShape);
                DropData.Add("BodyTypes", BodyTypes);
                DropData.Add("Extensions", Extensions);

            }
            catch (System.Exception)
            {

                //throw;
            }
           

            return DropData;
        }


        //Adds a phone number
        [HttpPost]
        public object AddPhoneNumbers()
        {
            try
            {
                phone_numbers PhoneProperty = new phone_numbers();
                PhoneProperty.carrier = GetInputStream()["carrier"].ToString();
                PhoneProperty.type = GetInputStream()["type"].ToString();
                PhoneProperty.extension_or_range = GetInputStream()["extension_or_range"].ToString();
                PhoneProperty.description = GetInputStream()["description"].ToString();
                PhoneProperty.phone_number = GetInputStream()["phone_number"].ToString();
                SubmitQuotation.Phone_Numbers.Add(PhoneProperty);
     
            }
            catch
            {
              
            }
                return JsonConvert.SerializeObject(SubmitQuotation.Phone_Numbers);     

        }

    //Delete a phone number
       [HttpPost]
       public object RemovePhoneNumber()
        {
            SubmitQuotation.Phone_Numbers.RemoveAt(int.Parse(GetInputStream()["PhoneToDelete"].ToString()));
            return JsonConvert.SerializeObject(SubmitQuotation.Phone_Numbers);
        }


        //Adds a driver object
        [HttpPost]
        public object AddDriver()
        {
            try
            {
                drivers Driver = new drivers();
                Driver.drivers_licence_first_issued = GetInputStream()["drivers_licence_first_issued"].ToString();
                Driver.drivers_licence_number = GetInputStream()["drivers_licence_number"].ToString();
                Driver.driver_dob = GetInputStream()["driver_dob"].ToString();
                Driver.first_name = GetInputStream()["first_name"].ToString();
                Driver.is_main_driver = GetInputStream()["is_main_driver"].ToString(); 
                Driver.last_name = GetInputStream()["last_name"].ToString();
                Driver.middle_name = GetInputStream()["middle_name"].ToString();
                SubmitQuotation.Drivers.Add(Driver);

            }
                catch

            {

            }
            return JsonConvert.SerializeObject(SubmitQuotation.Drivers);
        }

        //Remove Driver
        [HttpPost]
        public object RemoveDriver()
        {
            try {SubmitQuotation.Drivers.RemoveAt(int.Parse(GetInputStream()["DriverToDelete"].ToString())); }
            catch { }

            return JsonConvert.SerializeObject(SubmitQuotation.Drivers);
        }

        //Adds a Manual Rate Object
        [HttpPost]
        public object AddManualRate()
        {
            try
            {
                manual_rates ManualRate = new manual_rates();
                ManualRate.code = GetInputStream()["code"].ToString();
                ManualRate.value = int.Parse(GetInputStream()["value"].ToString());
                SubmitQuotation.ManualRates.Add(ManualRate);
            } catch { }
            return JsonConvert.SerializeObject(SubmitQuotation.ManualRates);
        }


        //Remove Manual Rate
        [HttpPost]
        public object RemoveManualRate()
        {
            try { SubmitQuotation.ManualRates.RemoveAt(int.Parse(GetInputStream()["RateToDelete"].ToString())); }
            catch { }

            return JsonConvert.SerializeObject(SubmitQuotation.ManualRates);
        }
      
        [HttpPost]
        public object AddRisk()
        {
            bool success = false;
            try
            {
                //Risk Section
                Qrisk.authorized_driver_wording = GetInputStream()["authorized_driver_wording"].ToString();
                Qrisk.body_shape = GetInputStream()["body_shape"].ToString();
                Qrisk.body_type = GetInputStream()["body_type"].ToString();
                Qrisk.chassis_number = GetInputStream()["chassis_number"].ToString();
                Qrisk.colour = GetInputStream()["colour"].ToString();
                Qrisk.engine_modified = GetInputStream()["engine_modified"].ToString();
                Qrisk.engine_number = GetInputStream()["engine_number"].ToString();
                Qrisk.engine_type = GetInputStream()["engine_type"].ToString();
                Qrisk.extension = GetInputStream()["extension"].ToString();
                Qrisk.has_electric_doors = GetInputStream()["has_electric_doors"].ToString();
                Qrisk.has_electric_side_mirror = GetInputStream()["has_electric_side_mirror"].ToString();
                Qrisk.has_electric_window = GetInputStream()["has_electric_window"].ToString();
                Qrisk.has_power_steering = GetInputStream()["has_power_steering"].ToString();
                Qrisk.hp_cc_rating = GetInputStream()["hp_cc_rating"].ToString();
                Qrisk.hp_cc_unit_type = GetInputStream()["hp_cc_unit_type"].ToString();
                Qrisk.import_type = GetInputStream()["import_type"].ToString();
                Qrisk.left_hand_drive = GetInputStream()["left_hand_drive"].ToString();
                Qrisk.main_driver_dob = GetInputStream()["main_driver_licence_first_issued"].ToString();
                Qrisk.make = GetInputStream()["make"].ToString();
                Qrisk.mileage = GetInputStream()["mileage"].ToString();
                Qrisk.mileage_type = GetInputStream()["mileage_type"].ToString();
                Qrisk.model = GetInputStream()["model"].ToString();
                Qrisk.model_type = GetInputStream()["model_type"].ToString();
                Qrisk.ncd_percent =GetInputStream()["ncd_percent"].ToString();
                Qrisk.number_of_cyclinders = GetInputStream()["number_of_cyclinders"].ToString();
                Qrisk.number_of_doors = (GetInputStream()["number_of_engines"].ToString());
                Qrisk.registration_number = GetInputStream()["registration_number"].ToString();
                Qrisk.roof_type = GetInputStream()["roof_type"].ToString();
                Qrisk.seating = GetInputStream()["seating"].ToString();
                Qrisk.seat_type = GetInputStream()["seat_type"].ToString();
                Qrisk.sum_insured = GetInputStream()["sum_insured"].ToString();
                Qrisk.tonnage = GetInputStream()["tonnage"].ToString();
                Qrisk.transmission_type = GetInputStream()["transmission_type"].ToString();
                Qrisk.usage_code = GetInputStream()["usage_code"].ToString();
                //if(GetInputStream()["usage_code"].ToString() == "") { Qrisk.usage_code = "SDP"; }
                Qrisk.year = GetInputStream()["year"].ToString();
                Qrisk.year_for_rating = GetInputStream()["year_for_rating"].ToString();



                //Vehicle Location Section
                Loc.altitude = GetInputStream()["altitude"].ToString();
                SubmitQuotation.VehLoc.block = GetInputStream()["block"].ToString();
                SubmitQuotation.VehLoc.building_number = GetInputStream()["building_number"].ToString();
                SubmitQuotation.VehLoc.categorization = GetInputStream()["categorization"].ToString();
                SubmitQuotation.VehLoc.complex_name = GetInputStream()["complex_name"].ToString();
                SubmitQuotation.VehLoc.country = GetInputStream()["country"].ToString();
                SubmitQuotation.VehLoc.general_area = GetInputStream()["general_area"].ToString();
                SubmitQuotation.VehLoc.international_area = GetInputStream()["international_area"].ToString();
                SubmitQuotation.VehLoc.island = GetInputStream()["island"].ToString();
                SubmitQuotation.VehLoc.is_international_address = GetInputStream()["is_international_address"].ToString();
                SubmitQuotation.VehLoc.latitude = GetInputStream()["latitude"].ToString();
                SubmitQuotation.VehLoc.locaction_class = GetInputStream()["locaction_class"].ToString();
                SubmitQuotation.VehLoc.longitude = GetInputStream()["longitude"].ToString();
                SubmitQuotation.VehLoc.parish = GetInputStream()["parish"].ToString();
                SubmitQuotation.VehLoc.postal_zip_code = GetInputStream()["postal_zip_code"].ToString();
                SubmitQuotation.VehLoc.street_name = GetInputStream()["street_name"].ToString();
                SubmitQuotation.VehLoc.street_number = GetInputStream()["street_number"].ToString();
                SubmitQuotation.VehLoc.street_type = GetInputStream()["street_type"].ToString();
                SubmitQuotation.VehLoc.town = GetInputStream()["town"].ToString();
                SubmitQuotation.VehLoc.unit_number = GetInputStream()["unit_number"].ToString();

                success = true;
            }
            catch
            {
                success = false;
            }

            return Json(new { success = success });
        }


        [HttpPost]
        public object Index(quotation Quote)
        {
           
            try
            {

                //Main 
                
                Tempquotations.accidents_in_last_few_years = Quote.accidents_in_last_few_years;
                Tempquotations.company_name = Quote.company_name;
                Tempquotations.currency = Quote.currency;
                Tempquotations.email_address = Quote.email_address;
                Tempquotations.first_name = Quote.first_name;
                Tempquotations.is_motor_cat_perils_covered = Quote.is_motor_cat_perils_covered;
                Tempquotations.last_name = Quote.last_name;
                Tempquotations.middle_name = Quote.middle_name;
                Tempquotations.national_id = Quote.national_id;
                Tempquotations.national_id_type = Quote.national_id_type;
                Tempquotations.occupation = Quote.occupation;
                Tempquotations.policy_prefix = Quote.policy_prefix;
                Tempquotations.quotation_number = Quote.quotation_number;
                Tempquotations.start_date = Quote.start_date;

                //Gets Customer's name
                CustomerNames = Quote.first_name + " " + Quote.middle_name + ", " + Quote.last_name;

                //Address Section

                SubmitQuotation.Adds.altitude = Quote.address.altitude;
                SubmitQuotation.Adds.block = Quote.address.block;
                SubmitQuotation.Adds.building_number = Quote.address.building_number;
                SubmitQuotation.Adds.categorization = Quote.address.categorization;
                SubmitQuotation.Adds.complex_name = Quote.address.complex_name;
                SubmitQuotation.Adds.country = Quote.address.country;
                SubmitQuotation.Adds.general_area = Quote.address.general_area;
                SubmitQuotation.Adds.international_area = Quote.address.international_area;
                SubmitQuotation.Adds.island = Quote.address.island;
                SubmitQuotation.Adds.is_international_address = Quote.address.is_international_address;
                SubmitQuotation.Adds.latitude = Quote.address.latitude;
                SubmitQuotation.Adds.locaction_class = Quote.address.locaction_class;
                SubmitQuotation.Adds.longitude = Quote.address.longitude;
                SubmitQuotation.Adds.parish = Quote.address.parish;
                SubmitQuotation.Adds.postal_zip_code = Quote.address.postal_zip_code;
                SubmitQuotation.Adds.street_name = Quote.address.street_name;
                SubmitQuotation.Adds.street_number = Quote.address.street_number;
                SubmitQuotation.Adds.street_type = Quote.address.street_type;
                SubmitQuotation.Adds.town = Quote.address.town;
                SubmitQuotation.Adds.unit_number = Quote.address.unit_number;

                
                JObject QuotationObject = new JObject();
                SubmitQuotation.r.key = KeyManagement.ApiKey();
                
                QuotationObject.Add("resquestkey", JObject.Parse(JsonConvert.SerializeObject(SubmitQuotation.r)));
                QuotationObject.Add("quotation", JObject.Parse(JsonConvert.SerializeObject(Tempquotations)));
                List<Temprisk> TMidi = new List<Temprisk>();
                TMidi.Add(Qrisk);
                QuotationObject.Add("risk", JArray.Parse(JsonConvert.SerializeObject(TMidi)));
                JObject F = QuotationObject;
                object QuoteRequest = new object();
                QuoteRequest = F;
                


                QuoteResponse = JObject.Parse(KeyManagement.PerformRequest("epic_mwSubmitQuotation", KeyManagement.RequestType.Quotation, "", QuoteRequest));
              
                //Returns error page if the Object is empty

                if (QuoteResponse.Count == 0)
                {
                    RedirectToAction("ErrorQuote", (new ErrorQuoteResponse { success = false, error_message = "There were errors in the information you tried to submit" }).ToString());
                }

                bool HasError = false;
               
                foreach (JProperty P in QuoteResponse.Children())
                {
                    if (P.Name == "error_message")
                    {
                        HasError = true;
                        ErrorsInfoName = QuoteResponse["error_message"].ToString();
                    } else if(P.Name == "Info")
                    {
                        HasError = true;
                        ErrorsInfoName =QuoteResponse["Details"].ToString().Replace("start_date is date", "start_date");
                       
                    }
                }
                QuotationResponseObject.ErrorCode = ErrorsInfoName;
                if (HasError == true)
                {
                    RedirectToAction("ErrorQuote", (new ErrorQuoteResponse { success = false, error_message = ErrorsInfoName }).ToString());
                }


            }
            catch
            
            {
                RedirectToAction("ErrorQuote", (new ErrorQuoteResponse {success=false,error_message= ErrorsInfoName }).ToString()); 
            }

            GlobalQuoteDetails = QuoteResponse;
            return RedirectToAction("QuotationResponseDetails");
        }

       
        public ActionResult QuotationResponseDetails()
        {
            QuotationResponseObject QDetails = new QuotationResponseObject();
            try
            {
                 QDetails.QuoteError = QuotationResponseObject.ErrorCode.ToString();
            }
            catch (System.Exception)
            {

            }

          
           
            //Response is null
            if (GlobalQuoteDetails == null)
            {
                RedirectToAction("ErrorQuote", (new ErrorQuoteResponse { success = false, error_message = ErrorsInfoName }).ToString());

            }


            try
            {
            JObject ResponseObject = JObject.Parse(JsonConvert.SerializeObject(GlobalQuoteDetails));

                //Main Section
                
                QDetails.success = (bool)ResponseObject["success"];   
                try {  QDetails.quotation_number = int.Parse(ResponseObject["quotation_number"].ToString());}catch (System.Exception) {
                    try { QDetails.quotation_number = int.Parse(ResponseObject["quotation"]["quotation_number"].ToString()); } catch (System.Exception) { }
                }
                QDetails.net_premium = double.Parse(ResponseObject["premium_calculation"]["net_premium"].ToString());
                QDetails.stamp_duty = double.Parse(ResponseObject["premium_calculation"]["stamp_duty"].ToString());
                QDetails.tax = double.Parse(ResponseObject["premium_calculation"]["tax"].ToString());
                QDetails.total_premium = double.Parse(ResponseObject["premium_calculation"]["total_premium"].ToString());
                QDetails.CustomerName = ResponseObject["quotation"]["first_name"].ToString() + " " + ResponseObject["quotation"]["last_name"].ToString();

                //limits section
                int LimitsCount = 0;
                foreach (var Limits in ResponseObject["limits"].Children())
                {
                    QDetails.Limits.Add(new limits
                    {
                        code = ResponseObject["limits"][LimitsCount]["code"].ToString(),
                        heading = ResponseObject["limits"][LimitsCount]["heading"].ToString(),
                        limit = ResponseObject["limits"][LimitsCount]["limit"].ToString(),
                        description = ResponseObject["limits"][LimitsCount]["description"].ToString()
                    });

                    LimitsCount += 1;
                }


                //extensions section
                int ExtensionCount = 0;

                foreach (var Extensions in ResponseObject["extensions"].Children())
                {
                    QDetails.extensions.Add(new extensions
                    {
                        type = ResponseObject["extensions"][ExtensionCount]["type"].ToString(),
                        code = ResponseObject["extensions"][ExtensionCount]["code"].ToString(),
                        heading = ResponseObject["extensions"][ExtensionCount]["heading"].ToString(),
                        description = ResponseObject["extensions"][ExtensionCount]["description"].ToString()
                    });
                    ExtensionCount += 1;
                }
            }
            
            catch (System.Exception)
            {
                RedirectToAction("ErrorQuote", GlobalQuoteDetails);
            }
          
           if(GlobalQuoteDetails == null)
            {
                RedirectToAction("ErrorQuote", (new ErrorQuoteResponse { success = false, error_message = "There were errors in the information you tried to submit" }).ToString());

            }
            return View(QDetails);
        }

      
        public ActionResult ErrorQuote(string ErrorResponses)
        {
            ErrorQuoteResponse ErrorsR = new ErrorQuoteResponse();

            ErrorsR.success = false;
            ErrorsR.error_message = JObject.Parse(ErrorResponses)["error_message"].ToString();

            return View(ErrorsR);
        }
        
        
        public static JObject QuoteResponse;
        [HttpPost]
        public object GetQresponse() { return QuoteResponse; }

        private class Tempquotation
        {
            public string quotation_number { set; get; }
            [Required]
            public string policy_prefix { set; get; }
            [Required]
            public string first_name { set; get; }
            public string middle_name { set; get; }
            [Required]
            public string last_name { set; get; }
            [Required]
            public string national_id { set; get; }
            [Required]
            public string national_id_type { set; get; }
            public string email_address { set; get; }
            public string company_name { set; get; }
            public string start_date { set; get; }
            public bool accidents_in_last_few_years { set; get; }
            public string occupation { set; get; }
            public string currency { set; get; }
            public bool is_motor_cat_perils_covered { set; get; }
            public List<phone_numbers> phone_numbers = SubmitQuotation.Phone_Numbers;
            public address address = SubmitQuotation.Adds;
            
        }      
          
       
        public class Temprisk
        {
            public string sum_insured { set; get; }
            [Required]
            public string usage_code { set; get; }
            [Required]
            public string authorized_driver_wording { set; get; }
            [Required]
            public string year_for_rating { set; get; }
            [Required]
            public string year { set; get; }
            [Required]
            public string make { set; get; }
            public string model { set; get; }
            public string model_type { set; get; }
            public string extension { set; get; }
            public string body_type { set; get; }
            public string body_shape { set; get; }
            public string hp_cc_rating { set; get; }
            public string hp_cc_unit_type { set; get; }
            public string chassis_number { set; get; }
            public string colour { set; get; }
            public string number_of_cyclinders { set; get; }
            public string engine_modified { set; get; }
            public string engine_number { set; get; }
            public string engine_type { set; get; }
            public string has_electric_doors { set; get; }
            public string has_electric_side_mirror { set; get; }
            public string has_electric_window { set; get; }
            public string has_power_steering { set; get; }
            public string import_type { set; get; }
            public string ncd_percent { set; get; }
            public string left_hand_drive { set; get; }
            public string main_driver_dob { set; get; }
            public string main_driver_licence_first_issued { set; get; }
            public string mileage { set; get; }
            public string mileage_type { set; get; }
            public string number_of_doors { set; get; }
            public string number_of_engines { set; get; }
            public string registration_number { set; get; }
            public string roof_type { set; get; }
            public string seat_type { set; get; }
            public string seating { set; get; }
            public string tonnage { set; get; }
            public string transmission_type { set; get; }
            public vehicle_location vehicle_location = SubmitQuotation.VehLoc;
            public List<drivers> drivers = SubmitQuotation.Drivers;
            public List<manual_rates> manual_rates = SubmitQuotation.ManualRates;

        }

        public class Veh
        {
            public string altitude { set; get; }
            public string block { set; get; }
            public string building_number { set; get; }
            public string categorization { set; get; }
            public string complex_name { set; get; }
            public string country { set; get; }
            public string general_area { set; get; }
            public string international_area { set; get; }
            public string is_international_address { set; get; }
            public string island { set; get; }
            public string latitude { set; get; }
            public string locaction_class { set; get; }
            public string longitude { set; get; }
            public string parish { set; get; }
            public string postal_zip_code { set; get; }
            public string street_name { set; get; }
            public string street_number { set; get; }
            public string street_type { set; get; }
            public string town { set; get; }
            public string unit_number { set; get; }
        }

        //Gets Makes and Models
        [HttpPost]
        public object GetModels()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehMakeModels", KeyManagement.RequestType.SignleParameter,"Motor",new object()));
        }

        public static void GetQuotation(string QuoteNumber)
        {
            SubmitQuotation.Phone_Numbers.Clear();
            SubmitQuotation.ManualRates.Clear();
            SubmitQuotation.Drivers.Clear();
            GlobalQuoteDetails = "";
            CustomerNames = "";
            GlobalQuoteDetails = JObject.Parse(KeyManagement.PerformRequest("epic_mwQuotationGet", KeyManagement.RequestType.SignleParameter, QuoteNumber, new object()));
          
        }



        public ActionResult MPQ()
        {
            return View();
        }

    }
}
