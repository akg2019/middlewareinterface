﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ucon.Models
{
    public class PolicySubmit
    {
        public class policy
        {
            public int policy_number { set; get; }
            public String account_code { set; get; }
            public bool agreed_value { set; get; }
            public String billing_account_code { set; get; }
            public String branch { set; get; }
            public String country { set; get; }
            public String currency { set; get; }
            public String end_date { set; get; }
            public String policy_prefix { set; get; }
            public String source_account_code { set; get; }
            public String start_date { set; get; }

            //Insured 
            public List<insured> Insured = new List<insured>();
            public class insured
            {
                public bool is_main_insured { set; get; }
                public String national_id { set; get; }
                public String national_id_ty { set; get; }
            }


            public class global_names
            {
                public String company_name { set; get; }
                public String dob { set; get; }
                public String drivers_licence_country { set; get; }
                public String drivers_licence_date_issued { set; get; }
                public String drivers_licence_first_issued { set; get; }
                public String drivers_licence_number { set; get; }
                public String email_address { set; get; }
                public String employment_type { set; get; }
                public String first_name { set; get; }
                public String gender { set; get; }
                public int global_name_number { set; get; }
                public bool is_a_company { set; get; }
                public bool is_a_service_provider { set; get; }
                public String last_name { set; get; }
                public String locations { set; get; }
            }

            //Location
            List<location> Location = new List<location>();
            public class location
            {
                public bool is_main_location { set; get; }
                public bool is_mailing_address { set; get; }
                public String altitude { set; get; }
                public String block { set; get; }
                public String building_number { set; get; }
                public String country { set; get; }
                public String general_area { set; get; }
                public String international_area { set; get; }
                public String is_international_address { set; get; }
                public String island { set; get; }
                public String latitude { set; get; }
                public String longitude { set; get; }
                public String parish { set; get; }
                public String postal_zip_code { set; get; }
                public String street_name { set; get; }
                public String street_number { set; get; }
                public String street_type { set; get; }
                public String town { set; get; }
                public String unit_number { set; get; }

                //Phone Numbers
                List<phone_numbers> PhoneNumbers = new List<phone_numbers>();
                public class phone_numbers 
						{
                          public String type { set; get; }
                          public String carrier { set; get; }
                          public String description { set; get; }
                          public String extension_or_range { set; get; }
                            public String phone_number { set; get; }
                        }
                
            }

			public String maiden_name {set; get;}
			public String mailing_name {set; get;}
			public String marital_status {set; get;}
			public String middle_name {set; get;}
			public String national_id {set; get;}
			public String national_id_type {set; get;}
			public String nationality {set; get;}
			public String notes {set; get;}
			public String occupation {set; get;}
			public String occupation_code {set; get;}
			public String occupation_description {set; get;}
			public String phone_number_fax {set; get;}
			public String phone_number_general {set; get;}
			public String phone_number_mobile {set; get;}
			public String place_of_birth {set; get;}
			public String salutation_name {set; get;}
			public String service_type {set; get;}
			public String tax_id_number {set; get;}
			public String title {set; get;}

            //Phone numbers
            List<phone_numbers> PhoneNumbers = new List<phone_numbers>();
            public class phone_numbers
				{
					public String type {set; get;}
					public String carrier {set; get;}
					public String description {set; get;}
					public String extension_or_range { set; get; }
                    public String phone_number { set; get; }
                }

            //Risk
            List<risk> Risk = new List<risk>();
        	public class risk
		        {
			        public String risk_item_type {set; get;}
			        public bool deleted { set; get; }
                    public int sum_insured { set; get; }
                    public String usage_code  {set; get;}
			        public String authorized_drivers {set; get;}
			        public int year_for_rating { set; get; }
                    public int year { set; get; }
                    public String make {set; get;}
			        public String model {set; get;}
			        public String model_type {set; get;}
			        public String extension {set; get;}
			        public String body_type {set; get;}
			        public String body_shape {set; get;}
			        public int hp_cc_rating { set; get; }
                    public String hp_cc_unit_type {set; get;}
			        public String cert_type {set; get;}
			        public String chassis_number {set; get;}
			        public String colour {set; get;}
			        public int number_of_cyclinders { set; get; }
                    public bool engine_modified { set; get; }
                    public String engine_number {set; get;}
			        public String engine_type {set; get;}
			        public bool has_electric_doors { set; get; }
                    public bool has_electric_side_mirror { set; get; }
                    public bool has_electric_window { set; get; }
                    public bool has_power_steering { set; get; }
                    public String import_type {set; get;}
			        public float ncd_percent { set; get; }
                    public bool left_hand_drive { set; get; }
                    public float mileage { set; get; }
                    public String mileage_type {set; get;}
			        public int number_of_doors { set; get; }
                    public int number_of_engines { set; get; }
                    public String registration_number {set; get;}
			        public String roof_type {set; get;}
			        public String seat_type {set; get;}
			        public int seating { set; get; }
                    public int tonnage { set; get; }
                    public String transmission_type {set; get;}
			        public String driver_dob { set; get; }
                    public String date_license_issued { set; get; }

                public class vehicle_location
                { 
				        public String altitude {set; get;}
				        public String block {set; get;}
				        public String building_number {set; get;}
				        public String country {set; get;}
				        public String general_area {set; get;}
				        public String international_area {set; get;}
				        public String is_international_address {set; get;}
				        public String island {set; get;}
				        public String latitude {set; get;}
				        public String longitude {set; get;}
				        public String parish {set; get;}
				        public String postal_zip_code {set; get;}
				        public String street_name {set; get;}
				        public String street_number {set; get;}
				        public String street_type {set; get;}
				        public String town {set; get;}
				        public String unit_number { set; get; }
                }

                //Drivers
                List<drivers> Drivers = new List<drivers>();
                public class drivers
				{
					public bool is_excluded { set; get; }
                    public bool is_included_by_exception { set; get; }
                    public bool is_main_driver { set; get; }
                    public String national_id {set; get;}
					public String national_id_type {set; get;}
					public String relation_to_insured { set; get; }
                }

		    }
	
        }

    }
}