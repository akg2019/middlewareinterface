﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class MakeModels
    {
        public string PolicyType { set; get; }
        public bool ConcatList { set; get; }
        public JObject MakesAndModels = new JObject();
    }
}