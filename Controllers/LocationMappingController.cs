﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;



namespace Ucon.Controllers
{
    public class LocationMappingController : Controller
    {
        // GET: LocationMapping
        public ActionResult Index()
        {
            Models.DBModel.Address AddressMapping = new Models.DBModel.Address();
            return View(AddressMapping);
        }

        [HttpPost]
        public ActionResult Index(Models.DBModel.Address AddressMappings)
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var AddressToUpdate = from p in DB.Addresses
                                  where p.CompanyID == AddressMappings.CompanyID
                                  select p;


            Models.DBModel.Address NewAddressMapping = AddressToUpdate.Single();

            NewAddressMapping.altitude = AddressMappings.altitude;
            NewAddressMapping.block = AddressMappings.block;
            NewAddressMapping.building_number = AddressMappings.building_number;
            NewAddressMapping.categorization = AddressMappings.categorization;
            NewAddressMapping.complex_name = AddressMappings.complex_name;
            NewAddressMapping.country = AddressMappings.country;
            NewAddressMapping.general_area = AddressMappings.general_area;
            NewAddressMapping.international_area = AddressMappings.international_area;
            NewAddressMapping.island = AddressMappings.island;
            NewAddressMapping.is_international_address = AddressMappings.is_international_address;
            NewAddressMapping.latitude = AddressMappings.latitude;
            NewAddressMapping.location_class = AddressMappings.location_class;
            NewAddressMapping.longitude = AddressMappings.longitude;
            NewAddressMapping.parish = AddressMappings.parish;
            NewAddressMapping.postal_zip_code = AddressMappings.postal_zip_code;
            NewAddressMapping.street_name = AddressMappings.street_name;
            NewAddressMapping.street_number = AddressMappings.street_number;
            NewAddressMapping.street_type = AddressMappings.street_type;
            NewAddressMapping.town = AddressMappings.town;
            NewAddressMapping.unit_number = AddressMappings.unit_number;

            DB.SaveChanges();

            return View(NewAddressMapping);
        }



        // GET: LocationMapping/Details/5
        public ActionResult Details(int id)
        {

            return View();
        }

    }
}
