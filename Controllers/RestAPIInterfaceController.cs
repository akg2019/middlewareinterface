﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;
using Newtonsoft.Json.Linq;

namespace Ucon.Controllers
{
    public class RestAPIInterfaceController : Controller
    {
        // GET: RestAPI
        public ActionResult Index()
        {
            RestAPI API = new RestAPI();
            return View(API);
        }

        [HttpPost]
        public ActionResult Index( RestAPI API)
        {
            try
            {
                API.ResponseBody = JObject.Parse(KeyManagement.ExecuteMethod(API.MethodName,API.RequestBody));
            }
            catch (Exception e)
            {
                JObject Error = new JObject();
                Error.Add("Error",e.Message);
                API.ResponseBody = Error;
            }
           
            return View(API);
        }

    }
}
