﻿using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class GeneralAreasController : Controller
    {
        // GET: GeneralAreas
        public ActionResult Index()
        {
            var GeneralAreas = new GetGeneralAreas();

            JObject O = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCGeneralAreas",
                KeyManagement.RequestType.SignleParameter, "Trinidad",
                null));


            foreach (JToken T in O["data"])
            {
                if(T.ToString() != "")
                {
                 GeneralAreas.Lists.Add(T.ToString());
                }
               

            }
            return View(GeneralAreas);
        }

        //Performs search with Country Parameter
        [HttpPost]
        public ActionResult Index(GetGeneralAreas Search)
        {
           
            JObject O = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCGeneralAreas",
                KeyManagement.RequestType.SignleParameter, Search.SearchMethod.SingleParameter,
                null));
            
            foreach (JToken T in O["data"])
            {
                if (T.ToString() != "")
                {
                    Search.Lists.Add(T.ToString());
                }

            }
            return View(Search);
        }

        // GET: GeneralAreas/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: GeneralAreas/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: GeneralAreas/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GeneralAreas/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: GeneralAreas/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: GeneralAreas/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: GeneralAreas/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
