﻿
//AddMainPhoneNumber

function AddPhoneNumberMain(Model) {
    $.ajax({
        type: "POST",
        url: "/GlobalNameManager/AddPhoneNumberMain",
        data: JSON.stringify(Model),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessPhones,
        error: ErrorPhones
    });

    function SuccessPhones() { }
    function ErrorPhones() { }
}