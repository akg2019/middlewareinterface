﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;

namespace Ucon.Controllers
{
    public class VehicleColorsController : Controller
    {
        // GET: VehicleColors
        public ActionResult Index()
        {

            return View();
        }

        // GET: VehicleColors/Details/5
        public ActionResult Details()
        {
            JObject RequestBody = new JObject();
            var Col = new Models.VehicleColors();

            RequestBody.Add("vehicleType", "Motor");
            RequestBody.Add("concatenatedList", false);
           
            string UnderWriterResponse = Models.KeyManagement.PerformRequest("epic_mwGetLOCCountries", 
                Models.KeyManagement.RequestType.NoParameters,"All",RequestBody.ToString()); 


            JObject MyResponse = JObject.Parse(UnderWriterResponse);
            List<JToken> Results;
            Results = MyResponse.Children().ToList();
            
            foreach (JToken Item in Results.Children())
            {
               foreach (JToken Child in Item.Children())
                {
                    Col.Colors.Add(Child.ToString());
                }
            }
         
            return View(Col);
        }

        // GET: VehicleColors/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: VehicleColors/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VehicleColors/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: VehicleColors/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: VehicleColors/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: VehicleColors/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
