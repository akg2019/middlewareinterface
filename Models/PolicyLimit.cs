﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class PolicyLimit
    {
        public JObject PolicyData = new JObject();
        public string PolicyID { set; get; }
    }
}