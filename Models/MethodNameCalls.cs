﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ucon.Models
{
    public class MethodNameCalls
    {
        public enum MethodNames
        {
            epic_mwGetLOCCountries,
            epic_mwGetLOCGeneralAreas,
            epic_mwGetLOCIslands,
            epic_mwGetLOCParishTowns,
            epic_mwGetLOCStreetNames,
            epic_mwGetLOCStreetTypes,
            epic_mwGetLOCZipCodes,
            epic_mwGetOccupations,
            epic_mwGetPropRoofType,
            epic_mwGetPropWallType,
            epic_mwGetUsages,
            epic_mwGetVehAuthDrivers,
            epic_mwGetVehColour,
            epic_mwGetVehMakeModels,
            epic_mwGetVehRoofType,
            epic_mwGetVehTransmissionType,
            epic_mwPolicySubmit,
            epic_mwQuotationConvert,
            epic_mwQuotationGet,
            epic_mwSubmitQuotation,
            epic_mwGlobalNameGet,
            epic_mwIsBroker,
            epic_mwIsClient,
            epic_mwVehDoNotInsure,
            GetContactenatedMakesAndModels,
            epic_mwGlobalNameSubmit
        }
                
        public static string GetMethodName(MethodNames M)
        {
           return M.ToString();
        }

    }
}