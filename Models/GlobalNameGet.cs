﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class GlobalNameGet
    {
        //Global Names
        public string GlobalNumber { set; get; }
        public JObject GlobalNames = new JObject();
        public bool Success { set; get; }
        public class global_name
        {
            public int global_name_number { set; get; }
            public String company_name { set; get; }
            public String dob { set; get; }
            public String drivers_licence_country { set; get; }
            public String drivers_licence_date_issued { set; get; }
            public String drivers_licence_first_issued { set; get; }
            public String drivers_licence_number { set; get; }
            public String email_address { set; get; }
            public String employment_type { set; get; }
            public String first_name { set; get; }
            public String gender { set; get; }
            public bool is_a_companypublic { set; get; }
            public bool is_a_service_provider { set; get; }
            public String last_name { set; get; }

            //Locations
            List<locations> Locations = new List<locations>();
            public class locations
            {
                public int location_number { set; get; }
                public bool is_main_locationpublic { set; get; }
                public String altitude { set; get; }
                public String block { set; get; }
                public String building_label { set; get; }
                public String building_number { set; get; }
                public String complex_name { set; get; }
                public String country { set; get; }
                public String general_area { set; get; }
                public String international_area { set; get; }
                public String is_international_address { set; get; }
                public String island { set; get; }
                public String latitude { set; get; }
                public String longitude { set; get; }
                public String parish { set; get; }
                public String postal_zip_code { set; get; }
                public String street_name { set; get; }
                public String street_number { set; get; }
                public String street_type { set; get; }
                public String town { set; get; }
                public String unit_number { set; get; }

                //Phone numbers
                List<phone_numbers> PhoneNumbers = new List<phone_numbers>();
                public class phone_numbers
                {
                    public String phone_number_id { set; get; }
                    public String type { set; get; }
                    public String carrier { set; get; }
                    public String extension_or_range { set; get; }
                    public String phone_number { set; get; }
                }
            }


            public String maiden_name { set; get; }
            public String mailing_name { set; get; }
            public String marital_status { set; get; }
            public String middle_name { set; get; }
            public String national_id { set; get; }
            public bool national_id_not_required { set; get; }
            public String national_id_type { set; get; }
            public String nationality { set; get; }
            public String notes { set; get; }
            public String occupation { set; get; }
            public String place_of_birth { set; get; }
            public String salutation_name { set; get; }
            public String service_type { set; get; }
            public String tax_id_number { set; get; }
            public String title { set; get; }

            //Phone numbers
            public List<phone_numbers> PhoneNumbers = new List<phone_numbers>();
            public class phone_numbers
            {
                public String phone_number_id { set; get; }
                public String type { set; get; }
                public String carrier { set; get; }
                public String extension_or_range { set; get; }
                public String phone_number { set; get; }
            }
            





        }
    }
}