﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Ucon.Startup))]
namespace Ucon
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
