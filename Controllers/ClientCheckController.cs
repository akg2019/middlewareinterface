﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class ClientCheckController : Controller
    {
        // GET: ClientCheck
        public ActionResult Index()
        {
            ClientCheck Client = new ClientCheck();
            return View(Client);
        }


        [HttpPost]
        public ActionResult Index(ClientCheck Client)
        {
            JObject ReqObject = new JObject();
            ReqObject.Add("key", KeyManagement.ApiKey());
            Client.resquestkey = ReqObject;

            string UnderWriterResponse = KeyManagement.PerformRequest("epic_mwIsBroker",
            KeyManagement.RequestType.TwoParameters, "All", Client);
            JObject MyResponse = JObject.Parse(UnderWriterResponse);

            if (MyResponse["global_name_number"] != null)
            {
                Client.global_name_number = int.Parse(MyResponse["global_name_number"].ToString());
                Client.success = (bool)(MyResponse["success"]);
            }

            return RedirectToAction("IndexResult", Client);
        }



        public ActionResult IndexResult(ClientCheck Client)
        {
            return View(Client);
        }

    }
}
