﻿using System;
using System.Collections.Generic;
using System.Web.Mvc;
using Ucon.Models;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Ucon.Controllers
{
    public class GlobalNameManagerController : Controller
    {
        // GET: GlobalNameManager
        public ActionResult Index()
        {
            return View();
        }

        static GlobalNameSubmit GlobalNames = new GlobalNameSubmit();
        locations GLocations = new locations();
        public static List<phone_numbers> LocationPhoneNumbers = new List<phone_numbers>();    

        public ActionResult SubmitGlobalName()
        {
          return View(GlobalNames);
        }

        public ActionResult AddPhoneNumberMain()
        {
            return View();
        }

        //Adds A phone Number to the Main section of the Global Name
        [HttpPost]
        public ActionResult AddPhoneNumberMain(phone_numbers MainPhone)
        {
            GlobalNames.phone_numbers.Add(MainPhone);
           return RedirectToAction("SubmitGlobalName"); 
        }

        public ActionResult RemovePhoneNumberMain(int Index)
        {
            try
            {
                GlobalNames.phone_numbers.RemoveAt(Index);
            }
            catch (Exception)
            {

                return RedirectToAction("SubmitGlobalName"); 
            }
           
            return RedirectToAction("SubmitGlobalName"); 
        }

        public ActionResult AddPhoneNumberLocation()
        {
            return View();
        }

        //Adds A phone Number to the Location section of the Global Name
        [HttpPost]
        public ActionResult AddPhoneNumberLocation(phone_numbers MainPhone)
        {
            LocationPhoneNumbers.Add(MainPhone);

            return RedirectToAction("AddLocation");
        }

        public ActionResult RemovePhoneNumberLocation(int Index)
        {
            try
            {
                LocationPhoneNumbers.RemoveAt(Index);
            }
            catch (Exception)
            {

                return RedirectToAction("AddLocation");
            }

            return RedirectToAction("AddLocation");
        }


        public ActionResult  AddLocation()
        {
            return View();
        }


        [HttpPost]
        public ActionResult AddLocation(locations Location)
        {
            Location.phone_numbers.AddRange(LocationPhoneNumbers);
            GlobalNames.locations.Add(Location);

            LocationPhoneNumbers.Clear();
            return RedirectToAction("SubmitGlobalName");
        }


        //Submits Global Name
        [HttpPost]
        public ActionResult SubmitGlobalName(GlobalNameSubmit G)
        {
            GlobalNames.company_name = G.company_name;
            GlobalNames.dob = G.dob;
            GlobalNames.drivers_licence_country = G.drivers_licence_country;
            GlobalNames.drivers_licence_date_issued = G.drivers_licence_date_issued;
            GlobalNames.drivers_licence_first_issued = G.drivers_licence_first_issued;
            GlobalNames.drivers_licence_number = G.drivers_licence_number;
            GlobalNames.email_address = G.email_address;
            GlobalNames.employment_type = G.employment_type;
            GlobalNames.first_name = G.first_name;
            GlobalNames.gender = G.gender;
            GlobalNames.global_name_id = G.global_name_id;
            GlobalNames.global_name_number = G.global_name_number;
            GlobalNames.is_a_company = G.is_a_company;
            GlobalNames.is_a_service_provider = G.is_a_service_provider;
            GlobalNames.last_name = G.last_name;
            GlobalNames.maiden_name = G.maiden_name;
            GlobalNames.mailing_name = G.mailing_name;
            GlobalNames.marital_status = G.marital_status;
            GlobalNames.middle_name = G.middle_name;
            GlobalNames.nationality = G.nationality;
            GlobalNames.national_id = G.national_id;
            GlobalNames.national_id_type = G.national_id_type;
            GlobalNames.notes = G.notes;
            GlobalNames.occupation = G.occupation;
            GlobalNames.place_of_birth = G.place_of_birth;
            GlobalNames.salutation_name = G.salutation_name;
            GlobalNames.service_type = G.service_type;
            GlobalNames.tax_id_number = G.tax_id_number;
            GlobalNames.title = G.title;
            GlobalNames.resquestkey.key = KeyManagement.ApiKey();

            JObject Responses = GlobalData.SubmitGlobalName(GlobalNames);
            //GlobalData.SubmitGlobalName(GlobalNames);

            return RedirectToAction("GlobalName",new { GlobalID =JsonConvert.SerializeObject(Responses)});
        }


        public ActionResult GlobalName(string GlobalID)
        {
            GlobalNameObject G = new GlobalNameObject();
            G.GlobalNameID = JObject.Parse(GlobalID);
            
            GlobalNames = new GlobalNameSubmit();
            return View(G);
        }
        


    }
}
