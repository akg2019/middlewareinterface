﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class GetGeneralAreas
    {
        public JObject GAreas = new JObject();
        public List<string> Lists = new List<string>();
        public SearchTool SearchMethod { set; get; }
        public string Countries { set; get; }
    }
    
}