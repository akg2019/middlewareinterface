﻿using System;
using Newtonsoft.Json.Linq;
using System.IO;
using System.Text;
using System.Net;
using Newtonsoft.Json;

namespace Ucon.Models
{
    public class KeyManagement

    {
        #region API Key Manager
        //Generates API Keys
        public static string ApiKey()
        {
            string ConnectUrl = Properties.Settings.Default.MidURI + "RequestQuote/RequestLogin";
            
            String LoginInfo = "{Login:{\"username\":\"jotest@test.com\",\"password\":\"P@$$w0rd\"}}";

            WebRequest request = WebRequest.Create(ConnectUrl);
            request.Method = "POST";

            string MyPostData = LoginInfo;
            byte[] ByteArray = Encoding.UTF8.GetBytes(MyPostData);

            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = ByteArray.Length;
            Stream DataStream = request.GetRequestStream();
            DataStream.Write(ByteArray, 0, ByteArray.Length);
            DataStream.Close();

            WebResponse response = request.GetResponse();
            DataStream = response.GetResponseStream();
            
            StreamReader Reader = new StreamReader(DataStream);
            string UnderWriterResponse = Reader.ReadToEnd();

            Reader.Close();
            DataStream.Close();
            response.Close();

            string Key = "";

            JArray MyRes = JArray.Parse(UnderWriterResponse);

           foreach(JToken Keys in MyRes.Children())
            {
                Key = Keys["Response"][0]["key"].ToString();
            }

            return Key;
        }
        #endregion

        #region Execute Methods

        //Execute methods
        public static string ExecuteMethod(string MethodName, String MethodBody)
        {
            string ConnectUrl = Properties.Settings.Default.MidURI + "RequestQuote/" + MethodName;

            WebRequest request = WebRequest.Create(ConnectUrl);
            request.Method = "POST";

            string MyPostData = MethodBody;
            byte[] ByteArray = Encoding.UTF8.GetBytes(MyPostData);

            request.ContentType = "application/x-www-form-urlencoded";
            request.ContentLength = ByteArray.Length;
            Stream DataStream = request.GetRequestStream();
            DataStream.Write(ByteArray, 0, ByteArray.Length);
            DataStream.Close();

            WebResponse response = request.GetResponse();
            DataStream = response.GetResponseStream();

            StreamReader Reader = new StreamReader(DataStream);
            string UnderWriterResponse = Reader.ReadToEnd();
            Reader.Close();
            DataStream.Close();
            response.Close();

            return UnderWriterResponse;
        }

        #endregion

        #region Request Body Representations

        // Single Parameters

        public enum RequestType
        {
        /// <summary>
            /// Only one paramter is being passed
            /// </summary>
        SignleParameter,
        /// <summary>
        /// Only two paramters will be passed
        /// </summary>
        TwoParameters,
        /// <summary>
        /// No Paramters will be passed
        /// </summary>
        NoParameters,
        /// <summary>
        /// An Array or Object is Built and Passed
        /// </summary>
        MultipleParameters,
        /// <summary>
        /// Using the property name Parameter
        /// </summary>
        Parameter,
        //For submitting a Quotation
        Quotation,
        
        //Requesting with Object Body   
        ObjectBody
        }

        /// <summary>
        /// Performs request on underwriter server
        /// </summary>
        /// <param name="MethodName">The name of the method to execute</param>
        /// <param name="ParameterType">The type of parameters you are passing</param>
        /// <param name="RequestJSON">Your JSON request Body</param>
        /// <returns></returns>
       public static string PerformRequest(string MethodName, RequestType ParameterType, string RequestJSON, object TwoParams)
        {
            JObject SingleParam = new JObject();
            JObject KeyObject = new JObject();
            JObject ParameterObject = new JObject();
            
            switch (ParameterType)
            {
                case RequestType.NoParameters:

                    //Key Object
                    KeyObject.Add("key", ApiKey());
                    SingleParam.Add("resquestkey", KeyObject);
                                       
                    return ExecuteMethod(MethodName, JsonConvert.SerializeObject(SingleParam));
                    
                case RequestType.SignleParameter:
                   
                    //Key Object
                    KeyObject.Add("key", ApiKey());
                    SingleParam.Add("resquestkey",KeyObject);

                    //Parameter Object
                    ParameterObject.Add("param", RequestJSON);
                    SingleParam.Add("parameter", ParameterObject);
                   
                    return ExecuteMethod(MethodName, JsonConvert.SerializeObject(SingleParam));

                case RequestType.TwoParameters:
                   
                    return ExecuteMethod(MethodName, JsonConvert.SerializeObject(TwoParams));

                case RequestType.MultipleParameters:
                    return ExecuteMethod(MethodName, RequestJSON);

                case RequestType.Parameter:
                    object Q = JObject.Parse(JsonConvert.SerializeObject(TwoParams));
                    KeyObject.Add("key", ApiKey());
                    SingleParam.Add("resquestkey", KeyObject);
                    SingleParam.Add("parameter", JObject.Parse(JsonConvert.SerializeObject(TwoParams)));

                    return ExecuteMethod(MethodName, JsonConvert.SerializeObject(SingleParam));

                case RequestType.ObjectBody:

                    SingleParam = JObject.Parse(JsonConvert.SerializeObject(TwoParams));
                    KeyObject.Add("key", ApiKey());
                    SingleParam.Add("resquestkey", KeyObject);
                    return ExecuteMethod(MethodName, JsonConvert.SerializeObject(SingleParam));

                case RequestType.Quotation:

                    return ExecuteMethod(MethodName, JsonConvert.SerializeObject(TwoParams));

            }
            return  "{}";
        }



        #endregion



    }
}