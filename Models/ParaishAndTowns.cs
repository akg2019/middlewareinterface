﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class ParaishAndTowns
    {
        public string Country { set; get; }
        public JObject Parish = new JObject();
        public List<string> Lists = new List<string>();
    }
}