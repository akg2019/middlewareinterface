﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ucon.Models
{
    public class SearchTool
    {
        public string SingleParameter { set; get; }
        public string VehicleType { set; get; }
        public string PolicyPrefix { set; get; }
        public bool BooleanParameter { set; get; }

    }
}