﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ucon.Controllers
{
    public class LocalCountriesController : Controller
    {
        // GET: LocalCountries
        public ActionResult Index()
        {
            var LocalCountry = new Models.GetLocalCountries();

            JObject O = JObject.Parse(Models.KeyManagement.PerformRequest("epic_mwGetLOCCountries",
                Models.KeyManagement.RequestType.NoParameters, "All",
                null));


            foreach (JToken T in O["data"])
            {
                LocalCountry.Lists.Add(T.ToString());

            }
            return View(LocalCountry);
        }

        // GET: LocalCountries/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LocalCountries/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LocalCountries/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LocalCountries/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LocalCountries/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LocalCountries/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LocalCountries/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
