﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OmaxFramework;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

namespace Ucon.Models
{
    public class AuditLog
    {
        public int logid { set; get; }
        public int userid { set; get; }
        public DateTime logDate { set; get; }
        public string logAction { set; get; }
        public string comments { set; get; }
        public string errors { set; get; }
        public string requestType { set; get; }
        public string JSONrequest { set; get; }
        public string JSONresponse {set;get;}
        public string importKey { set; get; }
        public string ip { set; get; }
        public string computerName { set; get; }
        public string computerUserName { set; get; }
        public string userName { set; get; }

        public AuditLog() { }


        public static IEnumerable<AuditLog> getAuditLogs(DateTime start, DateTime end, string ip = null)
        {
            DBModel.MiddleAccsEntities db = new DBModel.MiddleAccsEntities();

            //IEnumerable<AuditLog> L ;
            //L = (db.AuditLogs.Where(p => p.LogDate <= end && p.LogDate >= start).ToList());

            return null;
        }


        public static List<AuditLog> getAuditLog(DateTime start = new DateTime(), DateTime end = new DateTime(), string ip = null)
        {
            List<AuditLog> l = new List<AuditLog>();
            string startString = "";
            string endString = "";
            string query = "GetAuditLogs ";

            if (start != new DateTime())
            {
                startString = start.ToString("yyyy-MM-dd H:mm");
                query += "'" + startString + "'";
            }
            else query += "null";

            if (end != new DateTime())
            {
                endString = end.ToString("yyyy-MM-dd H:mm");
                query += ",'" + endString + "'";
            }
            else query += ",null";

            //if (start != new DateTime() && end != new DateTime())
            //{
            //    //string startString = start.ToString("yyyy-MM-dd H:mm");
            //   // string endString = end.ToString("yyyy-MM-dd H:mm");
            //    //query += "'" + startString + "','" + endString + "'";
            //}
            //else
            //{
            //    query += "null,null";
            //}
            if (ip != null && ip != "") query += ",'" + ip + "'";

            try {
                var obj = Utilities.OmaxData.HandleQuery(query, System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);
                JArray logs = JArray.Parse(JsonConvert.SerializeObject(obj));


                //AuditLog al = new AuditLog();

                for (int x = 0; x < logs.Count(); x++)
                {
                    AuditLog al = new AuditLog();
                    al.logid = int.Parse(logs[x]["LogID"].ToString());
                    al.userid = logs[x]["UserID"].ToString() != "" ? int.Parse(logs[x]["UserID"].ToString()) : 0;
                    al.logDate = DateTime.Parse(logs[x]["LogDate"].ToString());
                    al.logAction = logs[x]["LogAction"].ToString();
                    al.comments = logs[x]["Comments"].ToString();
                    al.errors = logs[x]["Errors"].ToString();
                    al.requestType = logs[x]["RequestType"].ToString();
                    al.JSONresponse = logs[x]["JSONResponse"].ToString();
                    al.ip = logs[x]["IP"].ToString();
                    al.computerName = logs[x]["ComputerName"].ToString();
                    al.computerUserName = logs[x]["ComputerUserName"].ToString();
                    al.JSONrequest = logs[x]["JSONRequest"].ToString();

                    l.Add(al);
                }
            }
            catch (OutOfMemoryException ome)
            {
                Console.WriteLine(ome);
            }
            

            return l;
        }


        /// <summary>
        /// New optimized log
        /// </summary>
        /// <param name="start">The start date of your search range</param>
        /// <param name="end">The end date of your search range</param>
        /// <param name="ip">The ip address to search for</param>
        /// <returns></returns>
        public static List<DBModel.GetAuditLogs_Result> GetLogs(DateTime start = new DateTime(), DateTime end = new DateTime(), string ip = null)
        {
            DBModel.MiddleAccsEntities DB = new DBModel.MiddleAccsEntities();
            return DB.GetAuditLogs(start, end, ip).ToList();  
        }

    }
}