﻿
//Paging for Occupations
angular.module('ui.bootstrap.demo').controller('occupationList', function ($scope, $log) {
    $scope.totalItems = 64;
    $scope.currentPage = 4;

    $scope.setPage = function (pageNo) {
        $scope.currentPage = pageNo;
    };

    $scope.pageChanged = function () {
        $log.log('Page changed to: ' + $scope.currentPage);
    };

    $scope.maxSize = 5;
    $scope.bigTotalItems = 175;
    $scope.bigCurrentPage = 1;
});

//General areas
function GetGeneralArea() {
       window.location.replace("GeneralAreas/Index/" + document.getElementById("GenBox").value);
   }
