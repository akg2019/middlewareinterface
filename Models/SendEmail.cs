﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Ucon.Models
{
    class SendEmail
    {
        //Send an email
        public void SendEmails(string Message, string Subject, string SendTo, string FromAddress)
        {
            try
            {
               
                var SmtpSettings = (from S in (new DBModel.MiddleAccsEntities()).SMTP_SERVER select S).Take(1);
                string MailLabel = "Middleware Service";
                string SMTPServer; String ServerUsername; String ServerPassword; String Port;

                SMTPServer = SmtpSettings.SingleOrDefault().ServerUrl;
                ServerUsername = SmtpSettings.SingleOrDefault().Username;
                ServerPassword = SmtpSettings.SingleOrDefault().Password;
                Port = SmtpSettings.SingleOrDefault().PortNumber;

                MailMessage mailMsg = new MailMessage();

                // To
                try
                {
                    if (SendTo.Split(',').Count() > 1)
                    {
                        foreach (string Address in SendTo.Split(','))
                        {
                            mailMsg.To.Add(new MailAddress(Address, ""));
                        }
                    }else
                    {
                        mailMsg.To.Add(new MailAddress(SendTo, ""));
                    }
                }
                catch (Exception)
                {

                }
            
                // From
                mailMsg.From = new MailAddress(FromAddress, MailLabel);

                // Subject and multipart/alternative Body
                mailMsg.Subject = Subject;
                string text = Message;
                string html = @"<p>" + Message + "</p>";
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(text, null, MediaTypeNames.Text.Plain));
                mailMsg.AlternateViews.Add(AlternateView.CreateAlternateViewFromString(html, null, MediaTypeNames.Text.Html));

                // Init SmtpClient and send
                SmtpClient smtpClient = new SmtpClient(SMTPServer, Convert.ToInt32(Port));
                System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(ServerUsername, ServerPassword);
                smtpClient.Credentials = credentials;

                smtpClient.Send(mailMsg);
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }
    }
}
