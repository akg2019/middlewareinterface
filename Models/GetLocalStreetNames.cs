﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class GetLocalStreetNames
    {
        public string SearchCriteria { set; get; }
        public JObject Streets = new JObject();
        public List<string> Lists = new List<string>();
    }
}