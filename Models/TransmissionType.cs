﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class TransmissionType
    {
        public JObject Types = new JObject();
        public List<string> Lists = new List<string>();
    }
}