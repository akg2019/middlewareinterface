﻿//Delete phone number object
function RemovePhoneNumber(id) {
    var PhoneID = { PhoneToDelete: id };

    $.ajax({
        type: "POST",
        url: "RequestQuotation/RemovePhoneNumber",
        data: JSON.stringify(PhoneID),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessDelete,
        error: ErrorDelete
    });

};

function GetParish() {
            $.ajax({
            type: "POST",
            url: "RequestQuotation/Country?CountryT=" + document.getElementById("CX").value,
            data: "{}",
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessData1,
            error: ErrorData1
        });

    };
function SuccessData1(response) {
   
    var P = "";

   
    for (var i = 0; i < response.data.length; i++) {
        P += "<option>" + response.data[i].parish + "</option>";
       
    }
  
    document.getElementById("Parish").innerHTML = P;
    document.getElementById("Parish2").innerHTML = P;
    document.getElementById("parish").innerHTML = P;
    document.getElementById("_parish").innerHTML = P;
}
function ErrorData1() {
    alert("we have errors");
}

// Renders phone numbers after delete
function SuccessDelete(response) {
    var PhoneNumbers = "";
    var Carriers = "";
    var ID = "";

    for (var i = 0; i < response.length; i++) {
        Carriers = response[i].carrier;
        ID = i;
        PhoneNumbers += "<li class = \"ul\" id=\"" + ID + "\" class=\"Phone\"><table><tr><td class = \"TableCells\"><input type = \"button\" value = \"Remove\" class = \"btn btn-danger btn-sm\" id = \"" + ID + "\" onclick = \"RemovePhoneNumber(this.id)\"</td>"
           + "<td>" + response[i].phone_number + " " + Carriers + "</td></tr></table></li>";
    }
    document.getElementById("PhoneArea").innerHTML = PhoneNumbers;
}
function ErrorDelete(response){}

//Adds a phone Number
function AddPhoneNumber() {
    PhoneObject.type = document.getElementById("type").value;
    PhoneObject.carrier = document.getElementById("carrier").value;
    PhoneObject.description = document.getElementById("description").value;
    PhoneObject.extension_or_range = document.getElementById("extension_or_range").value;
    PhoneObject.phone_number = document.getElementById("phone_number").value;

    $.ajax({
        type: "POST",
        url: "RequestQuotation/AddPhoneNumbers",
        data: JSON.stringify(PhoneObject),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessPhoneNumber,
        error: ErrorPhoneNumber
    });

};
var PhoneObject = {
    type: "",
    carrier: "",
    description: "",
    extension_or_range: "",
    phone_number: ""
};

//Renders phone numbers after adding
function SuccessPhoneNumber(response)
{
    var PhoneNumbers = "";
    var Carriers = "";
    var ID = "";
   
    for (var i = 0; i < response.length; i++) {
        Carriers = response[i].carrier;
        ID = i;
        PhoneNumbers += "<li class = \"ul\" id=\"" + ID + "\" class=\"Phone\"><table><tr><td class = \"TableCells\"><input type = \"button\" value = \"Remove\" class = \"btn btn-danger btn-sm\" id = \"" + ID + "\" onclick = \"RemovePhoneNumber(this.id)\"</td>"
            + "<td>" + response[i].phone_number + " " + Carriers + "</td></tr></table></li>";
    }  
    
    document.getElementById("PhoneArea").innerHTML = PhoneNumbers;
    
};
function ErrorPhoneNumber(response) {
   
};

//Drivers
function AddDrivers() {
    var Drivers = {
        "first_name": "",
        "middle_name": "",
        "last_name": "",
        "driver_dob": "",
        "drivers_licence_first_issued": "",
        "drivers_licence_number": "",
        "is_main_driver": ""
    };

    Drivers.first_name = document.getElementById("dfirst_name").value;
    Drivers.middle_name = document.getElementById("dmiddle_name").value;
    Drivers.last_name = document.getElementById("dlast_name").value;
    Drivers.driver_dob = document.getElementById("driver_dob").value;
    Drivers.drivers_licence_first_issued = document.getElementById("drivers_licence_first_issued").value;
    Drivers.drivers_licence_number = document.getElementById("drivers_licence_number").value;
    Drivers.is_main_driver = document.getElementById("is_main_driver").checked;

    

    $.ajax({
        type: "POST",
        url: "RequestQuotation/AddDriver",
        data: JSON.stringify(Drivers),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessDriver,
        error: ErrorDriver
    });
};

//Renders drivers list after adding
function SuccessDriver(response) {
    var DriverList = "";
    var Lic = "";
    for (var i = 0; i < response.length; i++) {
        Lic = response[i].drivers_licence_number;
        ID = i;
        DriverList += "<li class = \"ul\" id=\"" + ID + "\" class=\"Phone\"><table><tr><td class = \"TableCells\"><input type = \"button\" value = \"Remove\" class = \"btn btn-danger btn-sm\" id = \"" + ID + "\" onclick = \"RemoveDriver(this.id)\"</td>"
            + "<td>" + response[i].first_name + " " + response[i].last_name + " <b class = \"LicNumber\">" + Lic + "</b></td></tr></table></li>";
    }
  
    document.getElementById("DriverArea").innerHTML = DriverList;

};
function ErrorDriver(response) { };

// Removes drivers object
function RemoveDriver(id) {
    var DriverID = { DriverToDelete: id };

    $.ajax({
        type: "POST",
        url: "RequestQuotation/RemoveDriver",
        data: JSON.stringify(DriverID),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessDeleteDriver,
        error: ErrorDeleteDriver
    });

};

//Renders drivers after adding
function SuccessDeleteDriver(response) {
    var DriverList = "";
    var Lic = "";
    for (var i = 0; i < response.length; i++) {
        Lic = response[i].drivers_licence_number;
        ID = i;
        DriverList += "<li class = \"ul\" id=\"" + ID + "\" class=\"Phone\"><table><tr><td class = \"TableCells\"><input type = \"button\" value = \"Remove\" class = \"btn btn-danger btn-sm\" id = \"" + ID + "\" onclick = \"RemoveDriver(this.id)\"</td>"
            + "<td>" + response[i].first_name + " " + response[i].last_name + " <b class = \"LicNumber\">" + Lic + "</b></td></tr></table></li>";
    }

    document.getElementById("DriverArea").innerHTML = DriverList;
}
function ErrorDeleteDriver(response) { };

//Manual Rates
function AddManualRate() {
var code = document.getElementById("code").value;
var value = document.getElementById("value").value;
var ManualRates = {
    "code": code,
    "value": value
};
$.ajax({
    type: "POST",
    url: "RequestQuotation/AddManualRate",
    data: JSON.stringify(ManualRates),
    contentType: "application/json; charset-utf-8",
    dataType: "json",
    success: SuccessManualRate,
    error: ErrorManualRate
});
}

//Renders Manual Rates after adding
function SuccessManualRate(response) {
    var RatesList = "";
    var Codes = "";
    var Values = "";

    for (var i = 0; i < response.length; i++) {
        Codes = response[i].code;
        Values = response[i].value
        ID = i;
        RatesList += "<li class = \"ul\" id=\"" + ID + "\" class=\"Phone\"><table><tr><td class = \"TableCells\"><input type = \"button\" value = \"Remove\" class = \"btn btn-danger btn-sm\" id = \"" + ID + "\" onclick = \"RemoveRate(this.id)\"</td>"
            + "<td>" + Codes + " " + Values + "</td></tr></table></li>";
    }

    document.getElementById("RatesArea").innerHTML = RatesList;
};
function ErrorManualRate(response) { };

//Remove Manual Rate
function RemoveRate(id) {
    var RateID = { RateToDelete: id };

    $.ajax({
        type: "POST",
        url: "RequestQuotation/RemoveManualRate",
        data: JSON.stringify(RateID),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessDeleteRate,
        error: ErrorDeleteRate
    });

};

//Rederes list of rates after delete
function SuccessDeleteRate(response)
{
    var RatesList = "";
    var Codes = "";
    var Values = "";

    for (var i = 0; i < response.length; i++) {
        Codes = response[i].code;
        Values = response[i].code
        ID = i;
        RatesList += "<li class = \"ul\" id=\"" + ID + "\" class=\"Phone\"><table><tr><td class = \"TableCells\"><input type = \"button\" value = \"Remove\" class = \"btn btn-danger btn-sm\" id = \"" + ID + "\" onclick = \"RemoveRate(this.id)\"</td>"
            + "<td>" + Codes + " " + Values + "</td></tr></table></li>";
    }

    document.getElementById("RatesArea").innerHTML = RatesList;
}
function ErrorDeleteRate(response) { };

//POST A RISK/ADD A RISK OBJECT
function AddRisk() {
    
    //Risks
    var sum_insured = document.getElementById("sum_insured").value;
    var usage_code = document.getElementById("usage_code").value;
    var authorized_driver_wording = document.getElementById("authorized_driver_wording").value;
    var year_for_rating = document.getElementById("year_for_rating").value;
    var year = document.getElementById("year").value;
    var make = document.getElementById("make").value
    var model = document.getElementById("model").value; 
    var model_type =  document.getElementById("model_type").value;
    var extension =  document.getElementById("extension").value;
    var body_type =  document.getElementById("body_type").value; 
    var body_shape = document.getElementById("body_shape").value; 
    var hp_cc_rating = document.getElementById("hp_cc_rating").value;
    var hp_cc_unit_type = document.getElementById("hp_cc_unit_type").value;
    var chassis_number = document.getElementById("chassis_number").value;
    var colour =  document.getElementById("colour").value;
    var number_of_cyclinders = document.getElementById("number_of_cyclinders").value;
    var engine_modified = document.getElementById("engine_modified").checked;
    var engine_number = document.getElementById("engine_number").value;
    var engine_type = document.getElementById("engine_type").value;
    var has_electric_doors = document.getElementById("has_electric_doors").checked;
    var has_electric_side_mirror = document.getElementById("has_electric_side_mirror").checked;
    var has_electric_window = document.getElementById("has_electric_window").checked;
    var has_power_steering = document.getElementById("has_power_steering").checked;
    var import_type = document.getElementById("import_type").value;
    var ncd_percent = document.getElementById("ncd_percent").value;
    var left_hand_drive = document.getElementById("left_hand_drive").checked;
    var mileage = document.getElementById("mileage").value;
    var mileage_type = document.getElementById("mileage_type").value;
    var number_of_doors = document.getElementById("number_of_doors").value;
    var number_of_engines = document.getElementById("number_of_engines").value;
    var registration_number = document.getElementById("registration_number").value;
    var roof_type = document.getElementById("roof_type").value;
    var seat_type = document.getElementById("seat_type").value;
    var seating = document.getElementById("seating").value;
    var tonnage = document.getElementById("tonnage").value;
    var transmission_type = document.getElementById("transmission_type").value; 
    var main_driver_licence_first_issued = document.getElementById("main_driver_licence_first_issued").value;


    //Vehicle Location

    var altitude = document.getElementById("altitude").value;
    var block = document.getElementById("block").value;
    var building_number = document.getElementById("building_number").value;
    var complex_name = document.getElementById("complex_name").value;
    var country = document.getElementById("country").value;
    var general_area = document.getElementById("general_area").value;
    var international_area = document.getElementById("international_area").value;
    var is_international_address = document.getElementById("is_international_address").value;
    var island = document.getElementById("island").value;
    var latitude = document.getElementById("latitude").value;
    var longitude = document.getElementById("longitude").value;
    var parish = document.getElementById("parish").value;
    var postal_zip_code = document.getElementById("postal_zip_code").value;
    var street_name = document.getElementById("street_name").value;
    var street_number = document.getElementById("street_number").value;
    var street_type = document.getElementById("street_type").value;
    var town = document.getElementById("town").value;
    var unit_number = document.getElementById("unit_number").value;
    var categorization = document.getElementById("categorization").value;
    var locaction_class = document.getElementById("categorization").value;

    //Validation
    if (model == "Select Model" || model == "") { model = document.getElementById("modelc").value; }
    if (model_type == "Select Model Type" || model_type == "") { model_type = document.getElementById("model_typec").value; }
    if (extension == "Select Extension" || extension == "") { extension = document.getElementById("extensionc").value }
    if (body_type == "Select Body Type" || body_type == "") { body_type = document.getElementById("body_typec").value; }
    if (body_shape == "Select Body Shape" || body_shape == "") { body_shape = document.getElementById("body_shapec").value; }
    if (make == "Select Make" || make == "") { make = document.getElementById("makec").value; }
    if (colour == "Select Vehicle Color" || colour == "") { colour = document.getElementById("colourc").value; }
    if (roof_type == "Select Roof Type" || roof_type == "") { roof_type = document.getElementById("roof_typec").value; }
        

   //Risk Object
    var risk = {
        "sum_insured": sum_insured,
        "usage_code": usage_code,
        "authorized_driver_wording": authorized_driver_wording,
        "year_for_rating": year_for_rating,
        "year": year,
        "make": make,
        "model": model,
        "model_type": model_type,
        "extension": extension,
        "body_type": body_type,
        "body_shape": body_shape,
        "hp_cc_rating": hp_cc_rating,
        "hp_cc_unit_type": hp_cc_unit_type,
        "cert_type": "",
        "chassis_number": chassis_number,
        "colour": colour,
        "number_of_cyclinders": number_of_cyclinders,
        "engine_modified": engine_modified,
        "engine_number": engine_number,
        "engine_type": engine_type,
        "has_electric_doors": has_electric_doors,
        "has_electric_side_mirror": has_electric_side_mirror,
        "has_electric_window": has_electric_window,
        "has_power_steering": has_power_steering,
        "import_type": import_type,
        "ncd_percent": ncd_percent,
        "left_hand_drive": left_hand_drive,
        "mileage": mileage,
        "mileage_type": mileage_type,
        "number_of_doors": number_of_doors,
        "number_of_engines": number_of_engines,
        "registration_number": registration_number,
        "roof_type": roof_type,
        "seat_type": seat_type,
        "seating": seating,
        "tonnage": tonnage,
        "transmission_type": transmission_type,
        "altitude": altitude,
        "block": block,
        "building_number": building_number,
        "country": country,
        "general_area": general_area,
        "international_area": international_area,
        "is_international_address": is_international_address,
        "island": island,
        "latitude": latitude,
        "longitude": longitude,
        "parish": parish,
        "postal_zip_code": postal_zip_code,
        "street_name": street_name,
        "street_number": street_number,
        "street_type": street_type,
        "town": town,
        "unit_number": unit_number,
        "main_driver_licence_first_issued": main_driver_licence_first_issued,
        "categorization": categorization,
        "complex_name": complex_name,
        "locaction_class": locaction_class
    };

    
    $.ajax({
        type: "POST",
        url: "RequestQuotation/AddRisk",
        data: JSON.stringify(risk),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessRisk,
        error: ErrorRisk
    });

};
function SuccessRisk(response) {};
function ErrorRisk(response) {};

//Get Vehicle Models
function RequestModels() {
        $.ajax({
        type: "POST",
        url: "RequestQuotation/GetModels",
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessModels,
        error: ErrorModels
    });

        //GetVehicleColor();
        //RequestUsages();
        //RequestTransmissionTypes();
        //RequestRoofTypes();
        //RequestImportTypes();
       //PolicyPrefix();
 }

var Makes = "[ ";
function SuccessModels(response) {

    var ColorList = "<option>Select Make</option>";
    var Options = "";
    var Models = "<option>Select Model</option>";
    var ModelType = "<option>Select Model Type</option>";
    var Extension = "<option>Select Extension</option>";
    var BodyType = "<option>Select Body Type</option>";
    var BodyShape = "<option>Select Body Shape</option>";

    if (response.data != undefined) {
 for (var i = 0; i < response.data.length; i++) {

        if (response.data[i].make !== "") { ColorList += "<option>" + response.data[i].make + "</option>"; }

        for (var j = 0; j < response.data[i].models.length; j++) {

            if (response.data[i].models[j].model !== "") { Models += "<option>" + response.data[i].models[j].model + "</option>"; }
            if (response.data[i].models[j].model_type !== "") { ModelType += "<option>" + response.data[i].models[j].model_type + "</option>"; }
            if (response.data[i].models[j].extension !== "") { Extension += "<option>" + response.data[i].models[j].extension + "</option>"; }
            if (response.data[i].models[j].body_type !== "") { BodyType += "<option>" + response.data[i].models[j].body_type + "</option>"; }
            if (response.data[i].models[j].body_shape !== "") { BodyShape += "<option>" + response.data[i].models[j].body_shape + "</option>"; }

        }

        if (i != response.data.length) {
            Makes += "\"" + response.data[i].make + "\",";
        } else {
            Makes += "\"" + response.data[i].make + "\"";
        }
    }

    document.getElementById("make").innerHTML = ColorList;
    //document.getElementById("model").innerHTML = Models;
    //document.getElementById("model_type").innerHTML = ModelType;
    //document.getElementById("extension").innerHTML = Extension;
    //document.getElementById("body_type").innerHTML = BodyType;
    //document.getElementById("body_shape").innerHTML = BodyShape;

    Makes += "]";
   
    }

   
    
};
function ErrorModels(response) { var i = response; };

//Drop Down List Populated
function GetVehicleColor() {
       $.ajax({
        type: "POST",
        url: "RequestQuotation/GetVehicleColor",
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessMethod,
        error: ErrorMethod
    });

}
function SuccessMethod(response) {
    //Populates Colors on Motor Quote Page
        var ColorList = "<option>Select Vehicle Color</option>";
        var Options = "";
        if (response.data != undefined) {
for (var i = 0; i < response.data.length; i++) {
            Options = "<option>" + response.data[i] + "</option>";
            ColorList += Options;
        }
        document.getElementById("colour").innerHTML = ColorList;
        }
        
             
}
function ErrorMethod(response) {
    
    //document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">Failed to connect to UnderWriter (:</div>";
}

//Get Usages
function RequestUsages() {
       $.ajax({
        type: "POST",
        url: "RequestQuotation/GetUsages?PolicyPrefix=" + document.getElementById("policy_prefix").value,
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessUsages,
        error: ErrorUsages
    });

}
function SuccessUsages(response) {
    var ColorList = "<option>Select Usage Code</option>";
    var Options = "";

    if (response.data != undefined) {
    for (var i = 0; i < response.data.length; i++) {
        Options = "<option>" + response.data[i].code + " <" + response.data[i].description + ">" + "</option>";
        ColorList += Options;
    }
    document.getElementById("usage_code").innerHTML = ColorList;
    }
   
}
function ErrorUsages(response) {
    //alert(response.statusText);
}

//Get Import Types
function RequestImportTypes() {
        $.ajax({
        type: "POST",
        url: "RequestQuotation/VehImportType",
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessImportTypes,
        error: ErrorImportTypes
    });

}
function SuccessImportTypes(response) {
    var ColorList = "<option>Select Import Type</option>";
    var Options = "";
    if (response.data != undefined) {
    for (var i = 0; i < response.data.length; i++) {
        Options = "<option>" + response.data[i] + "</option>";
        ColorList += Options;
    }
    document.getElementById("import_type").innerHTML = ColorList;
    }
    
}
function ErrorImportTypes(response) {
    //alert(response.statusText);
}

//Get Roof Types
function RequestRoofTypes() {
        $.ajax({
        type: "POST",
        url: "RequestQuotation/VehRoofType",
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessRoofTypes,
        error: ErrorRoofTypes
    });

}
function SuccessRoofTypes(response) {
    var ColorList = "<option>Select Roof Type</option>";
    var Options = "";
    if (response.data != undefined) {
 for (var i = 0; i < response.data.length; i++) {
        Options = "<option>" + response.data[i] + "</option>";
        ColorList += Options;
    }
    document.getElementById("roof_type").innerHTML = ColorList;
    }
   
}
function ErrorRoofTypes(response) {
    //alert(response.statusText);
}

//Gets Transmission Types
function RequestTransmissionTypes() {
       $.ajax({
        type: "POST",
        url: "RequestQuotation/VehTransmissionType",
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessTransmissionTypes,
        error: ErrorTransmissionTypes
    });

}
function SuccessTransmissionTypes(response) {
    var ColorList = "<option>Select Transmission Type</option>";
    var Options = "";
    if (response.data != undefined) {
for (var i = 0; i < response.data.length; i++) {
        Options = "<option>" + response.data[i] + "</option>";
        ColorList += Options;
    }
    document.getElementById("transmission_type").innerHTML = ColorList;
    }

    
}
function ErrorTransmissionTypes(response) {
    //alert(response.statusText);
};

function GetAssociatedModels() {
    $.ajax({
        type: "POST",
        url: "RequestQuotation/GetDropDownData?Make=" + document.getElementById("make").value,
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessData,
        error: ErrorData
    });
}

function SuccessData(response) {

    var ColorList = "<option>Select Make</option    >";
    var Options = "";
    var Models = "<option>Select Model</option>";
    var ModelType = "<option>Select Model Type</option>";
    var Extension = "<option>Select Extension</option>";
    var BodyType = "<option>Select Body Type</option>";
    var BodyShape = "<option>Select Body Shape</option>";
          

    for (var i = 0; i < response.Models.length; i++) {
        if (response.Models[i].trim() !== "") { if (Models.indexOf(response.Models[i].trim()) < 1) { Models += "<option>" + response.Models[i].trim() + "</option>"; } }
     }

    for (var j = 0; j < response.ModelTypes.length; j++) {
        if (response.ModelTypes[j].trim() !== "") { if( ModelType.indexOf(response.ModelTypes[j].trim()) < 1) { ModelType += "<option>" + response.ModelTypes[j].trim() + "</option>";} }
    }

    for (var k = 0; k < response.BodyShapes.length; k++) {
        if (response.BodyShapes[k].trim() !== "") { if (BodyShape.indexOf(response.BodyShapes[k].trim()) < 1) { BodyShape += "<option>" + response.BodyShapes[k].trim() + "</option>"; } }
    }

    for (var l = 0; l < response.BodyTypes.length; l++) {
        if (response.BodyTypes[l].trim() !== "") { if (BodyType.indexOf(response.BodyTypes[l].trim()) < 1) { BodyType += "<option>" + response.BodyTypes[l].trim() + "</option>"; } }
    }

    for (var m = 0; m < response.Extensions.length; m++) {
        if (response.Extensions[m].trim() !== "") { if (Extension.indexOf(response.Extensions[m].trim()) < 1) { Extension += "<option>" + response.Extensions[m].trim() + "</option>"; } }
    }

        document.getElementById("model").innerHTML = Models;
        document.getElementById("model_type").innerHTML = ModelType;
        document.getElementById("extension").innerHTML = Extension;
        document.getElementById("body_type").innerHTML = BodyType;
        document.getElementById("body_shape").innerHTML = BodyShape;

}

function ErrorData() { };

function AddParish(Parish) {
    try {
        if (Parish == "Main") {
        document.getElementById("_parish").value = document.getElementById("Parish").value;
        } else {
            document.getElementById("parish").value = document.getElementById("Parish2").value;
        }
        
    } catch (e) {

    }
}

function PolicyPrefix() {
    $.ajax({
        type: "POST",
        url: "RequestQuotation/GetNamedDrivers?PolicyPrefix=" + document.getElementById("policy_prefix").value,
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessAuthDrivers,
        error: ErrorAuthDrivers
    });
}

function SuccessAuthDrivers(response)
{
    var DriverList = "<option>Select Authorized Driver</option>";
    try {
        for (var i = 0; i <response.data.length; i++) {
        DriverList += "<option>" + response.data[i] + "</option>";
    }
    } catch (e) {

    }
   
    document.getElementById("authorized_driver_wording").innerHTML = DriverList;

     RequestUsages();
}

function ErrorAuthDrivers()
{

}

$(function () {

    try {
  //var elemClass;
    $('.dropdown-submenu a.groupname').click(function (e) {
        $('.dropdown-submenu ul').hide();
       $(this).next('ul').toggle();
        //elemClass = $(this).next('ul').attr('class');
        e.stopImmediatePropagation();
        e.preventDefault();
    });
    } catch (e) {

    }
  
});

$(document).on('click', "#SearchBrokers", function () {
    location.href = "BrokerServers?BrokerName=" + document.getElementById("BrokerSearchName").value;
});

//RequestModels();
//This function does the mapping for Brokers
var BrokersObject = {
    BrokerID: "",
    BrokerName: "",
    URL: "",
    CompanyCode: ""
}

//Adds a new Broker
function AddNewBroker() {

    BrokersObject.BrokerID = "";
    BrokersObject.BrokerName = "";
    BrokersObject.CompanyCode = "";
    BrokersObject.URL = "";



    $.ajax({
        type: "POST",
        url: "RequestQuotation/EditServerSave",
        data: JSON.stringify(BrokersObject),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessAuthDrivers,
        error: ErrorAuthDrivers
    });

}

///This is Policy create section
//Insured Portion
var Insured = [
           {
               "is_main_insured": true,
               "national_id": "14584545432578212",
               "national_id_type": "Voter's ID"
           }
]

//Manual Rates Section
var manual_limits = [
			{
			    "limit_number": 12,
			    "limit_amount": 10
			}
]

//Main Policy Object
var PolicyObject = {
    "resquestkey": {
        "key": "10740677-b2ca-409d-9844-c2bf9f5ca467"
    },
    "create_certificate": false,
    "create_cover_note": false,
    "cover_note_details": {
        "effective_date": "05/09/2015 00:00:00",
        "days_effective": 0,
        "manual_number": 0
    },
    "policy": {
        "external_reference_number": "0021458",
        "agreed_value": false,
        "auto_renewal": false,
        "branch": "Main Office",
        "currency": "",
        "date_proposal_signed": "09/10/2016",
        "end_date": "09/10/2017 00:00:00",
        "memo": "",
        "occupancy": "",
        "policy_prefix": "MCPC",
        "limit_group": "",
        "source_global_name_id": "0",
        "start_date": "09/10/2016 00:00:00",
        "usage_category": "",
        "premium_finance_company_code": "",
        "insureds": [

        ],
        "manual_limits": [

        ]
    },
    "global_names": [
		{
		    "global_name_id": "0",
		    "global_name_number": 0,
		    "company_name": "COmpany 1",
		    "dob": "09/10/19806",
		    "drivers_licence_country": "",
		    "drivers_licence_date_issued": "09/10/2001",
		    "drivers_licence_expiry_date": "09/10/2018",
		    "drivers_licence_first_issued": "09/10/1997",
		    "drivers_licence_number": "65345345656",
		    "drivers_licence_type": "General",
		    "email_address": "johnbrown@yahoo.com",
		    "employment_type": "Full Time",
		    "first_name": "John",
		    "gender": "Male",
		    "is_a_company": false,
		    "last_name": "Brown",
		    "locations": [
				{
				    "location_id": "",
				    "is_main_location": false,
				    "altitude": "",
				    "block": "27",
				    "building_label": "",
				    "building_number": "",
				    "complex_name": "",
				    "country": "Jamaica",
				    "general_area": "",
				    "international_area": "",
				    "is_international_address": "",
				    "island": "",
				    "latitude": "",
				    "longitude": "",
				    "parish": "Kingston",
				    "postal_zip_code": "",
				    "street_name": "Mona",
				    "street_number": "27",
				    "street_type": "Road",
				    "town": "Mona Heights",
				    "unit_number": "77",
				    "phone_numbers": [
						{
						    "phone_number_id": "0",
						    "type": "Work",
						    "carrier": "Flow",
						    "extension_or_range": "4588",
						    "phone_number": "1876-845-7458"
						},
						{
						    "phone_number_id": "1",
						    "type": "Cell",
						    "carrier": "Digicel",
						    "extension_or_range": "4572",
						    "phone_number": "875-452-7858"
						}
				    ]
				}],
		    "maiden_name": "",
		    "mailing_name": "John Brown",
		    "marital_status": "Married",
		    "middle_name": "David",
		    "national_id": "124595689512",
		    "national_id_type": "Voter's ID",
		    "nationality": "Jamaican",
		    "notes": "N/A",
		    "occupation": "Accountant",
		    "place_of_birth": "Kingston",
		    "tax_id_number": "1546566536",
		    "title": "Mr",
		    "phone_numbers": [
				{
				    "phone_number_id": "7",
				    "type": "Work",
				    "carrier": "Digicel",
				    "extension_or_range": "1245",
				    "phone_number": "125-788-7459"
				},
				{
				    "phone_number_id": "8",
				    "type": "Work",
				    "carrier": "Flow",
				    "extension_or_range": "7448",
				    "phone_number": "587-859-7456"
				}
		    ]
		},
		{
		    "global_name_id": "0",
		    "global_name_number": 0,
		    "company_name": "Company 2",
		    "dob": "09/10/1982",
		    "drivers_licence_country": "",
		    "drivers_licence_date_issued": "09/10/2000",
		    "drivers_licence_expiry_date": "09/10/2017",
		    "drivers_licence_first_issued": "09/10/1997",
		    "drivers_licence_number": "65956895685",
		    "drivers_licence_type": "Private",
		    "email_address": "susanqueen@hitmail.com",
		    "employment_type": "Full Time",
		    "first_name": "Susan",
		    "gender": "Female",
		    "is_a_company": false,
		    "last_name": "Queen",
		    "locations": [
				{
				    "location_id": "",
				    "is_main_location": false,
				    "altitude": "",
				    "block": "85",
				    "building_label": "",
				    "building_number": "",
				    "complex_name": "",
				    "country": "Jamaica",
				    "general_area": "",
				    "international_area": "",
				    "is_international_address": "",
				    "island": "",
				    "latitude": "",
				    "longitude": "",
				    "parish": "Portland",
				    "postal_zip_code": "",
				    "street_name": "Boundbrook",
				    "street_number": "26",
				    "street_type": "Road",
				    "town": "Alverna",
				    "unit_number": "28",
				    "phone_numbers": [
						{
						    "phone_number_id": "8",
						    "type": "Work",
						    "carrier": "Flow",
						    "extension_or_range": "5694",
						    "phone_number": "145-859-7458"
						},
						{
						    "phone_number_id": "9",
						    "type": "Cell",
						    "carrier": "Flow",
						    "extension_or_range": "4868",
						    "phone_number": "789-451-4589"
						}
				    ]
				},
				{
				    "location_id": "",
				    "is_main_location": false,
				    "altitude": "",
				    "block": "90",
				    "building_label": "",
				    "building_number": "",
				    "complex_name": "",
				    "country": "Jamaica",
				    "general_area": "",
				    "international_area": "",
				    "is_international_address": "",
				    "island": "",
				    "latitude": "",
				    "longitude": "",
				    "parish": "St. Thomas",
				    "postal_zip_code": "",
				    "street_name": "Lens",
				    "street_number": "45",
				    "street_type": "Avenue",
				    "town": "Barrow",
				    "unit_number": "14",
				    "phone_numbers": [
						{
						    "phone_number_id": "9",
						    "type": "Home",
						    "carrier": "Digicel",
						    "extension_or_range": "7893",
						    "phone_number": "876-346-3774"
						},
						{
						    "phone_number_id": "10",
						    "type": "Work",
						    "carrier": "Flow",
						    "extension_or_range": "2324",
						    "phone_number": "876-908-6438"
						}
				    ]
				},
				{
				    "location_id": "",
				    "is_main_location": false,
				    "altitude": "",
				    "block": "",
				    "building_label": "",
				    "building_number": "",
				    "complex_name": "",
				    "country": "Jamaica",
				    "general_area": "",
				    "international_area": "",
				    "is_international_address": "",
				    "island": "",
				    "latitude": "",
				    "longitude": "",
				    "parish": "Clarendon",
				    "postal_zip_code": "",
				    "street_name": "Mapen",
				    "street_number": "90",
				    "street_type": "Road",
				    "town": "Clancart",
				    "unit_number": "15",
				    "phone_numbers": [
						{
						    "phone_number_id": "11",
						    "type": "Home",
						    "carrier": "Digicel",
						    "extension_or_range": "9475",
						    "phone_number": "876-901-2322"
						},
						{
						    "phone_number_id": "12",
						    "type": "Work",
						    "carrier": "Flow",
						    "extension_or_range": "2901",
						    "phone_number": "876-997-1177"
						}
				    ]
				}
		    ],
		    "maiden_name": "Maron",
		    "mailing_name": "Seena Maron",
		    "marital_status": "Married",
		    "middle_name": "Kenra",
		    "national_id": "27846343",
		    "national_id_type": "Voter Id",
		    "nationality": "Jamaican",
		    "notes": "",
		    "occupation": "Higgler",
		    "place_of_birth": "Victoria Jubilee",
		    "tax_id_number": "1890274723",
		    "title": "Miss",
		    "phone_numbers": [
				{
				    "phone_number_id": "13",
				    "type": "Home",
				    "carrier": "Digicel",
				    "extension_or_range": "100",
				    "phone_number": "876-362-2273"
				},
				{
				    "phone_number_id": "14",
				    "type": "Work",
				    "carrier": "Flow",
				    "extension_or_range": "100",
				    "phone_number": "876-245-9090"
				}
		    ]
		}
    ],
    "risks": [
		{
		    "risk_id": "",
		    "sum_insured": 150000,
		    "usage_code": "USE",
		    "authorized_driver_wording": "MAY GET INSURED",
		    "year_for_rating": 2000,
		    "year": 2000,
		    "make": "Missu",
		    "model": "Xperia",
		    "model_type": "Lenvar",
		    "extension": "83232",
		    "body_type": "Sudan",
		    "body_shape": "Round",
		    "hp_cc_rating": 10,
		    "hp_cc_unit_type": "Type",
		    "chassis_number": "903KBJ744NER12",
		    "colour": "White",
		    "number_of_cyclinders": 2,
		    "engine_modified": false,
		    "engine_number": "18273323",
		    "engine_type": "Diesel",
		    "has_electric_doors": false,
		    "has_electric_side_mirror": false,
		    "has_electric_window": false,
		    "has_power_steering": false,
		    "import_type": "",
		    "left_hand_drive": false,
		    "main_driver_dob": "12/05/1960",
		    "main_drivers_licence_first_issued": "10/10/1999",
		    "mileage": 1900,
		    "mileage_type": "",
		    "number_of_doors": 4,
		    "number_of_engines": 2,
		    "registration_number": "7890PP",
		    "roof_type": "Hard",
		    "seat_type": "Soft",
		    "seating": 5,
		    "tonnage": 5,
		    "transmission_type": "automatic",
		    "type_of_license": "General",
		    "vehicle_registered_location": "Kingston",
		    "drivers": [
				{
				    "is_excluded": false,
				    "is_included_by_exception": false,
				    "is_main_driver": false,
				    "national_id": "93773523",
				    "national_id_type": "Passport",
				    "relation_to_insured": "Brother"
				},
				{
				    "is_excluded": false,
				    "is_included_by_exception": false,
				    "is_main_driver": false,
				    "national_id": "1232234",
				    "national_id_type": "Voter Id",
				    "relation_to_insured": "Mother"
				}

		    ],
		    "manual_rates": [
				{
				    "code": "UPKL",
				    "value": 100
				},
				{
				    "code": "HERG",
				    "value": 250
				},
				{
				    "code": "MNP",
				    "value": 300
				},
				{
				    "code": "CASI",
				    "value": 189
				}
		    ],
		    "mortgagees": [
				{
				    "branch": "Half-Way-Tree",
				    "currency": "JA",
				    "national_id": "7028934",
				    "national_id_type": "Voter Id",
				    "notes": "",
				    "original_loan_amount": 150000
				}
		    ]
		},
		{
		    "risk_id": "",
		    "sum_insured": 1700000,
		    "usage_code": "USAGE",
		    "authorized_driver_wording": "Insured Today",
		    "year_for_rating": 2011,
		    "year": 2005,
		    "make": "Honda",
		    "model": "Civic",
		    "model_type": "Lean",
		    "extension": "2837",
		    "body_type": "Sedan",
		    "body_shape": "Flat",
		    "hp_cc_rating": 3,
		    "hp_cc_unit_type": "Power",
		    "chassis_number": "34NM8343",
		    "colour": "Red",
		    "number_of_cyclinders": 2,
		    "engine_modified": false,
		    "engine_number": "2",
		    "engine_type": "Engine",
		    "has_electric_doors": false,
		    "has_electric_side_mirror": false,
		    "has_electric_window": false,
		    "has_power_steering": false,
		    "import_type": "",
		    "left_hand_drive": false,
		    "main_driver_dob": "05/07/1970",
		    "main_drivers_licence_first_issued": "03/01/2001",
		    "mileage": 49530,
		    "mileage_type": "",
		    "number_of_doors": 4,
		    "number_of_engines": 2,
		    "registration_number": "GHKO78",
		    "roof_type": "Hard",
		    "seat_type": "Soft",
		    "seating": 5,
		    "tonnage": 1,
		    "transmission_type": "Manual",
		    "type_of_license": "DL",
		    "vehicle_registered_location": "New Kingston",
		    "drivers": [
				{
				    "is_excluded": false,
				    "is_included_by_exception": false,
				    "is_main_driver": false,
				    "national_id": "24537",
				    "national_id_type": "DL",
				    "relation_to_insured": "Aunt"
				},
				{
				    "is_excluded": false,
				    "is_included_by_exception": false,
				    "is_main_driver": false,
				    "national_id": "467683",
				    "national_id_type": "Voter Id",
				    "relation_to_insured": "Cousin"
				}
		    ],
		    "manual_rates": [
				{
				    "code": "NGG",
				    "value": 200
				},
				{
				    "code": "BVT",
				    "value": 700
				},
				{
				    "code": "IOP",
				    "value": 900
				},
				{
				    "code": "MNN",
				    "value": 500
				}
		    ],
		    "mortgagees": [
				{
				    "branch": "Mandeville",
				    "currency": "US",
				    "national_id": "999943",
				    "national_id_type": "Passport",
				    "notes": "",
				    "original_loan_amount": 200000
				}
		    ]
		}
    ]
};
///End of Policy Create Section


//JSON formatter start here
function output(inp) {
    //inp = $('.Outs').html();
    $('.Outs').append(document.createElement('pre').innerHTML = inp);
}

function syntaxHighlight(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    //json = json.replace("{", "").replace("}", "").replace("[", "").replace("]", "");
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}


function GetPretty() {
var obj = $('.Outs').html();
var str = obj;

//output(str);
output(syntaxHighlight(str));
}


//JSON formatter ends here
var app = angular.module('Users', []);
app.controller('myCtrl', function ($scope, $http) {
    //Gets users
    $http.get("ManageUsers")
    .then(function (response) {
        $scope.Users = response.data;
    });

    //Gets user Sources
    $http.get("GetUserSources")
    .then(function (response) {
        $scope.UserSources = response.data;
    });


    //Gets user registry Sources
    $http.get("epic_mwGetDepartments")
        .then(function (response) {
            $scope.RegistryDepartments = response.data;
        });


    $scope.EditUser = function (UserID, SourceNames, Email, EDISource, registry_destination) {
        $scope.UserID = UserID;
        $scope.SourceName = SourceNames;
        $scope.Email = Email;
        $scope.ErrorMessage = "";
        $scope.ErrorMessages = "";
        $scope.Password = "";
        $scope.ConfirmPassword = "";
        $scope.EDISource = EDISource;
        $scope.registry_destination = registry_destination;
    }

    $scope.ClearFields = function () {
        $scope.UserID = "";
        $scope.SourceName = "";
        $scope.Email = "";
        $scope.ErrorMessage = "";
        $scope.ErrorMessages = "";
        $scope.Password = "";
        $scope.ConfirmPassword = "";
        $scope.EDISource = "";
        $scope.registry_destination = "";
    }

    $scope.Match = function () {
        if ($scope.Password != $scope.ConfirmPassword) {
            $("#Error").removeClass("Success").addClass("Error");
            $scope.ErrorMessage = "Password and Confirm Password does not match";
            $("#Errors").removeClass("Success").addClass("Error");
            $scope.ErrorMessages = "Password and Confirm Password does not match";
        } else {
            $scope.ErrorMessage = "";
            $scope.ErrorMessages = "";
        }
    }

    $scope.SaveUser = function () {
        //Saves users
        $http.get("SaveUsers?UserID=" + $scope.UserID + "&NewSource=" + $scope.SourceName + "&UserType=" + $scope.UserType + "&EDISource=" + $scope.EDISource + "&registry_destination=" + $scope.registry_destination)
        .then(function (response) {

            //$modal("#EditUser").close;
            $http.get("ManageUsers")
               .then(function (response) {
                   $scope.Users = response.data;
               });
        });
    }

    $scope.ResetPassword = function () {
            //Saves users
        $http.get("ResetUserPassword?Email=" + $scope.Email + "&NewPassword=" + $scope.Password)
            .then(function (response) {
                if (response.data.success == true) {
                    $("#Errors").removeClass("Error").addClass("Success");
                    $scope.ErrorMessages = "Password reset was successful.";
                    $scope.ErrorMessage = "";
                } else {
                    $("#Errors").removeClass("Success").addClass("Error");
                    $scope.ErrorMessages = response.data.Error[0];
                    $scope.ErrorMessage = "";

                };
            });
        }
    
    $scope.EmailCheck = function () {

        if (!$scope.Email.includes("@") || !$scope.Email.includes(".")) {
            $("#Error").removeClass("Success").addClass("Error");
            $scope.ErrorMessage = "Invalid email";
        } else {
            if ($scope.Email.substring($scope.Email.lastIndexOf(".")).length >= 3) {
                //Email address is ok
                $scope.ErrorMessage = "";
            } else {
                $("#Error").removeClass("Success").addClass("Error");
                $scope.ErrorMessage = "Invalid email";
            }
        }
            
    }

    $scope.AddNewUser = function () {

        if ($scope.Password != $scope.ConfirmPassword) {
            $("#Error").removeClass("Success").addClass("Error");
            $scope.ErrorMessage = "Password and Confirm Password does not match";
        } else {
                 $scope.ErrorMessage = "";
                 $http.get("RegisterNewUser?Email=" + $scope.Email + "&Password=" + $scope.Password)
                .then(function (response) {

                    if (response.data.success == false) {
                        $scope.ErrorMessage = response.data.ErrorMessage[0];
                    } else {
                        
                        $("#Error").removeClass("Error").addClass("Success");
                        $scope.ErrorMessage = "User Created successfully";
                        //$modal("#EditUser").close;
                        $http.get("ManageUsers")
                           .then(function (response) {
                               $scope.Users = response.data;
                               try {
                                   $("#NewUser").modal('toggle');
                               } catch (e) {

                               }
                           });
                    }
  
                });
        }

    }

});

var app = angular.module('SMTP', []);
app.controller('SMTPCTRL', function ($scope, $http)
{
        
    //Gets SMTP Settings
    $http.get("/MiddleWareHome/GetSMTP")
    .then(function (response) {
        $scope.ServerUrl = response.data.ServerUrl;
        $scope.Username = response.data.Username;
        $scope.Password = response.data.Password;
        $scope.PortNumber = response.data.PortNumber;
        $scope.SenderName = response.data.SenderName;
        });


    //Gets ftp Settings
    $http.get("/MiddleWareHome/Getftp")
        .then(function (response) {
            $scope.ftpUrl = response.data.ftp_URL;
            $scope.ftpUsername = response.data.ftp_UserName;
            $scope.ftpPassword = response.data.ftp_Password;
            $scope.ftpUserID = response.data.ftp_UserID;
            $scope.ftpbasePath = response.data.ftp_basePath;

            if (response.data.ftp_UserID.trim != "" && response.data.ftp_UserID != undefined) {
                $scope.GetSelectedUser(response.data.ftp_UserID);
            }
           
        });

    //Gets ftp users
    $scope.GetAllUsers = function () {

    $http.get("/MiddleWareHome/GetftpUsers")
        .then(function (response) {
            var U = [];
            for (var i = 0; i < response.data.length; i++) {
                if (response.data[i].UserName.trim() != "") {
                    U.push(response.data[i].UserName);
                }
            }

          $scope.AllFtpUsers = U;
         
        });
    }

    $scope.GetAllUsers();

    
    $scope.GetUID = function () {
        //Gets ftp users
        $http.get("/MiddleWareHome/GetftpUser?UID=" + $scope.ftpUSer)
            .then(function (response) {
                $scope.ftpUserID = response.data.Id;
               
            });
    }

     $scope.GetSelectedUser = function (u) {
        //Gets ftp users
         $http.get("/MiddleWareHome/GetftpUserID?UID=" + u)
            .then(function (response) {
                $scope.SelectedUser = response.data.UserName;
            });
    }



    //SAVE ftp Settings
        $scope.SaveftpSettings = function () {
             $http.get("/MiddleWareHome/SaveFtpSettings?ftp_Url=" + $scope.ftpUrl
                + "&ftp_Username=" + $scope.ftpUsername
                + "&ftp_Password=" + $scope.ftpPassword
                + "&ftp_User=" + $scope.ftpUserID
                + "&ftp_basePath=" + $scope.ftpbasePath
            )
            .then(function (response) {
                $scope.SelectedUser = $scope.ftpUSer;
            });
        }
  
    $scope.SaveSMTPSettings = function () {
        //Gets SMTP Settings
        $http.get("/MiddleWareHome/SaveSMTP?ServerUrl="
               + $scope.ServerUrl
               + "&Username=" + $scope.Username
               + "&Password=" + $scope.Password
               + "&PortNumber=" + $scope.PortNumber
               + "&SenderName=" + $scope.SenderName
               + "&ftp_basePath=" + $scope.ftpbasePath)
        .then(function (response) {
           
            });


        
    }
    

});



var app = angular.module('PolPrefix', []);
app.controller('SMTPCTRL', function ($scope, $http) {

    //Gets SMTP Settings
    $http.get("MiddleWareHome/GetSMTP")
    .then(function (response) {
        $scope.ServerUrl = response.data.ServerUrl;
        $scope.Username = response.data.Username;
        $scope.Password = response.data.Password;
        $scope.PortNumber = response.data.PortNumber;
        $scope.SenderName = response.data.SenderName;
    });

    $scope.SaveSMTPSettings = function () {
        //Gets SMTP Settings
        $http.get("MiddleWareHome/SaveSMTP?ServerUrl="
               + $scope.ServerUrl
               + "&Username=" + $scope.Username
               + "&Password=" + $scope.Password
               + "&PortNumber=" + $scope.PortNumber
               + "&SenderName=" + $scope.SenderName)
        .then(function (response) {

        });
    }

});


//Format Json in a beautiful way
function Pretty(json) {
    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    //json = json.replace("{", "").replace("}", "").replace("[", "").replace("]", "");
    return json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
        var cls = 'number';
        if (/^"/.test(match)) {
            if (/:$/.test(match)) {
                cls = 'key';
            } else {
                cls = 'string';
            }
        } else if (/true|false/.test(match)) {
            cls = 'boolean';
        } else if (/null/.test(match)) {
            cls = 'null';
        }
        return '<span class="' + cls + '">' + match + '</span>';
    });
}
function JSON_Prettify(Json) {
    var JsonObject = $("#" + Json).html();
    if (JsonObject.includes('<span')) {
        $("#" + Json).html(JsonObject);
    } else {
        $("#" + Json).html(Pretty(JsonObject));
    }
  
}

$('#JSONPretty').on('shown.bs.modal', function () {
    JSON_Prettify('JsonID');
})




