﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ucon.Models.DBModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.Core.Objects;
    using System.Linq;
    
    public partial class MiddleAccsEntities : DbContext
    {
        public MiddleAccsEntities()
            : base("name=MiddleAccsEntities")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<C__MigrationHistory> C__MigrationHistory { get; set; }
        public virtual DbSet<ActionWording> ActionWordings { get; set; }
        public virtual DbSet<Address> Addresses { get; set; }
        public virtual DbSet<AspNetRole> AspNetRoles { get; set; }
        public virtual DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public virtual DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public virtual DbSet<AspNetUser> AspNetUsers { get; set; }
        public virtual DbSet<AuditLog> AuditLogs { get; set; }
        public virtual DbSet<AuthorizedDriver> AuthorizedDrivers { get; set; }
        public virtual DbSet<BorkerServer> BorkerServers { get; set; }
        public virtual DbSet<Broker> Brokers { get; set; }
        public virtual DbSet<Client> Clients { get; set; }
        public virtual DbSet<Company> Companies { get; set; }
        public virtual DbSet<Company1> Companies1 { get; set; }
        public virtual DbSet<CompanyLic> CompanyLics { get; set; }
        public virtual DbSet<Driver> Drivers { get; set; }
        public virtual DbSet<Global_Names> Global_Names { get; set; }
        public virtual DbSet<Insured> Insureds { get; set; }
        public virtual DbSet<IPWhiteList> IPWhiteLists { get; set; }
        public virtual DbSet<License> LICENSES { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Manual_Rates> Manual_Rates { get; set; }
        public virtual DbSet<MapDataLog> MapDataLogs { get; set; }
        public virtual DbSet<Phone_Numbers> Phone_Numbers { get; set; }
        public virtual DbSet<Policy> Policies { get; set; }
        public virtual DbSet<PolicyPrefix> PolicyPrefixes { get; set; }
        public virtual DbSet<PropertyRisk> PropertyRisks { get; set; }
        public virtual DbSet<QuotationData> QuotationDatas { get; set; }
        public virtual DbSet<Quotation> Quotations { get; set; }
        public virtual DbSet<RatingCode> RatingCodes { get; set; }
        public virtual DbSet<Risk> Risks { get; set; }
        public virtual DbSet<SettingConfig> SettingConfigs { get; set; }
        public virtual DbSet<TestPolicy> TestPolicies { get; set; }
        public virtual DbSet<TransactionWording> TransactionWordings { get; set; }
        public virtual DbSet<TransactionWordingsAction> TransactionWordingsActions { get; set; }
        public virtual DbSet<UsageCode> UsageCodes { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<VehDoNotInsure> VehDoNotInsures { get; set; }
        public virtual DbSet<Vehicle_Location> Vehicle_Location { get; set; }
        public virtual DbSet<SMTP_SERVER> SMTP_SERVER { get; set; }
    
        public virtual int ActivateKey(Nullable<int> companyID)
        {
            var companyIDParameter = companyID.HasValue ?
                new ObjectParameter("CompanyID", companyID) :
                new ObjectParameter("CompanyID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ActivateKey", companyIDParameter);
        }
    
        public virtual int ActivateUserKey(Nullable<System.Guid> key)
        {
            var keyParameter = key.HasValue ?
                new ObjectParameter("key", key) :
                new ObjectParameter("key", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("ActivateUserKey", keyParameter);
        }
    
        public virtual ObjectResult<string> ADDAuthDrivers(string code, string value)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("ADDAuthDrivers", codeParameter, valueParameter);
        }
    
        public virtual int AddKey(string key)
        {
            var keyParameter = key != null ?
                new ObjectParameter("Key", key) :
                new ObjectParameter("Key", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddKey", keyParameter);
        }
    
        public virtual ObjectResult<string> ADDPolicyPrefix(string code, string value)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("ADDPolicyPrefix", codeParameter, valueParameter);
        }
    
        public virtual int AddQuotationData(Nullable<int> quotationNumber, string quotationData, string uWPrefixDescription, Nullable<int> quotationNumberFromQuote)
        {
            var quotationNumberParameter = quotationNumber.HasValue ?
                new ObjectParameter("QuotationNumber", quotationNumber) :
                new ObjectParameter("QuotationNumber", typeof(int));
    
            var quotationDataParameter = quotationData != null ?
                new ObjectParameter("QuotationData", quotationData) :
                new ObjectParameter("QuotationData", typeof(string));
    
            var uWPrefixDescriptionParameter = uWPrefixDescription != null ?
                new ObjectParameter("UWPrefixDescription", uWPrefixDescription) :
                new ObjectParameter("UWPrefixDescription", typeof(string));
    
            var quotationNumberFromQuoteParameter = quotationNumberFromQuote.HasValue ?
                new ObjectParameter("QuotationNumberFromQuote", quotationNumberFromQuote) :
                new ObjectParameter("QuotationNumberFromQuote", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddQuotationData", quotationNumberParameter, quotationDataParameter, uWPrefixDescriptionParameter, quotationNumberFromQuoteParameter);
        }
    
        public virtual ObjectResult<string> ADDRatingCodes(string code, string value)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("ADDRatingCodes", codeParameter, valueParameter);
        }
    
        public virtual int AddRisks(Nullable<int> riskID, Nullable<decimal> sumInsured, string riskItemType)
        {
            var riskIDParameter = riskID.HasValue ?
                new ObjectParameter("RiskID", riskID) :
                new ObjectParameter("RiskID", typeof(int));
    
            var sumInsuredParameter = sumInsured.HasValue ?
                new ObjectParameter("SumInsured", sumInsured) :
                new ObjectParameter("SumInsured", typeof(decimal));
    
            var riskItemTypeParameter = riskItemType != null ?
                new ObjectParameter("RiskItemType", riskItemType) :
                new ObjectParameter("RiskItemType", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddRisks", riskIDParameter, sumInsuredParameter, riskItemTypeParameter);
        }
    
        public virtual ObjectResult<string> ADDTransactionWordings(string code, string value)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("ADDTransactionWordings", codeParameter, valueParameter);
        }
    
        public virtual ObjectResult<string> ADDUsages(string code, string value)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            var valueParameter = value != null ?
                new ObjectParameter("Value", value) :
                new ObjectParameter("Value", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("ADDUsages", codeParameter, valueParameter);
        }
    
        public virtual int AddUser(string username, string password, string firstName, string lastName, Nullable<bool> isDefault, Nullable<bool> isAdmin, string emailAddress)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("Username", username) :
                new ObjectParameter("Username", typeof(string));
    
            var passwordParameter = password != null ?
                new ObjectParameter("Password", password) :
                new ObjectParameter("Password", typeof(string));
    
            var firstNameParameter = firstName != null ?
                new ObjectParameter("FirstName", firstName) :
                new ObjectParameter("FirstName", typeof(string));
    
            var lastNameParameter = lastName != null ?
                new ObjectParameter("LastName", lastName) :
                new ObjectParameter("LastName", typeof(string));
    
            var isDefaultParameter = isDefault.HasValue ?
                new ObjectParameter("IsDefault", isDefault) :
                new ObjectParameter("IsDefault", typeof(bool));
    
            var isAdminParameter = isAdmin.HasValue ?
                new ObjectParameter("IsAdmin", isAdmin) :
                new ObjectParameter("IsAdmin", typeof(bool));
    
            var emailAddressParameter = emailAddress != null ?
                new ObjectParameter("EmailAddress", emailAddress) :
                new ObjectParameter("EmailAddress", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddUser", usernameParameter, passwordParameter, firstNameParameter, lastNameParameter, isDefaultParameter, isAdminParameter, emailAddressParameter);
        }
    
        public virtual int AddUserQuote(string username, Nullable<int> quotationNumber)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            var quotationNumberParameter = quotationNumber.HasValue ?
                new ObjectParameter("QuotationNumber", quotationNumber) :
                new ObjectParameter("QuotationNumber", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddUserQuote", usernameParameter, quotationNumberParameter);
        }
    
        public virtual int AddWording(Nullable<int> actionID, string wordingsValue)
        {
            var actionIDParameter = actionID.HasValue ?
                new ObjectParameter("ActionID", actionID) :
                new ObjectParameter("ActionID", typeof(int));
    
            var wordingsValueParameter = wordingsValue != null ?
                new ObjectParameter("WordingsValue", wordingsValue) :
                new ObjectParameter("WordingsValue", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddWording", actionIDParameter, wordingsValueParameter);
        }
    
        public virtual ObjectResult<string> ADDWordingActions(string code, Nullable<int> wordCount)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            var wordCountParameter = wordCount.HasValue ?
                new ObjectParameter("WordCount", wordCount) :
                new ObjectParameter("WordCount", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("ADDWordingActions", codeParameter, wordCountParameter);
        }
    
        public virtual int AddWordingProperty(string wordingProperty, Nullable<int> actionID)
        {
            var wordingPropertyParameter = wordingProperty != null ?
                new ObjectParameter("WordingProperty", wordingProperty) :
                new ObjectParameter("WordingProperty", typeof(string));
    
            var actionIDParameter = actionID.HasValue ?
                new ObjectParameter("ActionID", actionID) :
                new ObjectParameter("ActionID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddWordingProperty", wordingPropertyParameter, actionIDParameter);
        }
    
        public virtual int AuditLogging(Nullable<int> userID, string logAction, string comments, string errors, string requestType, string jSONRequest, string jSONResponse)
        {
            var userIDParameter = userID.HasValue ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(int));
    
            var logActionParameter = logAction != null ?
                new ObjectParameter("LogAction", logAction) :
                new ObjectParameter("LogAction", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var errorsParameter = errors != null ?
                new ObjectParameter("Errors", errors) :
                new ObjectParameter("Errors", typeof(string));
    
            var requestTypeParameter = requestType != null ?
                new ObjectParameter("RequestType", requestType) :
                new ObjectParameter("RequestType", typeof(string));
    
            var jSONRequestParameter = jSONRequest != null ?
                new ObjectParameter("JSONRequest", jSONRequest) :
                new ObjectParameter("JSONRequest", typeof(string));
    
            var jSONResponseParameter = jSONResponse != null ?
                new ObjectParameter("JSONResponse", jSONResponse) :
                new ObjectParameter("JSONResponse", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AuditLogging", userIDParameter, logActionParameter, commentsParameter, errorsParameter, requestTypeParameter, jSONRequestParameter, jSONResponseParameter);
        }
    
        public virtual ObjectResult<CheckActiveKey_Result> CheckActiveKey(Nullable<System.Guid> key)
        {
            var keyParameter = key.HasValue ?
                new ObjectParameter("key", key) :
                new ObjectParameter("key", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<CheckActiveKey_Result>("CheckActiveKey", keyParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> CheckIfExist(string code)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("CheckIfExist", codeParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> CheckIfExistAuthDrivers(string code)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("CheckIfExistAuthDrivers", codeParameter);
        }
    
        public virtual int CheckIfExistPolicyPrefix(string code)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CheckIfExistPolicyPrefix", codeParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> CheckIfExistUsageCode(string code)
        {
            var codeParameter = code != null ?
                new ObjectParameter("Code", code) :
                new ObjectParameter("Code", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("CheckIfExistUsageCode", codeParameter);
        }
    
        public virtual ObjectResult<CheckLogin_Result> CheckLogin(string importKeys)
        {
            var importKeysParameter = importKeys != null ?
                new ObjectParameter("ImportKeys", importKeys) :
                new ObjectParameter("ImportKeys", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<CheckLogin_Result>("CheckLogin", importKeysParameter);
        }
    
        public virtual ObjectResult<CheckRegistration_Result> CheckRegistration(Nullable<int> companyID)
        {
            var companyIDParameter = companyID.HasValue ?
                new ObjectParameter("CompanyID", companyID) :
                new ObjectParameter("CompanyID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<CheckRegistration_Result>("CheckRegistration", companyIDParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> CompanyExist(string companyName)
        {
            var companyNameParameter = companyName != null ?
                new ObjectParameter("CompanyName", companyName) :
                new ObjectParameter("CompanyName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("CompanyExist", companyNameParameter);
        }
    
        public virtual int CreateCompany(string companyName, string notes)
        {
            var companyNameParameter = companyName != null ?
                new ObjectParameter("CompanyName", companyName) :
                new ObjectParameter("CompanyName", typeof(string));
    
            var notesParameter = notes != null ?
                new ObjectParameter("Notes", notes) :
                new ObjectParameter("Notes", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("CreateCompany", companyNameParameter, notesParameter);
        }
    
        public virtual ObjectResult<string> CreateLiscense(string companyName, string companyID, string endDate, Nullable<int> updateID, string key)
        {
            var companyNameParameter = companyName != null ?
                new ObjectParameter("CompanyName", companyName) :
                new ObjectParameter("CompanyName", typeof(string));
    
            var companyIDParameter = companyID != null ?
                new ObjectParameter("CompanyID", companyID) :
                new ObjectParameter("CompanyID", typeof(string));
    
            var endDateParameter = endDate != null ?
                new ObjectParameter("EndDate", endDate) :
                new ObjectParameter("EndDate", typeof(string));
    
            var updateIDParameter = updateID.HasValue ?
                new ObjectParameter("UpdateID", updateID) :
                new ObjectParameter("UpdateID", typeof(int));
    
            var keyParameter = key != null ?
                new ObjectParameter("Key", key) :
                new ObjectParameter("Key", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("CreateLiscense", companyNameParameter, companyIDParameter, endDateParameter, updateIDParameter, keyParameter);
        }
    
        public virtual int DeleteCompany(Nullable<int> companyID)
        {
            var companyIDParameter = companyID.HasValue ?
                new ObjectParameter("companyID", companyID) :
                new ObjectParameter("companyID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteCompany", companyIDParameter);
        }
    
        public virtual ObjectResult<GetAuditLogs_Result> GetAuditLogs(Nullable<System.DateTime> startdate, Nullable<System.DateTime> enddate, string ip)
        {
            var startdateParameter = startdate.HasValue ?
                new ObjectParameter("startdate", startdate) :
                new ObjectParameter("startdate", typeof(System.DateTime));
    
            var enddateParameter = enddate.HasValue ?
                new ObjectParameter("enddate", enddate) :
                new ObjectParameter("enddate", typeof(System.DateTime));
    
            var ipParameter = ip != null ?
                new ObjectParameter("ip", ip) :
                new ObjectParameter("ip", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAuditLogs_Result>("GetAuditLogs", startdateParameter, enddateParameter, ipParameter);
        }
    
        public virtual ObjectResult<string> GetColumnNames(string tableName)
        {
            var tableNameParameter = tableName != null ?
                new ObjectParameter("TableName", tableName) :
                new ObjectParameter("TableName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetColumnNames", tableNameParameter);
        }
    
        public virtual ObjectResult<string> GetLic()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetLic");
        }
    
        public virtual ObjectResult<Nullable<System.Guid>> GetNewKey()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<System.Guid>>("GetNewKey");
        }
    
        public virtual ObjectResult<Nullable<System.Guid>> LoginProtocol(string userID)
        {
            var userIDParameter = userID != null ?
                new ObjectParameter("UserID", userID) :
                new ObjectParameter("UserID", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<System.Guid>>("LoginProtocol", userIDParameter);
        }
    
        public virtual int LogOutUser(Nullable<System.Guid> key)
        {
            var keyParameter = key.HasValue ?
                new ObjectParameter("key", key) :
                new ObjectParameter("key", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("LogOutUser", keyParameter);
        }
    
        public virtual int MapLogs(string logType, string logMessage)
        {
            var logTypeParameter = logType != null ?
                new ObjectParameter("LogType", logType) :
                new ObjectParameter("LogType", typeof(string));
    
            var logMessageParameter = logMessage != null ?
                new ObjectParameter("LogMessage", logMessage) :
                new ObjectParameter("LogMessage", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("MapLogs", logTypeParameter, logMessageParameter);
        }
    
        public virtual int MiddleLog(string logAction, string comments, string errors, string requestType, string jSONRequest, string jSONResponse, string importKey, string iP, string computerName, string computerUserName, string userName, string timespan)
        {
            var logActionParameter = logAction != null ?
                new ObjectParameter("LogAction", logAction) :
                new ObjectParameter("LogAction", typeof(string));
    
            var commentsParameter = comments != null ?
                new ObjectParameter("Comments", comments) :
                new ObjectParameter("Comments", typeof(string));
    
            var errorsParameter = errors != null ?
                new ObjectParameter("Errors", errors) :
                new ObjectParameter("Errors", typeof(string));
    
            var requestTypeParameter = requestType != null ?
                new ObjectParameter("RequestType", requestType) :
                new ObjectParameter("RequestType", typeof(string));
    
            var jSONRequestParameter = jSONRequest != null ?
                new ObjectParameter("JSONRequest", jSONRequest) :
                new ObjectParameter("JSONRequest", typeof(string));
    
            var jSONResponseParameter = jSONResponse != null ?
                new ObjectParameter("JSONResponse", jSONResponse) :
                new ObjectParameter("JSONResponse", typeof(string));
    
            var importKeyParameter = importKey != null ?
                new ObjectParameter("ImportKey", importKey) :
                new ObjectParameter("ImportKey", typeof(string));
    
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            var computerNameParameter = computerName != null ?
                new ObjectParameter("ComputerName", computerName) :
                new ObjectParameter("ComputerName", typeof(string));
    
            var computerUserNameParameter = computerUserName != null ?
                new ObjectParameter("ComputerUserName", computerUserName) :
                new ObjectParameter("ComputerUserName", typeof(string));
    
            var userNameParameter = userName != null ?
                new ObjectParameter("UserName", userName) :
                new ObjectParameter("UserName", typeof(string));
    
            var timespanParameter = timespan != null ?
                new ObjectParameter("Timespan", timespan) :
                new ObjectParameter("Timespan", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("MiddleLog", logActionParameter, commentsParameter, errorsParameter, requestTypeParameter, jSONRequestParameter, jSONResponseParameter, importKeyParameter, iPParameter, computerNameParameter, computerUserNameParameter, userNameParameter, timespanParameter);
        }
    
        public virtual int NewCompany(string companyName, byte[] companyInfo)
        {
            var companyNameParameter = companyName != null ?
                new ObjectParameter("CompanyName", companyName) :
                new ObjectParameter("CompanyName", typeof(string));
    
            var companyInfoParameter = companyInfo != null ?
                new ObjectParameter("CompanyInfo", companyInfo) :
                new ObjectParameter("CompanyInfo", typeof(byte[]));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("NewCompany", companyNameParameter, companyInfoParameter);
        }
    
        public virtual int NewQuotation()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("NewQuotation");
        }
    
        public virtual ObjectResult<string> PrepAuthDrivers()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("PrepAuthDrivers");
        }
    
        public virtual ObjectResult<string> PrepPolicyPrefix()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("PrepPolicyPrefix");
        }
    
        public virtual ObjectResult<PrepUsages_Result> PrepUsages()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<PrepUsages_Result>("PrepUsages");
        }
    
        public virtual ObjectResult<RegisterCompany_Result> RegisterCompany(string companyName, string address, string phone, string email, string contactPerson)
        {
            var companyNameParameter = companyName != null ?
                new ObjectParameter("CompanyName", companyName) :
                new ObjectParameter("CompanyName", typeof(string));
    
            var addressParameter = address != null ?
                new ObjectParameter("Address", address) :
                new ObjectParameter("Address", typeof(string));
    
            var phoneParameter = phone != null ?
                new ObjectParameter("Phone", phone) :
                new ObjectParameter("Phone", typeof(string));
    
            var emailParameter = email != null ?
                new ObjectParameter("Email", email) :
                new ObjectParameter("Email", typeof(string));
    
            var contactPersonParameter = contactPerson != null ?
                new ObjectParameter("ContactPerson", contactPerson) :
                new ObjectParameter("ContactPerson", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<RegisterCompany_Result>("RegisterCompany", companyNameParameter, addressParameter, phoneParameter, emailParameter, contactPersonParameter);
        }
    
        public virtual int RemoveWordingActions(Nullable<int> wordID)
        {
            var wordIDParameter = wordID.HasValue ?
                new ObjectParameter("WordID", wordID) :
                new ObjectParameter("WordID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("RemoveWordingActions", wordIDParameter);
        }
    
        public virtual int SetLoggedIn(string username)
        {
            var usernameParameter = username != null ?
                new ObjectParameter("username", username) :
                new ObjectParameter("username", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("SetLoggedIn", usernameParameter);
        }
    
        public virtual int UpdateDriver(Nullable<int> quotationID, string driverLic, string driverFirstName, string driverLastName)
        {
            var quotationIDParameter = quotationID.HasValue ?
                new ObjectParameter("QuotationID", quotationID) :
                new ObjectParameter("QuotationID", typeof(int));
    
            var driverLicParameter = driverLic != null ?
                new ObjectParameter("DriverLic", driverLic) :
                new ObjectParameter("DriverLic", typeof(string));
    
            var driverFirstNameParameter = driverFirstName != null ?
                new ObjectParameter("DriverFirstName", driverFirstName) :
                new ObjectParameter("DriverFirstName", typeof(string));
    
            var driverLastNameParameter = driverLastName != null ?
                new ObjectParameter("DriverLastName", driverLastName) :
                new ObjectParameter("DriverLastName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateDriver", quotationIDParameter, driverLicParameter, driverFirstNameParameter, driverLastNameParameter);
        }
    
        public virtual int UpdateDriverRecord(Nullable<int> quotationID, string driverDOB, string driverLicFirstIssued, string driverFirstName, string driverLastName)
        {
            var quotationIDParameter = quotationID.HasValue ?
                new ObjectParameter("QuotationID", quotationID) :
                new ObjectParameter("QuotationID", typeof(int));
    
            var driverDOBParameter = driverDOB != null ?
                new ObjectParameter("DriverDOB", driverDOB) :
                new ObjectParameter("DriverDOB", typeof(string));
    
            var driverLicFirstIssuedParameter = driverLicFirstIssued != null ?
                new ObjectParameter("DriverLicFirstIssued", driverLicFirstIssued) :
                new ObjectParameter("DriverLicFirstIssued", typeof(string));
    
            var driverFirstNameParameter = driverFirstName != null ?
                new ObjectParameter("DriverFirstName", driverFirstName) :
                new ObjectParameter("DriverFirstName", typeof(string));
    
            var driverLastNameParameter = driverLastName != null ?
                new ObjectParameter("DriverLastName", driverLastName) :
                new ObjectParameter("DriverLastName", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateDriverRecord", quotationIDParameter, driverDOBParameter, driverLicFirstIssuedParameter, driverFirstNameParameter, driverLastNameParameter);
        }
    
        public virtual int UpdateQuote(Nullable<int> quotationID, Nullable<bool> accident_in_last_few_years)
        {
            var quotationIDParameter = quotationID.HasValue ?
                new ObjectParameter("QuotationID", quotationID) :
                new ObjectParameter("QuotationID", typeof(int));
    
            var accident_in_last_few_yearsParameter = accident_in_last_few_years.HasValue ?
                new ObjectParameter("Accident_in_last_few_years", accident_in_last_few_years) :
                new ObjectParameter("Accident_in_last_few_years", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateQuote", quotationIDParameter, accident_in_last_few_yearsParameter);
        }
    
        public virtual int UpdateRisk(Nullable<int> quotationID, string driverWording, string nCD)
        {
            var quotationIDParameter = quotationID.HasValue ?
                new ObjectParameter("QuotationID", quotationID) :
                new ObjectParameter("QuotationID", typeof(int));
    
            var driverWordingParameter = driverWording != null ?
                new ObjectParameter("DriverWording", driverWording) :
                new ObjectParameter("DriverWording", typeof(string));
    
            var nCDParameter = nCD != null ?
                new ObjectParameter("NCD", nCD) :
                new ObjectParameter("NCD", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("UpdateRisk", quotationIDParameter, driverWordingParameter, nCDParameter);
        }
    
        public virtual ObjectResult<ValidateKey_Result> ValidateKey(string activationDate)
        {
            var activationDateParameter = activationDate != null ?
                new ObjectParameter("ActivationDate", activationDate) :
                new ObjectParameter("ActivationDate", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<ValidateKey_Result>("ValidateKey", activationDateParameter);
        }
    
        public virtual int AddCompanyCode(string companyCode, string brokerNameS)
        {
            var companyCodeParameter = companyCode != null ?
                new ObjectParameter("CompanyCode", companyCode) :
                new ObjectParameter("CompanyCode", typeof(string));
    
            var brokerNameSParameter = brokerNameS != null ?
                new ObjectParameter("BrokerNameS", brokerNameS) :
                new ObjectParameter("BrokerNameS", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddCompanyCode", companyCodeParameter, brokerNameSParameter);
        }
    
        public virtual ObjectResult<GetCompanyUrl_Result> GetCompanyUrl(string compCode)
        {
            var compCodeParameter = compCode != null ?
                new ObjectParameter("compCode", compCode) :
                new ObjectParameter("compCode", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetCompanyUrl_Result>("GetCompanyUrl", compCodeParameter);
        }
    
        public virtual int AddIP(string iP, string iPDescription, Nullable<bool> isBlocked)
        {
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            var iPDescriptionParameter = iPDescription != null ?
                new ObjectParameter("IPDescription", iPDescription) :
                new ObjectParameter("IPDescription", typeof(string));
    
            var isBlockedParameter = isBlocked.HasValue ?
                new ObjectParameter("IsBlocked", isBlocked) :
                new ObjectParameter("IsBlocked", typeof(bool));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("AddIP", iPParameter, iPDescriptionParameter, isBlockedParameter);
        }
    
        public virtual ObjectResult<Nullable<bool>> IsIPExist(string iP)
        {
            var iPParameter = iP != null ?
                new ObjectParameter("IP", iP) :
                new ObjectParameter("IP", typeof(string));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<Nullable<bool>>("IsIPExist", iPParameter);
        }
    
        public virtual int DeleteIP(Nullable<int> iPID)
        {
            var iPIDParameter = iPID.HasValue ?
                new ObjectParameter("IPID", iPID) :
                new ObjectParameter("IPID", typeof(int));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction("DeleteIP", iPIDParameter);
        }
    
        public virtual ObjectResult<string> GetSource(Nullable<System.Guid> key)
        {
            var keyParameter = key.HasValue ?
                new ObjectParameter("Key", key) :
                new ObjectParameter("Key", typeof(System.Guid));
    
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<string>("GetSource", keyParameter);
        }
    
        public virtual ObjectResult<GetAMPLUSUrl_Result> GetAMPLUSUrl()
        {
            return ((IObjectContextAdapter)this).ObjectContext.ExecuteFunction<GetAMPLUSUrl_Result>("GetAMPLUSUrl");
        }
    }
}
