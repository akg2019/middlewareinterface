﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class WallTypes
    {
        public JObject Walls = new JObject();
        public List<Company> Lists = new List<Company>();
    }
}