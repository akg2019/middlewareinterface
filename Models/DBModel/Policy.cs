//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ucon.Models.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Policy
    {
        public int PolicyID { get; set; }
        public Nullable<int> CompanyID { get; set; }
        public string policy_number { get; set; }
        public string account_code { get; set; }
        public string agreed_value { get; set; }
        public string billing_account_code { get; set; }
        public string branch { get; set; }
        public string country { get; set; }
        public string currency { get; set; }
        public string end_date { get; set; }
        public string polciy_prefix { get; set; }
        public string source_amount_code { get; set; }
        public string start_date { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public Nullable<System.Guid> UserID { get; set; }
        public string UserName { get; set; }
    
        public virtual Company Company { get; set; }
    }
}
