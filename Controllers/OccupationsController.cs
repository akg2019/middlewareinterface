﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ucon.Controllers
{
    public class OccupationsController : Controller
    {

        // GET: Occupations
        public ActionResult Index()
        {
            var occupations = new Models.GetOccupations();

            JObject O = JObject.Parse(Models.KeyManagement.PerformRequest("epic_mwGetOccupations", 
                Models.KeyManagement.RequestType.SignleParameter, "All", 
                null));


            foreach( JToken T in O["data"])
            {
                occupations.Lists.Add(new Models.Company(T["industry"].ToString(), T["occupation"].ToString(), (bool)T["do_not_insure"]));
                
            }

            return View(occupations);
        }



        // GET: Occupations/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: Occupations/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Occupations/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Occupations/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: Occupations/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Occupations/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: Occupations/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
