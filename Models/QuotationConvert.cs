﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class QuotationConvert
    {
        public string QuotationNumber { set; get; }
        public JObject Quote = new JObject();
        public List<string> Lists = new List<string>();
    }
}