﻿    //Handles Button Click for underwriter

    function SubmitRequest() {
        var RequestMethodBody;

        RequestQuote("",JSON.stringify(RequestMethodBody));
    };
   

    function RequestQuote(Method,RequestBody) {
        $.ajax({
            type: "POST",
            url: "http://127.0.0.1:8081/mw/wsCall.php/",
            data: RequestBody,
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessMethod,
            error: ErrorMethod
        });

    }

    function SuccessMethod(response) {

        alert("This is the response " + response);
    }

    function ErrorMethod(response) {
        alert(response.status + " this is the error! " + response.statusText);
    }

//Handles process completed for the wizard
    function ClickeButton(Button) {
        var ProgressBar = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"2\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em; width: 2%;\">2%</div>";
       
        if (Button == 1) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 40em; width: 2%;\">40% Complete</div>";
        } else if (Button == 2) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"55\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 55em; width: 2%;\">55% Complete</div>";
        } else if (Button == 3) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 70em; width: 2%;\">70% Complete</div>";
        } else if (Button == 4) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 85em; width: 2%;\">85% Complete</div>";
        } else if (Button == 5) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em; width: 100%;\">100% Complete. Finised!</div>";
        } else if (Button == 0) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"25\" style=\"min-width: 2em; width: 25%;\">25% Complete</div>";
        }
    }
//Request Motor Vehicle Quotation
    var MotorQuote = {
        "quotation": {
            "quotation_number": 0,
            "policy_prefix": "",
            "first_name": "",
            "middle_name": "",
            "last_name": "",
            "national_id": "",
            "national_id_type": "",
            "phone_numbers": [
                {
                    "type": "",
                    "carrier": "",
                    "description": "",
                    "extension_or_range": "",
                    "phone_number": ""
                }
            ],
            "email_address": "",
            "company_name": "",
            "address": {
                "altitude": "",
                "block": "",
                "building_number": "",
                "complex_name": "",
                "country": "",
                "general_area": "",
                "international_area": "",
                "is_international_address": "",
                "island": "",
                "latitude": "",
                "longitude": "",
                "parish": "",
                "postal_zip_code": "",
                "street_name": "",
                "street_number": "",
                "street_type": "",
                "town": "",
                "unit_number": ""
            },
            "start_date": "00/00/0000 00:00:00",
            "accidents_in_last_few_years": false,
            "occupation": "",
            "currency": "",
            "is_motor_cat_perils_covered": false
        },
        "risk": [
            {
                "sum_insured": 0.0,
                "usage_code": "",
                "authorized_driver_wording": "",
                "year_for_rating": 0,
                "year": 0,
                "make": "",
                "model": "",
                "model_type": "",
                "extension": "",
                "body_type": "",
                "body_shape": "",
                "hp_cc_rating": 0,
                "hp_cc_unit_type": "",
                "cert_type": "",
                "chassis_number": "",
                "colour": "",
                "number_of_cyclinders": 0,
                "engine_modified": false,
                "engine_number": "",
                "engine_type": "",
                "has_electric_doors": false,
                "has_electric_side_mirror": false,
                "has_electric_window": false,
                "has_power_steering": false,
                "import_type": "",
                "ncd_percent": 0.0,
                "left_hand_drive": false,
                "mileage": 0,
                "mileage_type": "",
                "number_of_doors": 0,
                "number_of_engines": 0,
                "registration_number": "",
                "roof_type": "",
                "seat_type": "",
                "seating": 0,
                "tonnage": 0,
                "transmission_type": "",
                "vehicle_location": {
                    "altitude": "",
                    "block": "",
                    "building_number": "",
                    "country": "",
                    "general_area": "",
                    "international_area": "",
                    "is_international_address": "",
                    "island": "",
                    "latitude": "",
                    "longitude": "",
                    "parish": "",
                    "postal_zip_code": "",
                    "street_name": "",
                    "street_number": "",
                    "street_type": "",
                    "town": "",
                    "unit_number": ""
                },
                "drivers": [
                    {
                        "first_name": "",
                        "middle_name": "",
                        "last_name": "",
                        "driver_dob": "00/00/0000",
                        "drivers_licence_first_issued": "00/00/0000",
                        "drivers_licence_number": "",
                        "is-main-driver":false
                    }
                ]
            }
        ]
    };



//Validation

    function Rtest() {
       
        if (document.getElementById("first_name").value == null || document.getElementById("first_name").value == "") {
            alert("First Name is required");
            return false;
        } else if (document.getElementById("last_name").value == null || document.getElementById("last_name").value == "") {
            alert("Last Name is required");
            return false;
        } else if (document.getElementById("currency").value == null || document.getElementById("currency").value == "") {
            alert("Currency is required");
            return false;
        }

    }
