﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class GetPolicyRisks
    {
        public JObject PolicyData = new JObject();
        public string PolicyID { set; get; }
    }
}