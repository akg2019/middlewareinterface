﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class GlobalNamePolices
    {
        public JObject GlobalNamesPolicies = new JObject(); 
        public string GlobalNameID { set; get; }
    }
}