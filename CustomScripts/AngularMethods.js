﻿//Handles Get Sources
var app = angular.module('GetSources', []);
app.controller('myCtrl2', function ($scope, $http) {
    //Gets users
    $scope.LoadSources = function () {
        $http.get("/MiddleWareHome/getsources")
          .then(function (response) {
              $scope.Sources = response.data.sources;
          });
    }

    $scope.LoadSources();

});
