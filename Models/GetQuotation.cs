﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ucon.Models
{
    public class GetQuotation
    {
        //Quotation Section
        public bool success { set; get; }
        public class quotation
         {
            public class resquestkey
            {
                public string key { set; get; }
            }
            public int quotation_number { set; get; }
            public String policy_prefix { set; get; }
            public String first_name { set; get; }
            public String middle_name { set; get; }
            public String last_name { set; get; }
            public String national_id { set; get; }
            public String national_id_type { set; get; }
        }

        //Phone numbers
        public List<phone_numbers> Phone_Numbers = new List<phone_numbers>();
        public class phone_numbers
        {
            public String type { set; get; }
            public String carrier { set; get; }
            public String description { set; get; }
            public String extension_or_range { set; get; }
            public String phone_number { set; get; }

            phone_numbers(string Type, string Carrier,String Description, string Extension_or_range, string Phone_number)
            {
                type = Type;
                carrier = Carrier;
                description = Description;
                extension_or_range = Extension_or_range;
                phone_number = Phone_number;
            }
        }

        public String email_address { set; get; }
        public String company_name { set; get; }

        //Address section
            public class address
        {
            public String altitude { set; get; }
            public String block { set; get; }
            public String building_number { set; get; }
            public String categorization { set; get; }
            public String complex_name { set; get; }
            public String country { set; get; }
            public String general_area { set; get; }
            public String international_area { set; get; }
            public String is_international_address { set; get; }
            public String island { set; get; }
            public String latitude { set; get; }
            public String locaction_class { set; get; }
            public String longitude { set; get; }
            public String parish { set; get; }
            public String postal_zip_code { set; get; }
            public String street_name { set; get; }
            public String street_number { set; get; }
            public String street_type { set; get; }
            public String town { set; get; }
            public String unit_number { set; get; }
        }

        public String start_date { set; get; }
        public bool accidents_in_last_few_years { set; get; }
        public String occupation { set; get; }
        public String currency { set; get; }
        public bool is_motor_cat_perils_covered { set; get; }

        //Risk
        public class risk
        {
            public int sum_insured { set; get; }
            public String usage_code { set; get; }
            public String authorized_driver_wording { set; get; }
            public int year_for_rating { set; get; }
            public int year { set; get; }
            public String make { set; get; }
            public String model { set; get; }
            public String model_type { set; get; }
            public String extension { set; get; }
            public String body_type { set; get; }
            public String body_shape { set; get; }
            public int hp_cc_rating { set; get; }
            public String hp_cc_unit_type { set; get; }
            public String chassis_number { set; get; }
            public String colour { set; get; }
            public int number_of_cyclinders { set; get; }
            public bool engine_modified { set; get; }
            public String engine_number { set; get; }
            public String engine_type { set; get; }
            public bool has_electric_doors { set; get; }
            public bool has_electric_side_mirror { set; get; }
            public bool has_electric_window { set; get; }
            public bool has_power_steering { set; get; }
            public String import_type { set; get; }
            public Double ncd_percent { set; get; }
            public bool left_hand_drive { set; get; }
            public String main_driver_dobpublic { set; get; }
            public String main_driver_licence_first_issuedpublic { set; get; }
            public int mileage { set; get; }
            public String mileage_type { set; get; }
            public int number_of_doors { set; get; }
            public int number_of_engines { set; get; }
            public String registration_number { set; get; }
            public String roof_type { set; get; }
            public String seat_type { set; get; }
            public int seating { set; get; }
            public int tonnage { set; get; }
            public String transmission_type { set; get; }

            //Vehicle Location
            public class vehicle_location
            {
                public String altitude { set; get; }
                public String block { set; get; }
                public String building_number { set; get; }
                public String categorization { set; get; }
                public String complex_name { set; get; }
                public String country { set; get; }
                public String general_area { set; get; }
                public String international_area { set; get; }
                public String is_international_address { set; get; }
                public String island { set; get; }
                public String latitude { set; get; }
                public String locaction_class { set; get; }
                public String longitude { set; get; }
                public String parish { set; get; }
                public String postal_zip_code { set; get; }
                public String street_name { set; get; }
                public String street_number { set; get; }
                public String street_type { set; get; }
                public String town { set; get; }
                public String unit_number { set; get; }
            }

            //Drivers 
            List<drivers> Drivers = new List<drivers>();
            public class drivers
		    {
                public String first_name { set; get; }
                public String middle_name { set; get; }
                public String last_name { set; get; }
                public String driver_dob { set; get; }
                public String drivers_licence_first_issued { set; get; }
                public String drivers_licence_number { set; get; }
                public bool is_main_driver { set; get; }
            }
			
        }

        //Manual Rates
        public List<manual_rates> ManualRates = new List<manual_rates>();
        public class manual_rates
            {
                public string Code { set; get; }
                public float Value { set; get; }
            }

        //Premium Calculation
	    public class premium_calculation
            {
		    public float net_premium { set; get; }
            public float stamp_duty { set; get; }
            public float tax { set; get; }
            public float total_premium { set; get; }
        }

        //Limits
        public List<limits> Limits = new List<limits>();
	    public class limits 
		{
			public String code {set; get;}
			public String heading {set; get;}
			public float limit { set; get; }
            public String description { set; get; }
        }

        //Extensions
        public List<extensions> Extension = new List<extensions>();
	    public class extensions
		{
			public String type {set; get;}
			public String code {set; get;}
			public String heading {set; get;}
			public String description {set; get;}
		}
	
}

    
}