﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{

    public class VehicleColors
    {
        public JObject VehicleColor = new JObject();
        public List<string> Colors = new List<string>();
           
    }
}