﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class QuotationGet
    {
        public string QuotationNumber { set; get; }
        public static JObject QuoteResponse { set; get; }
    }
}