﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{

    public class ClientCheck
    {
        public JObject ClientResponse = new JObject();
        public object resquestkey { set; get; }
        public string company_name { set; get; }
        public string first_name { set; get; }
        public string last_name { set; get; }
        public string email { set; get; }
        public string national_id { set; get; }
        public string national_id_type { set; get; }
        public string policy_number{ set; get; }
        public bool success { set; get; }
        public int global_name_number { set; get; }
    }

 }