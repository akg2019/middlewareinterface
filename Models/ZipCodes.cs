﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class ZipCodes
    {
        public JObject Zips = new JObject();
        public string Country { set; get; }
        public List<string> Lists = new List<string>();
    }
}