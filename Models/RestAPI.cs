﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class RestAPI
    {
        public string URI { set; get; }
        public string RequestBody { set; get; }
        public JObject ResponseBody = new JObject();
        public string MethodName { set; get; }
    }
}
    