﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class GetAuthDrivers
    {
        public string PolicyPrefix { set; get; }
        public JObject AuthorizedDriver = new JObject();
        public List<string> Lists = new List<string>();
    }
}