﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
   public class GetPolicy
    {
        public string PolicyID { get; set; }
        public JObject PolicyData { get; set; }
    }
}
