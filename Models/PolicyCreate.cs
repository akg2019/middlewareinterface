﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class PolicyCreate
    {
        public resquestkey resquestkey = new resquestkey();
        public policy policy = new policy();
        public List<GlobalNameSubmit> global_names = new List<GlobalNameSubmit>();
        public List<risks> risks = new List<Models.risks>();
      
    }

    public class PolicyResponses
    {
        public string PolicyID { set; get; }
    }

    public class PolicyResponseData
    {
        public JObject PolResposne = new JObject();
    }
    
        public class policy
        {
            public string agreed_value { set; get; }
            public string auto_renewal { set; get; }
            public string currency { set; get; }
            public string date_proposal_signed { set; get; }
            public string end_date { set; get; }
            public string memo { set; get; }
            public string occupancy { set; get; }
            public string policy_prefix { set; get; }
            public string source_account_code { set; get; }
            public string start_date { set; get; }
            public string usage_category { set; get; }
            public List<insureds> insureds = new List<Models.insureds>();
        }


        public class insureds
        {
            public string is_main_insured { set; get; }
            public string national_id { set; get; }
            public string national_id_type { set; get; }
        }
           

        public class global_names
        {
        public string global_name_id { set; get; }
        public string global_name_number { set; get; }
        public string company_name { set; get; }
        public string dob { set; get; }
        public string drivers_licence_country { set; get; }
        public string drivers_licence_date_issued { set; get; }
        public string drivers_licence_first_issued { set; get; }
        public string drivers_licence_number { set; get; }
        public string email_address { set; get; }
        public string employment_type { set; get; }
        public string first_name { set; get; }
        public string gender { set; get; }
        public string is_a_company { set; get; }
        public string is_a_service_provider { set; get; }
        public string last_name { set; get; }
        public string maiden_name { set; get; }
        public string mailing_name { set; get; }
        public string marital_status { set; get; }
        public string middle_name { set; get; }
        public string national_id { set; get; }
        public string national_id_type { set; get; }
        public string nationality { set; get; }
        public string notes { set; get; }
        public string occupation { set; get; }
        public string place_of_birth { set; get; }
        public string salutation_name { set; get; }
        public string service_type { set; get; }
        public string tax_id_number { set; get; }
        public string title { set; get; }
        public List<phone_numbers> phone_numbers = new List<phone_numbers>();
        public List<locations> locations = new List<locations>();
    }


        public class risks
        {
            public string risk_id { set; get; }
            public string sum_insured { set; get; }
            public string usage_code { set; get; }
            public string authorized_driver_wording { set; get; }
            public string year_for_rating { set; get; }
            public string year { set; get; }
            public string make { set; get; }
            public string model { set; get; }
            public string model_type { set; get; }
            public string extension { set; get; }
            public string body_type { set; get; }
            public string body_shape { set; get; }
            public string hp_cc_rating { set; get; }
            public string hp_cc_unit_type { set; get; }
            public string chassis_number { set; get; }
            public string colour { set; get; }
            public string number_of_cyclinders { set; get; }
            public string engine_modified { set; get; }
            public string engine_number { set; get; }
            public string engine_type { set; get; }
            public string has_electric_doors { set; get; }
            public string has_electric_side_mirror { set; get; }
            public string has_electric_window { set; get; }
            public string has_power_steering { set; get; }
            public string import_type { set; get; }
            public string left_hand_drive { set; get; }
            public string main_driver_dob { set; get; }
            public string main_drivers_licence_first_issued { set; get; }
            public string mileage { set; get; }
            public string mileage_type { set; get; }
            public string number_of_doors { set; get; }
            public string number_of_engines { set; get; }
            public string registration_number { set; get; }
            public string roof_type { set; get; }
            public string seat_type { set; get; }
            public string seating { set; get; }
            public string tonnage { set; get; }
            public string transmission_type { set; get; }
            public string type_of_license { set; get; }
            public string vehicle_registered_location { set; get; }
            public List<OtherModels.drivers> drivers = new List<OtherModels.drivers>();
            public List<manual_rates> manual_rates = new List<Models.manual_rates>();
        }
               
    
   
}