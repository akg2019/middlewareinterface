﻿using Microsoft.AspNet.Identity;
using Newtonsoft.Json.Linq;
using System.Web.Mvc;
using Ucon.Models;
using System.Collections;
using OmaxFramework;
using System.Linq;
using System.Web;
using System.IO;
using System.Data;
using System;
using Newtonsoft.Json;
using System.Collections.Generic;

namespace Ucon.Controllers
{
    public class MiddleWareHomeController : Controller
    {
        // GET: MiddleWareHome
        string ConnectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public ActionResult Index()
        {

            if (User.Identity.IsAuthenticated == false)
            {
                return RedirectToAction("Login", "Account");
            }
            else
            {
                try { TempData["Versions"] = GlobalData.GetVersions(); } catch (Exception) { }
                try { TempData["UW"] = Properties.Settings.Default.MidURI; } catch (Exception) { }
                try { TempData["Server"] = Environment.MachineName; } catch (Exception) { }
            }
            
            return View();
        }

        #region MakeAnModels
         public ActionResult MakeAndModel()
        {
            MakeModels M = new MakeModels();
           return View(M);
        }


        [HttpPost]
        public ActionResult MakeAndModel(MakeModels Policy)
        {
            MakeModels M = new MakeModels();
            try
            {
                 M.ConcatList = Policy.ConcatList;
                 M.MakesAndModels = GlobalData.GetModels(Policy.PolicyType,Policy.ConcatList);
            }
            catch (System.Exception e)
            {
                JObject Err = new JObject();
                Err.Add("Errors", "There some errors when submitting your request." + e.Message);
                M.MakesAndModels = Err;
            }
           
            return View(M);
        }
        #endregion

        #region Countries
        public ActionResult Countries()
        {
            GetLocalCountries C = new GetLocalCountries();
            C.Countries = GlobalData.GetLOCCountries();
            return View(C);
        }

        #endregion

        #region GeneralReas
        public ActionResult GeneralAreas()
        {
            GetGeneralAreas G = new GetGeneralAreas();
            return View(G);
        }

        [HttpPost]
        public ActionResult GeneralAreas(GetGeneralAreas Country)
        {
            GetGeneralAreas G = new GetGeneralAreas();
            G.GAreas = GlobalData.GetLOCGeneralAreas(Country.Countries);
            return View(G);
        }
        #endregion

        #region Islands
        public ActionResult Islands()
        {
            GetLocalIslands I = new GetLocalIslands();
            return View(I);
        }

        [HttpPost]
        public ActionResult Islands(GetLocalIslands Island)
        {
            GetLocalIslands I = new GetLocalIslands();
            I.CuntryList = GlobalData.GetLOCIslands(Island.Countries);
            return View(I);
        }
        #endregion

        #region Parish
    public ActionResult Parish()
        {
            ParaishAndTowns P = new ParaishAndTowns();
            return View(P);
        }

        [HttpPost]
        public ActionResult Parish(ParaishAndTowns Parishes)
        {
            ParaishAndTowns P = new ParaishAndTowns();
            P.Parish = GlobalData.Parishes(Parishes.Country);
            return View(P);
        }

        #endregion

        #region StreetNames
        public ActionResult StreetNames()
        {
            GetLocalStreetNames S = new GetLocalStreetNames();
            return View(S);
        }

        [HttpPost]
        public ActionResult StreetNames(GetLocalStreetNames Street)
        {
            GetLocalStreetNames S = new GetLocalStreetNames();
            S.Streets = GlobalData.GetLOCStreetNames(S.SearchCriteria);
            return View(S);
        }
        #endregion

        #region StreetTypes
        public ActionResult StreetType()
        {
            StreetTypes S = new StreetTypes();
            S.Streettype = GlobalData.GetLOCStreetTypes();
            return View(S);
        }
        #endregion

        #region ZipCodes
        public ActionResult ZipCodes()
        {
            ZipCodes Z = new ZipCodes();
            return View(Z);
        }

        [HttpPost]
        public ActionResult ZipCodes(ZipCodes C)
        {
            ZipCodes ZC = new ZipCodes();
            ZC.Country = C.Country;
            ZC.Zips = GlobalData.GetLOCZipCodes(C.Country);
            return View(ZC);
        }
        #endregion

        #region Occupations
        public ActionResult Occupations()
        {
            GetOccupations O = new GetOccupations();
            return View (O);
        }

        [HttpPost]
        public ActionResult Occupations(GetOccupations Occupation)
        {
            GetOccupations O = new GetOccupations();
            O.OccupationList = GlobalData.GetOccupations(Occupation.Groups);
            return View (O);
        }

        #endregion

        #region PropertyRoofTypes
        public ActionResult PropertyRoofTypes()
        {
            PropertyRoofType R = new PropertyRoofType();
            R.RoofType = GlobalData.PropRoofType();
            return View(R);
        }
        #endregion

        #region PropertyWallTypes
        public ActionResult PropertyWallTYpes()
        {
            WallTypes W = new WallTypes();
            W.Walls = GlobalData.GetPropWallType();
            return View(W);
        }

        #endregion

        #region Usages
        public ActionResult Usages()
        {
            GetUsages U = new GetUsages();
            return View(U);
        }

        [HttpPost]
        public ActionResult Usages(GetUsages Usage)
        {
            GetUsages U = new GetUsages();
            U.Usage = GlobalData.GetUsages(Usage.PolicyPrefix);
            return View(U);
        }

        #endregion

        #region AuthorizedDrivers
        public ActionResult AuthorizedDrivers()
        {
            GetAuthDrivers A = new GetAuthDrivers();
            return View(A);
        }

        [HttpPost]
        public ActionResult AuthorizedDrivers(GetAuthDrivers Driver)
        {
            GetAuthDrivers A = new GetAuthDrivers();
            A.AuthorizedDriver = GlobalData.GetNamedDrivers(Driver.PolicyPrefix);
            return View(A);
        }
        #endregion

        #region VehicleColors
        public ActionResult MotorVehicleColors()
        {
            VehicleColors VC = new VehicleColors();
            VC.VehicleColor = GlobalData.GetVehicleColor();
            return View(VC);
        }
        #endregion

        #region MotorVehicleRoofTypes
        public ActionResult VehicleRoofTypes()
        {
            MotorVehicleRoofTypes R = new MotorVehicleRoofTypes();
            R.RoofType = GlobalData.VehRoofType();
            return View(R);
        }
        #endregion

        #region TransmissionTypes
        public ActionResult VehicleTransmissionTypes()
        {
            TransmissionType T = new TransmissionType();
            T.Types = GlobalData.VehTransmissionType();
            return View(T);
        }
        #endregion

        #region ImportTypes
        public ActionResult ImportTypes()
        {
            VehicleImportTypes Types = new VehicleImportTypes();
            Types.ImportTypes = GlobalData.VehImportType();
            return View(Types);
        }

        #endregion

        #region LocationClasses
        public ActionResult LocationClass()
        {
            LocationClasses C = new LocationClasses();
            C.Classes = GlobalData.GetLOCLocationClasses();
            return View(C);
        }
        #endregion

        #region QuotationConvert
        public ActionResult QuotationConverting()
        {
            QuotationConvert Q = new QuotationConvert();
            return View(Q);
        }

        [HttpPost]
        public ActionResult QuotationConverting(QuotationConvert Quotes)
        {
            QuotationConvert Q = new QuotationConvert();
            Q.Quote = GlobalData.QuotationConvert(Quotes.QuotationNumber);
            return View(Q);
        }

        #endregion

        #region GetGlobalNames
        public ActionResult GetGlobalNames()
        {
            GlobalNameGet G = new GlobalNameGet();
            return View(G);
        }

        [HttpPost]
        public ActionResult GetGlobalNames(GlobalNameGet GN)
        {
            GlobalNameGet G = new GlobalNameGet();
            G.GlobalNames = GlobalData.GlobalNameSearch(GN.GlobalNumber);
            return View(G);
        }
        #endregion

        #region BrokerChecks
        public ActionResult BrokerChecking()
        {
            BrokerCheck B = new BrokerCheck();
            return View(B);
        }

        [HttpPost]
         public ActionResult BrokerChecking(BrokerCheck Broker)
        {
            BrokerCheck B = new BrokerCheck();
            B.BrokerResponse = GlobalData.BrokerChecking(Broker);
            return View(B);
        }

        #endregion

        #region ClientChecks
        public ActionResult ClentChecking()
        {
            ClientCheck C = new ClientCheck();
            return View(C);
        }

        [HttpPost]
        public ActionResult ClentChecking(ClientCheck Client)
        {
            ClientCheck C = new ClientCheck();
            C.ClientResponse = GlobalData.ClientChecking(Client);
            return View(C);
        }
        #endregion

        #region ConcatenatedMakesAndModels
        public ActionResult ConcatenatedMakesAndModels()
        {
            MakeModels M = new MakeModels();
            return View(M);
        }

        [HttpPost]
        public ActionResult ConcatenatedMakesAndModels(MakeModels Makes)
        {
            MakeModels M = new MakeModels();
            M.MakesAndModels = GlobalData.ConcatMakesAndModels(Makes.PolicyType, Makes.ConcatList);
            return View(M);
        }
        #endregion

        #region SubmitQuotation
        public ActionResult RequestQuotation()
        {
            return RedirectToAction("Index", "RequestQuotation");
        }
        #endregion

        #region FindQuotation
        public ActionResult FindQuotation()
        {
            return RedirectToAction("Index", "FindQuotation");
        }
        #endregion

        #region DoNotInsure
        public ActionResult DoNotInsure()
        {
            return RedirectToAction("Index", "DoNotInsure");
        }
        #endregion

        #region BrokerStatus
        public ActionResult BrokerStatus()
        {
            return RedirectToAction("BrokerStatus", "AllMethods");
        }
        #endregion

        #region CLientStatus
        public ActionResult ClientStatus()
        {
            return RedirectToAction("ClientStatus", "AllMethods");
        }
        #endregion

        #region GetGlobalNamePolicies
        public ActionResult GetGlobalNamePolicy()
        {
            GlobalNamePolices GPolicies = new GlobalNamePolices();
            return View(GPolicies);
        }
        
        [HttpPost]
        public ActionResult GetGlobalNamePolicy(GlobalNamePolices GPolicies)
        {
            GPolicies.GlobalNamesPolicies = GlobalData.GlobalNamePolicies(GPolicies.GlobalNameID);
            return View(GPolicies);
        }

        #endregion

        #region GetPolicyExtensions
        public ActionResult GetPolicyExtension()
        {
            GetPolicyExtensions PE = new GetPolicyExtensions();
            return View(PE);
        }

        [HttpPost]
        public ActionResult GetPolicyExtension(GetPolicyExtensions PE)
        {
            try
            {
                PE.PolicyData = GlobalData.PolicyExtensions(PE.PolicyID);
            }
            catch (System.Exception e)
            {

                return Json(new {ErrorMessage =  e.Message});
            }
          
            return View(PE);
        }
        #endregion

        #region GetPolicyLimts
        public ActionResult GetPolicyLimit()
        {
            PolicyLimit PL = new PolicyLimit();
            return View(PL);
        }

        [HttpPost]
        public ActionResult GetPolicyLimit(PolicyLimit PL)
        {
            PL.PolicyData = GlobalData.PolicyLimits(PL.PolicyID);
            return View(PL);
        }
        #endregion

        #region PolicyTransactions
        public ActionResult GetPolicyTransactions()
        {
            PolicyTransactions PT = new PolicyTransactions();
            return View(PT);
        }

        [HttpPost]
        public ActionResult GetPolicyTransactions(PolicyTransactions PT)
        {
            PT.PolicyData = GlobalData.PolicyTrans(PT.PolicyID);
            return View(PT);
        }
        #endregion

        #region PolicyRisks
        public ActionResult GetPolicyRisk()
        {
            GetPolicyRisks PR = new GetPolicyRisks();
            return View(PR);
        }

        [HttpPost]
        public ActionResult GetPolicyRisk(GetPolicyRisks PR)
        {
            PR.PolicyData = GlobalData.PolicyRisks(PR.PolicyID);
            return View(PR);
        }
        #endregion

        #region PolicyClaims
        public ActionResult PolicyClaim()
        {
            PolicyClaims PC = new PolicyClaims();
            return View(PC);
        }

        [HttpPost]
        public ActionResult PolicyClaim(PolicyClaims PC)
        {
            PC.PolicyData = GlobalData.PolicyClaims(PC.PolicyID);
            return View(PC);
        }
        #endregion

        #region MapPolicyPrefix

        public ActionResult MapPrefix()
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            foreach( JToken Code in GlobalData.GetPrefixMapping()["data"].Children())
            {
                DB.ADDPolicyPrefix(Code.ToString(), Code.ToString());
               
            }
           
            var PolicyPrefix = from P in DB.PolicyPrefixes
                               orderby P.Code 
                               select P;
            return View(PolicyPrefix.ToList());
        }

        public ActionResult MapPrefixEdit(string Code)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.PolicyPrefixes
                               orderby P.Code
                               where P.Code == Code
                               select P;

            return View(PolicyPrefix.SingleOrDefault());
        }

        [HttpPost]
        public ActionResult MapPrefixEdit(Models.DBModel.PolicyPrefix Pr)
        {
           
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
           
            var PolicyPrefix = from P in DB.PolicyPrefixes
                               orderby P.Code where P.Code == Pr.Code
                               select P;

            PolicyPrefix.SingleOrDefault().Value = Pr.Value;
            DB.SaveChanges();

            return RedirectToAction("MapPrefix");
        }
        #endregion
        
        #region MapUsages

        public ActionResult MapUsages()
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            foreach (JToken Code in GlobalData.GetUsageMapping()["data"].Children())
            {
                DB.ADDUsages(Code["code"].ToString(), Code["code"].ToString());
            }


            var PolicyPrefix = from P in DB.UsageCodes
                               orderby P.Code
                               select P;
            return View(PolicyPrefix.ToList());
        }

        public ActionResult MapUsagesEdit(string Code)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.UsageCodes
                               orderby P.Code
                               where P.Code == Code
                               select P;

            return View(PolicyPrefix.SingleOrDefault());
        }

        [HttpPost]
        public ActionResult MapUsagesEdit(Models.DBModel.UsageCode Pr)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.UsageCodes
                               orderby P.Code
                               where P.Code == Pr.Code
                               select P;

            PolicyPrefix.SingleOrDefault().Value = Pr.Value;
            DB.SaveChanges();


            return RedirectToAction("MapUsages");
        }
        #endregion

        #region MapAuthDrivers

        public ActionResult MapAuthDrivers()
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            foreach (JToken Code in GlobalData.GetDriverWordingsMapping()["data"].Children())
            {
                DB.ADDAuthDrivers(Code.ToString(), Code.ToString());

            }


            var PolicyPrefix = from P in DB.AuthorizedDrivers
                               orderby P.Code
                               select P;

            return View(PolicyPrefix.ToList());
        }

        public ActionResult MapAuthDriversEdit(string Code)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.AuthorizedDrivers
                               orderby P.Code
                               where P.Code == Code
                               select P;

            return View(PolicyPrefix.SingleOrDefault());
        }

        [HttpPost]
        public ActionResult MapAuthDriversEdit(Models.DBModel.AuthorizedDriver Pr)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            //var PolicyPrefix = from P in DB.AuthorizedDrivers
            //                   orderby P.Code
            //                   where P.Code == Pr.Code
            //                   select P;

            //PolicyPrefix.SingleOrDefault().Value = Pr.Value;
            //DB.SaveChanges();

            try
            {
              Utilities.OmaxData.HandleQuery("Update AuthorizedDrivers set Value = '" + Pr.Value + "' where Code = '" + Pr.Code + "'"
                            , ConnectionString, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.NoneQuery);

            }
            catch (System.Exception)
            {

              
            }

          

            return RedirectToAction("MapAuthDrivers");
        }
        #endregion

        #region MapTransactionWording

        public ActionResult MapTransactionWording()
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            foreach (JToken Code in GlobalData.epic_mwGetTransWording()["trans_wordings"].Children())
            {
                DB.ADDTransactionWordings(Code.ToString(), Code.ToString());

            }


            var PolicyPrefix = from P in DB.TransactionWordings
                               orderby P.Code
                               select P;

            return View(PolicyPrefix.ToList());
        }

        public ActionResult MapTransactionWordingEdit(string Code)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.TransactionWordings
                               orderby P.Code
                               where P.Code == Code
                               select P;

            return View(PolicyPrefix.SingleOrDefault());
           
        }

        [HttpPost]
        public ActionResult MapTransactionWordingEdit(Models.DBModel.TransactionWording Pr)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.TransactionWordings
                               orderby P.Code
                               where P.Code == Pr.Code
                               select P;

            PolicyPrefix.SingleOrDefault().Value = Pr.Value;
            DB.SaveChanges();


            return RedirectToAction("MapTransactionWording");
        }


        [HttpPost]
        public ActionResult MapTransactionWording(Models.DBModel.AuthorizedDriver Pr)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            //var PolicyPrefix = from P in DB.AuthorizedDrivers
            //                   orderby P.Code
            //                   where P.Code == Pr.Code
            //                   select P;

            //PolicyPrefix.SingleOrDefault().Value = Pr.Value;
            //DB.SaveChanges();

            try
            {
                Utilities.OmaxData.HandleQuery("Update TransactionWordings set Value = '" + Pr.Value + "' where Code = '" + Pr.Code + "'"
                              , ConnectionString, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.NoneQuery);

            }
            catch (System.Exception)
            {


            }



            return RedirectToAction("MapTransactionWordings");
        }
        #endregion

        #region MapRatingCodes

        public ActionResult MapRatingCodes()
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            foreach (JToken Code in GlobalData.epic_mwGetAllRatingCodes()["rating_codes"].Children())
            {
                DB.ADDRatingCodes(Code.ToString(), Code.ToString());

            }


            var PolicyPrefix = from P in DB.RatingCodes
                               orderby P.Code
                               select P;

            return View(PolicyPrefix.ToList());
        }

        public ActionResult MapRatingCodesEdit(string Code)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.RatingCodes
                               orderby P.Code
                               where P.Code == Code
                               select P;

            return View(PolicyPrefix.SingleOrDefault());

        }

        [HttpPost]
        public ActionResult MapRatingCodesEdit(Models.DBModel.TransactionWording Pr)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var PolicyPrefix = from P in DB.RatingCodes
                               orderby P.Code
                               where P.Code == Pr.Code
                               select P;

            PolicyPrefix.SingleOrDefault().Value = Pr.Value;
            DB.SaveChanges();


            return RedirectToAction("MapRatingCodes");
        }


        //[HttpPost]
        //public ActionResult MapRatingCodesEdit(Models.DBModel.AuthorizedDriver Pr)
        //{

        //    Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

        //    //var PolicyPrefix = from P in DB.AuthorizedDrivers
        //    //                   orderby P.Code
        //    //                   where P.Code == Pr.Code
        //    //                   select P;

        //    //PolicyPrefix.SingleOrDefault().Value = Pr.Value;
        //    //DB.SaveChanges();

        //    try
        //    {
        //        Utilities.OmaxData.HandleQuery("Update RatingCode set Value = '" + Pr.Value + "' where Code = '" + Pr.Code + "'"
        //                      , ConnectionString, Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.NoneQuery);

        //    }
        //    catch (System.Exception)
        //    {


        //    }



        //    return RedirectToAction("MapRatingCodes");
        //}
        #endregion

        #region Submit Files
        public ActionResult GetFile()
        {
            return View();
        }

        [HttpPost]
        public ActionResult GetFile(HttpPostedFileBase File)
        {
            byte[] fileData = null;

            if (File.ContentLength > 0)
            {

                using (var binaryReader = new BinaryReader(File.InputStream))
                {
                    fileData = binaryReader.ReadBytes(File.ContentLength);
                }

            }

            TempData["FileByte"] = File;
            TempData["FileData"] = fileData;
            TempData["ContentSize"] = File.ContentLength;
            TempData["FileNames"] = File.FileName;
            TempData.Keep();

            return RedirectToAction("SubmitFile");
        }

        public ActionResult SubmitFile()
        {
            return View(new FileSubmit());
        }

        [HttpPost]
        public ActionResult SubmitFile(FileSubmit File)
        {
            try
            {
                if (TempData["FileByte"] != null)
                {
                    TempData.Keep();
                    File.Files = (HttpPostedFileBase)TempData["FileByte"];
                    TempData.Keep();
                }
            }
            catch (Exception)
            {
            
            }
                      
                int ContentLength = int.Parse(TempData["ContentSize"].ToString());
                byte[] FilesX = null;
                TempData.Keep();
                FilesX = (byte[])TempData["FileData"];
                TempData.Keep();
                        
            JObject Files = new JObject();

            Files.Add("table",File.table);
            Files.Add("record_id",File.record_id);
            Files.Add("file_id",File.file_id);
            if(File.file_name != string.Empty || File.file_name != null)
            {
                Files.Add("file_name", File.file_name);
            }else
            {
                Files.Add("file_name", File.Files.FileName);
            }
            
            Files.Add("file_type",File.Files.ContentType.Substring(File.Files.ContentType.IndexOf("/") + 1));
            Files.Add("file_data", System.Convert.ToBase64String(FilesX));
            Files.Add("description", File.description);

            File.FileData = GlobalData.SubmitFile(Files);

            return View(File);
        }
        #endregion

        #region PolicyProfExt

        public ActionResult PolicyProfileExtentions()
        {

            return View(new PolicyProfileExtension());
        }

        [HttpPost]
        public ActionResult PolicyProfileExtentions( PolicyProfileExtension ppe )
        {

            ppe.policData = GlobalData.PolicyProfExt(ppe.policyPrefix);
            return View(ppe);
        }

        #endregion

        #region PolicyProfLOL

        public ActionResult PolicyProfileLimitsOfLiability()
        {

            return View(new PolicyProfileExtension());
        }

        [HttpPost]
        public ActionResult PolicyProfileLimitsOfLiability(PolicyProfileExtension pplol)
        {

            pplol.policData = GlobalData.PolicyProfLOL(pplol.policyPrefix);
            return View(pplol);
        }

        #endregion

        #region PolicyProfiles

        public ActionResult PolicyProfiles()
        {

            return View(new PolicyProfileExtension());
        }

        [HttpPost]
        public ActionResult PolicyProfiles(PolicyProfileExtension pp)
        {

            pp.policData = GlobalData.PolicyProfile(pp.policyPrefix);
            return View(pp);
        }

        #endregion

        #region PolicyProfileRatingCodes

        public ActionResult PolicyProfileRatingCodes()
        {

            return View(new PolicyProfileExtension());
        }

        [HttpPost]
        public ActionResult PolicyProfileRatingCodes(PolicyProfileExtension pprc)
        {

            pprc.policData = GlobalData.PolicyProfileRatingCodes(pprc.policyPrefix);
            return View(pprc);
        }

        #endregion

        #region PolicyProfileRangeVals

        public ActionResult PolicyProfileRangeValues()
        {

            return View(new PolicyProfileExtension());
        }

        [HttpPost]
        public ActionResult PolicyProfileRangeValues(PolicyProfileExtension pprv)
        {

            pprv.policData = GlobalData.PolicyProfileRangeVals(pprv.policyPrefix);
            return View(pprv);
        }

        #endregion

        #region PolicyProfileRatings

        public ActionResult PolicyProfileRatings()
        {

            return View(new PolicyProfileExtension());
        }

        [HttpPost]
        public ActionResult PolicyProfileRatings(PolicyProfileExtension ppr)
        {

            ppr.policData = GlobalData.PolicyProfileRatings(ppr.policyPrefix);
            return View(ppr);
        }

        #endregion

        #region GetPolicy
        public ActionResult GetPolicy()
        {
            
            return View(new GetPolicy());
        }

        [HttpPost]
        public ActionResult GetPolicy(GetPolicy GOP)
        {
            GetPolicy GOPS = new Models.GetPolicy();
            GOPS.PolicyData = GlobalData.GetPolicy(GOP.PolicyID);        
            return View(GOPS);
        }

        #endregion

        #region Register Company

        public ActionResult RegisterCompany()
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            //Models.DBModel.Company1 Comp = new Models.DBModel.Company1();

            var CurrentCompany = (from C in DB.Companies1 select C).Take(1);
            //Comp = CurrentCompany.SingleOrDefault();
            return View(CurrentCompany.SingleOrDefault());
        }

        [HttpPost]
        public ActionResult RegisterCompany(Models.DBModel.Company1 Comp)
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            Models.DBModel.Company1 Comps = new Models.DBModel.Company1();

            string Keys = "";
            int CompanyID2 = 0;
            DataTable ISEXISITNG = new DataTable();

            ISEXISITNG = (DataTable)Utilities.OmaxData.HandleQuery("EXEC CompanyExist '" + Comp.CompanyName + "'", System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString, 
                         Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);

            bool ISALREADYTHERE = false;

            foreach(DataRow I in ISEXISITNG.Rows){
                try
                {
                    ISALREADYTHERE = (bool)I[0];
                }
                catch (System.Exception)
                {

                }

            }
           
           
            if(ISALREADYTHERE == false)
            {

                ISEXISITNG = (DataTable)Utilities.OmaxData.HandleQuery("exec RegisterCompany '" + Comp.CompanyName
                                + "','" + Comp.Address
                                + "','" + Comp.Phone
                                + "','" + Comp.Email
                                + "','" + Comp.ContactPerson + "'", System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
                                Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);

                foreach(DataRow Row in ISEXISITNG.Rows)
                {
                    CompanyID2 = int.Parse(Row["CompID"].ToString());

                    DataTable KeySource = new DataTable();

                    KeySource = (DataTable) Utilities.OmaxData.HandleQuery("exec GetNewKey", System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
                                Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);

                    foreach(DataRow Ky in KeySource.Rows)
                    {
                        Keys = Ky[0].ToString();
                    }

                    DataTable DSX = new DataTable();
                    DSX = (DataTable) Utilities.OmaxData.HandleQuery("exec CreateLiscense '" + Utilities.InformationHiding.MakeIllusion(Comp.CompanyName + ";" + CompanyID2.ToString() + ";" + Keys + ";" + DateTime.Now.AddDays(30))
                    + "','" + CompanyID2.ToString()
                    + "','" + Utilities.InformationHiding.MakeIllusion(DateTime.Now.AddDays(30).ToString())
                    + "'," + CompanyID2.ToString()
                    + ",'" + Utilities.InformationHiding.MakeIllusion(Keys) + "'", System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString,
                                Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.ScalarQuery);


                    foreach(DataRow Rk in DSX.Rows)
                    {
                        Comp.LicInfo = Rk[0].ToString();
                    }
                                 
                }
               
            } else
            {
                Comp.LicInfo = "This company is already registered!";
            }
           
            return View(Comp);
        }

        #endregion

        #region Action Word Mapping
        public ActionResult MapActions()
        {
            JObject ActionsMap = new JObject();
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            ActionsMap = GlobalData.epic_mwPolicyModActions();

            foreach(JToken Actions in ActionsMap["actions"].Children())
            {
                DB.ADDWordingActions(Actions["action"].ToString(), Actions["trans_wordings"].Count());
                    string ActioName = Actions["action"].ToString();
                    var T = from Tw in DB.ActionWordings where Tw.ActionName == ActioName  select Tw;
                    int ActionID = int.Parse(T.SingleOrDefault().ActionID.ToString());

                foreach (JToken Word in Actions["trans_wordings"].Children())
                {
                   
                    Utilities.OmaxData.HandleQuery("Exec AddWordingProperty '" + Word.ToString() + "'," +  ActionID.ToString() , GlobalData.GetConnectionString()
                        ,Utilities.OmaxData.DataBaseType.Sql,Utilities.OmaxData.QueryType.NoneQuery);
                }

               
            }


            var ActionTable = from Ac in DB.ActionWordings select Ac;
            return View(ActionTable.ToList());
        }

        private JObject GetInputStream()
        {
            Stream ReqStream = Request.InputStream;
            ReqStream.Seek(0, SeekOrigin.Begin);
            string IncommingData = new StreamReader(ReqStream).ReadToEnd();
            return JObject.Parse(IncommingData);
        }

        [HttpPost]
        public JObject AddWord()
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            int ActionID = int.Parse(GetInputStream()["ActionID"].ToString());
            int WordID = int.Parse(GetInputStream()["WordID"].ToString());
            string Word = GetInputStream()["Wording"].ToString();


            JObject Result = new JObject();

            try
            {

                var Words = from Word_ in DB.TransactionWordingsActions where Word_.WordID == WordID select Word_;

                var WW = Words.SingleOrDefault();
                WW.TransactionWording = Word;
                DB.SaveChanges();

                var Wordings = from W in DB.TransactionWordingsActions where W.ActionID == ActionID select W;

                Result.Add("Wordings", JArray.Parse(JsonConvert.SerializeObject(Wordings.ToList())));
            }
            catch (Exception)
            {
                Result.Add("Success", false);
               
            }
           
            return Result;
        }



        [HttpPost]
        public JObject DeleteWord( int WordID, int ActionID)
        {

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            JObject Result = new JObject();

            try
            {
                DB.RemoveWordingActions(WordID);
                Result.Add("Success", true);

                var Wordings = from W in DB.TransactionWordingsActions where W.ActionID == ActionID select W;

                Result.Add("Wordings", JArray.Parse(JsonConvert.SerializeObject(Wordings.ToList())));
            }
            catch (Exception)
            {
                Result.Add("Success", false);

            }

            return Result;
        }

        #endregion

        #region Register Key

        public ActionResult RegisterKeys()
        {
            return View(new RegisterKey());
        }

        [HttpPost]
        public ActionResult RegisterKeys(RegisterKey Keys)
        {
            JObject Lic = new JObject();
            Lic.Add("LicKey", Keys.RKey);
            Keys.RKeyResult =  GlobalData.RegisterMiddleWare(Lic);
            return View(Keys);
        }


        #endregion

        #region AuditLog

        public ActionResult ViewAuditLog()
        {
            ViewData["ip"] = "";
            //List<Ucon.Models.DBModel.GetAuditLogs_Result> d = AuditLog.getAuditLogs();

            return View(new Models.DBModel.MiddleAccsEntities().AuditLogs.Take(100).OrderByDescending(p => p.LogID).ToList());

            //return View(AuditLog.getAuditLogs(new DateTime(),new DateTime()));
        }

        [HttpPost]
        public ActionResult ViewAuditLog( DateTime? startdate = null, DateTime? enddate = null, string ip = null )
        {
            DateTime start = new DateTime();
            DateTime end = new DateTime();
            if (startdate != null) start = (DateTime)startdate;
            if (enddate != null) end = (DateTime)enddate;

            try
            {
                end = enddate.Value.AddDays(1);
            }
            catch (Exception)
            {

            }
            

            ViewData["ip"] = ip;

            var Data = new Models.DBModel.MiddleAccsEntities().AuditLogs.Where(o => o.LogDate <= end && o.LogDate >= start  ).OrderByDescending(p => p.LogID).ToList();

            return View(Data);
        }
               

        #endregion

        #region Map BrokerOne Servers
        public ActionResult BrokerServer(int BrokerID = 0)
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
   
            if (BrokerID == 0)
            {
                return View();
            }
            else
            {
                var Servers = from S in DB.BorkerServers orderby S.BrokerID descending where S.BrokerID == BrokerID select S;
                return View(Servers.SingleOrDefault());
            }
        }
        [HttpPost]
        public ActionResult BrokerServer(Models.DBModel.BorkerServer Broker)
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();


            var BrokerCompany = from B in DB.BorkerServers where B.CompanyCode == Broker.CompanyCode select B;
            var BrokerExist = BrokerCompany.SingleOrDefault();
            if (BrokerExist != null)
            {
                
                BrokerExist.BrokerName = Broker.BrokerName;
                BrokerExist.IP = Broker.IP;
                BrokerExist.CompanyCode = Broker.CompanyCode;
                DB.SaveChanges();

                return View();
            }
            else {

                if (Broker.BrokerID == 0)
                {
                    var NewBroker = DB.BorkerServers.Add(new Models.DBModel.BorkerServer
                    {
                        BrokerName = Broker.BrokerName,
                        IP = Broker.IP,
                        CompanyCode = Broker.CompanyCode
                    });
                    DB.SaveChanges();
                    return View(NewBroker);
                }
                else
                {
                    var Servers = from S in DB.BorkerServers orderby S.BrokerID descending where S.BrokerID == Broker.BrokerID select S;
                    var FoundBroker = Servers.SingleOrDefault();
                    FoundBroker.BrokerName = Broker.BrokerName;
                    FoundBroker.CompanyCode = Broker.CompanyCode;
                    FoundBroker.IP = Broker.IP;
                    DB.SaveChanges();

                    return View();
                }
            }
        }

        //Delete Broker
        public ActionResult DeleteBrokerServer(int BrokerID = 0)
        {

            if (BrokerID != 0)
            {
                Utilities.OmaxData.HandleQuery("Delete from BorkerServers where BrokerID = " + BrokerID.ToString(), GlobalData.GetConnectionString()
                    , Utilities.OmaxData.DataBaseType.Sql, Utilities.OmaxData.QueryType.NoneQuery);
            }

            return RedirectToAction("BrokerServers");
        }


        //Gets a list of existing Broker Servers
        public ActionResult BrokerServers(string BrokerName = "")
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            try
            {
                foreach (var br in GlobalData.GetBrokerCodes()["brokers"].Children())
                {
                    DB.AddCompanyCode(br["code"].ToString(), br["name"].ToString());
                }
            }
            catch (Exception)
            {

            }

            if (BrokerName != "")
            {
                var Servers = from S in DB.BorkerServers where S.BrokerName.Contains(BrokerName) || S.CompanyCode.Contains(BrokerName) || S.IP.Contains(BrokerName) select S;
                return View(Servers.ToList());
            }
            else
            {
                var Servers = from S in DB.BorkerServers select S;
                return View(Servers.ToList());
            }

        }

        //Edit Server
        public JObject EditServer(int ServerID = 0)
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            var Servers = from S in DB.BorkerServers where S.BrokerID == ServerID select S;
            return JObject.Parse(JsonConvert.SerializeObject(Servers));
        }

        //Save Changes on server
        public JObject EditServerSave()
        {
            //Get incomming values from POST request here
            int BrokerID = 0;
            BrokerID = int.Parse(GetInputStream()["BrokerID"].ToString());
            //Assigning new values
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
            var Servers = from S in DB.BorkerServers where S.BrokerID == BrokerID select S;
            if (Servers.ToList().Count() > 0)
            {
                Servers.SingleOrDefault().BrokerName = GetInputStream()["BrokerName"].ToString();
                Servers.SingleOrDefault().IP = GetInputStream()["URL"].ToString();
                Servers.SingleOrDefault().CompanyCode = GetInputStream()["CompanyCode"].ToString();
                DB.SaveChanges();
            }
            else
            {
                var NewServer = DB.BorkerServers.Add(new Models.DBModel.BorkerServer
                {
                    BrokerName = GetInputStream()["CompanyCode"].ToString(),
                    IP = GetInputStream()["URL"].ToString(),
                    CompanyCode = GetInputStream()["CompanyCode"].ToString()
                });
            }
            return JObject.Parse(JsonConvert.SerializeObject(Servers));
        }
        #endregion

        #region ManageSettings

        /// <summary>
        /// Gets existing Settings
        /// </summary>
        /// <returns></returns>
        public JArray GetSettings()
        {
            var Settings = from S in new Models.DBModel.MiddleAccsEntities().SettingConfigs select S;
            //Settings.SingleOrDefault().AmPassword = Utilities.InformationHiding.GetDecoded(Settings.SingleOrDefault().AmPassword).ToString();
            return JArray.Parse(JsonConvert.SerializeObject(Settings));
        }

        [HttpPost]
        public ActionResult SaveSettings()
        {
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            int SettingID = 0;
            try
            {
                SettingID = int.Parse(GetInputStream()["SettingsID"].ToString());
            }
            catch (Exception)
            {

            }
            

            var Settings = from S in DB.SettingConfigs where S.SettingsID == SettingID select S;
            var FoundSettings = Settings.SingleOrDefault();

            FoundSettings.EnableIPWhiteList = bool.Parse(GetInputStream()["EnableIPWhiteList"].ToString());
            FoundSettings.EnableSSL = bool.Parse(GetInputStream()["EnableSSL"].ToString());
            FoundSettings.AmPlusURL = GetInputStream()["AMPlusURL"].ToString();
            FoundSettings.AmUsername = GetInputStream()["AmUsername"].ToString();
            FoundSettings.AmPassword = GetInputStream()["AmPassword"].ToString();
            FoundSettings.DocumentUploadURL = GetInputStream()["DocumentUploadURL"].ToString();
            FoundSettings.AmPlusTextNotificationURL = GetInputStream()["AmPlusTextNotificationURL"].ToString();
            FoundSettings.GlobalDefaultDestination = GetInputStream()["GlobalDefaultDestination"].ToString();
            //FoundSettings.AmPassword = Utilities.InformationHiding.MakeIllusion(GetInputStream()["AmPassword"].ToString()).ToString();
            DB.SaveChanges();

            return RedirectToRoute("GetSettings");
          
        }

        

        //Gets all available ip settings
        public ActionResult IPWhiteList(string IP = "")
        {
            if (IP == "")
            {
                var IPLIST = from S in new Models.DBModel.MiddleAccsEntities().IPWhiteLists select S;
                return View(IPLIST.ToList());
            }
            else
            {
                var IPLIST = from S in new Models.DBModel.MiddleAccsEntities().IPWhiteLists where S.IP == IP select S;
                return View(IPLIST.ToList());
            }
           
        }

        //Gets an IP for Editing
        public JObject EditIP(int IPID)
        {
            var IPLIST = from S in new Models.DBModel.MiddleAccsEntities().IPWhiteLists where S.IPID == IPID select S;
            return JObject.Parse(JsonConvert.SerializeObject(IPLIST.SingleOrDefault()));
        }


        //Saves IP settings
        [HttpPost]
        public ActionResult SaveIP()
        {
            int IPID = int.Parse(GetInputStream()["IPID"].ToString());

            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            var IPLIST = from S in DB.IPWhiteLists where S.IPID == IPID select S;
            var FoundIP = IPLIST.SingleOrDefault();

            FoundIP.IP = GetInputStream()["IP"].ToString();
            FoundIP.IPDescription = GetInputStream()["IPDescription"].ToString();
            FoundIP.IsBlocked = bool.Parse(GetInputStream()["IsBlocked"].ToString());
            DB.SaveChanges();
            return View();
        }



        //Adds an IP settings
        [HttpPost]
        public ActionResult AddIP()
        {
            JObject Res = new JObject();
            Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();

            bool IsBlocked = false;

            try
            {
                IsBlocked = bool.Parse(GetInputStream()["IsBlocked"].ToString());
            }
            catch (Exception)
            {
                IsBlocked = false;
            }

            try
            {
            DB.IPWhiteLists.Add(new Models.DBModel.IPWhiteList
            {
            IP = GetInputStream()["IP"].ToString(),
            IPDescription = GetInputStream()["IPDescription"].ToString(),
            IsBlocked = IsBlocked
            });
 
            DB.SaveChanges();
            Res.Add("success", true);
            }
            catch (Exception)
            {

                Res.Add("success", false); ;
            }
            return View(Res);
        }




        //Remove an IP form the list
        public JObject RemoveIP(int IPID = 0)
        {
            JObject Res = new JObject();

            try
            {
              
                Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
                DB.DeleteIP(IPID);

                Res.Add("success", true);
            }
            catch (Exception)
            {

                Res.Add("success", false); ;
            }
            return Res;
        }



        #endregion

        #region Manage SMTP Settings
        //GetSMTP Settings
        public JObject GetSMTP()
        {
            var SMTP = (from Servers in (new Models.DBModel.MiddleAccsEntities()).SMTP_SERVER select Servers).Take(1);
            return JObject.Parse(JsonConvert.SerializeObject(SMTP.SingleOrDefault()));
        }

        /// <summary>
        /// Gets ftp information
        /// </summary>
        /// <returns></returns>
        public JObject Getftp()
        {
            var SMTP = (from Servers in (new Models.DBModel.MiddleAccsEntities()).SettingConfigs select Servers).Take(1);
            return JObject.Parse(JsonConvert.SerializeObject(SMTP.SingleOrDefault()));
        }

        /// <summary>
        /// Get Ftp Users
        /// </summary>
        /// <returns></returns>
        public JArray GetftpUsers()
        {
            var SMTP = (from Servers in (new Models.DBModel.MiddleAccsEntities()).AspNetUsers select Servers);
            return JArray.Parse(JsonConvert.SerializeObject(SMTP.ToList()));
        }

        /// <summary>
        /// Gets a User Id for selected user
        /// </summary>
        /// <param name="UID"></param>
        /// <returns></returns>
        public JObject GetftpUserID(string UID = "")
        {
            var SMTP = (from Servers in (new Models.DBModel.MiddleAccsEntities()).AspNetUsers where Servers.Id == UID select Servers ).Take(1);
            return JObject.Parse(JsonConvert.SerializeObject(SMTP.SingleOrDefault()));
        }


        public JObject GetftpUser(string UID = "")
        {
            var SMTP = (from Servers in (new Models.DBModel.MiddleAccsEntities()).AspNetUsers where Servers.UserName == UID select Servers).Take(1);
            return JObject.Parse(JsonConvert.SerializeObject(SMTP.SingleOrDefault()));
        }



        //Updates ftp Server settings
        public JObject SaveFtpSettings(string ftp_Url, string ftp_Username, string ftp_Password, string ftp_User, string ftp_basePath)
        {
            Models.DBModel.MiddleAccsEntities DBX = new Models.DBModel.MiddleAccsEntities();
            var SMTP = from Servers in DBX.SettingConfigs select Servers;

            var Details = SMTP.SingleOrDefault();
            Details.ftp_URL = ftp_Url;
            Details.ftp_UserName = ftp_Username;
            Details.ftp_Password= ftp_Password;
            if(ftp_User != null || ftp_User.Trim() != "")
            {
                Details.ftp_UserID = ftp_User;
            }
          
            Details.ftp_basePath = ftp_basePath;
            DBX.SaveChanges();

            JObject J = new JObject();
            J.Add("success", true);
            return J;

        }



        //Updates SMTP Server settings
        public JObject SaveSMTP(string ServerUrl, string Username, string Password, string PortNumber, string SenderName)
        {
            Models.DBModel.MiddleAccsEntities DBX = new Models.DBModel.MiddleAccsEntities();
            var SMTP = from Servers in DBX.SMTP_SERVER select Servers;

            var Details = SMTP.SingleOrDefault();
            Details.Password = Password;
            Details.PortNumber = PortNumber;
            Details.SenderName = SenderName;
            Details.ServerUrl = ServerUrl;
            Details.Username = Username;

            DBX.SaveChanges();

            JObject J = new JObject();
            J.Add("success", true);
            return J;

        }


        #endregion


        //Get available sources
    
        public JObject GetSources()
        {
            return GlobalData.epic_mwGetSources();
        }

        public ActionResult GetAllSources()
        {
            return View();
        }


        public ActionResult TestRow()
        {
           return View();
        }


        public JObject SendMessage()
        {
            SendEmail Email = new SendEmail();

            Email.SendEmails("Testing Send Grid", "New SMTP Service", "jhyman@etechja.com", "agayle@etechja.com");
            return new JObject();
        }

    }

}
