﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Ucon.Models
{
    public class FindQuotationController : Controller
    {
        // GET: FindQuotation
        public ActionResult Index()
        {
            QuotationGet RetrieveQuote = new QuotationGet();

            return View(RetrieveQuote);
        }



        [HttpPost]
        public ActionResult Index(QuotationGet Rquote)
        {
            Controllers.RequestQuotationController.GetQuotation(Rquote.QuotationNumber);
            return RedirectToAction("QuotationResponseDetails", "RequestQuotation");
        }
                      

    }


}
