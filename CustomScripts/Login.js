﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

var LoggedInUsername, LoggedInPassword;

(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);
    
    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        Ld.addEventListener('click', InputUserInfo(), false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };


    var LoginAnimation = "<div id=\"fountainG\">"
  + "<div id=\"fountainG_1\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_2\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_3\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_4\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_5\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_6\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_7\" class=\"fountainG\"></div>"
  + "<div id=\"fountainG_8\" class=\"fountainG\"></div>"
+ "</div>";


    $(document).on('click', ".SignIN", function () {
        document.getElementById("LoadingAnimation").innerHTML = LoginAnimation;
        LoggedInUsername = document.getElementById("username").value;
        LoggedInPassword = document.getElementById("password").value;

        if (document.getElementById("RemMe").value == "on") {
            var d = new Date();
            d.setTime(d.getTime() + (10 * 24 * 60 * 60 * 1000));

            document.cookie ="username=" + LoggedInUsername + ";expires=Thu," + d.toUTCString();
            document.cookie = "password=" + LoggedInPassword + ";expires=Thu," + d.toUTCString();
        }

        var Logins = { "Login": { "username": LoggedInUsername, "password": LoggedInPassword } }
        LoginUser("RequestLogin", JSON.stringify(Logins));

    });


    function InputUserInfo() {
        
       document.getElementById("username").value = getCookie("username");
       document.getElementById("password").value = getCookie("password");
    }

    //Get cookies
    function getCookie(cname) {
        var name = cname + "=";
        var ca = document.cookie.split(';');
        for (var i = 0; i < ca.length; i++) {
            var c = ca[i];
            while (c.charAt(0) == ' ') c = c.substring(1);
            if (c.indexOf(name) == 0) return c.substring(name.length, c.length);
        }
        return "";
    }


    function LoginUser(Method, RequestBody) {

        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/" + Method,
            data: RequestBody,
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessRequest,
            error: ErrorRequest
        });

    }

    function SuccessRequest(response) {
        if (response[0].Response[0].Login == true) {
        window.location.href = "index.html?LoggedInUsername=" + LoggedInUsername + "&?LoggedInPassword=" + LoggedInPassword;
        } else { alert(response[0].Response[0].Comments); }
       
    };

    function ErrorRequest(response) {
        alert(response.responseText);
    };


   



})();