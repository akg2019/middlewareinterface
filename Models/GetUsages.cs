﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class Usages
    {
        public string Code { set; get; }
        public string Description { set; get; }
    }
    public class GetUsages
    {
        public string PolicyPrefix { set; get; }
        public JObject Usage = new JObject();
        public List<Usages> Lists = new List<Usages>();
    }
}