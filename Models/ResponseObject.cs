﻿using System.Collections.Generic;

namespace Ucon.Models
{
    public class ResponseObjects
    {
        public class ResponseList
        {
            public List<Response> Response { get; set; }
        }

        public class Response
        {
        public string key { set; get; }
        public bool Login { set; get; }
        public string Info { set; get; }
        public string Comments { set; get; }
        }

       
        
    }
}

