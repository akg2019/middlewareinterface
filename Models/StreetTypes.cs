﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class StreetTypes
    {
        public JObject Streettype = new JObject();
        public List<string> Lists = new List<string>();
    }
}