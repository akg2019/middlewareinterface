﻿var ReqKey;
var LoggedInUsername, LoggedInPassword;
var ClearFixes = "<div class=\"ClearFixes\"></div>";
var Controls = "";

    //Check Brokers

    
    // Get Data from form controls
    function GetBrokerData() {
        var Brokers = {
            "company_name": document.getElementById("company_name").value,
            "first_name": document.getElementById("first_name").value,
            "last_name": document.getElementById("last_name").value,
            "email": document.getElementById("email").value,
            "national_id": document.getElementById("national_id").value,
            "national_id_type": document.getElementById("national_id_type").value,
            "policy_number_1": document.getElementById("policy_number_1").value,
            "policy_number_2": document.getElementById("policy_number_2").value,
            "policy_number_3": document.getElementById("policy_number_3").value
        };

        GetBrokerResponse(Brokers);
    };

    function GetBrokerResponse(SubmitData) {
        $.ajax({
            type: "POST",
            url: "AllMethods/IsBroker",
            data: JSON.stringify(SubmitData),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessBroker,
            error: ErrorBroker
        });

    };

    function SuccessBroker(response) {
        var GResponse;
        if (response.success == true) {
            GResponse = response.global_name_id;
        } else { GResponse = response.error_message; }

        var GlobalNameResponse = "<p class=\"VaribaleLables\"><strong>Global Name ID: </strong></p><table class=\"AlertMaxWidth\"><tr><td class =\"TdStyle\">" + GResponse + "</td></tr></table>"
                                 + "<p class=\"VaribaleLables\">Success: </p> <p>" + response.success + "</p>";
        document.getElementById("ModalContent").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\"><p class=\"TextGlobalName>" + GlobalNameResponse + "</p></div>";
        //document.getElementById("CheckBtn").style.display = "none";
    }

    function ErrorBroker() {
        document.getElementById("ModalContent").innerHTML = "<div class=\"alert alert-danger AlertMaxWidth\" role=\"alert\">Failed to connect to UnderWriter (:<p>" + response.error_message + "</p></div>";
    }

    //Loading Animation
    var LoadANimation = "<div class=\"cssload-dots\">"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                    + "</div>"
                    + "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
                      + "<defs>"
                          + "<filter id=\"goo\">"
                              + "<feGaussianBlur in=\"SourceGraphic\" result=\"blur\" stdDeviation=\"12\" ></feGaussianBlur>"
                              + "<feColorMatrix in=\"blur\" mode=\"matrix\" values=\"1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7\" result=\"goo\" ></feColorMatrix>"
                              + "<!--<feBlend in2=\"goo\" in=\"SourceGraphic\" result=\"mix\" ></feBlend>-->"
                          + "</filter>"
                      + "</defs>"
                    + "</svg>"
                    + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>";



    $(document).on("click", ".CheckBrokers", function () {
        Controls = document.getElementById("ModalContent").innerHTML;
        GetBrokerData();
        document.getElementById("ModalContent").innerHTML = LoadANimation;
        window.location.href = "#"
        //document.getElementById("CheckBtn").style.display = "none";
    })


    $(document).on('click', ".ChkBtn", function () {
        if (Controls != "") {
            document.getElementById("ModalContent").innerHTML = Controls;
            //Controls = "";
        }

        document.getElementById("CheckBtn").style.display = "normal"

        //if (document.getElementById("CheckBtn").style.display = "none")
        //{
        //    document.getElementById("CheckBtn").style.display = "normal";
        //}
        
    })
