//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Ucon.Models.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class Global_Names
    {
        public int GobalID { get; set; }
        public int CompanyID { get; set; }
        public string Global_NamesID { get; set; }
        public string company_name { get; set; }
        public string dob { get; set; }
        public string drivers_licence_country { get; set; }
        public string drivers_licence_date_issued { get; set; }
        public string drivers_licence_first_issued { get; set; }
        public string drivers_licence_number { get; set; }
        public string email_address { get; set; }
        public string employment_type { get; set; }
        public string first_name { get; set; }
        public string last_name { get; set; }
        public string gender { get; set; }
        public string global_name_number { get; set; }
        public string is_a_company { get; set; }
        public string is_a_service_provider { get; set; }
        public string maiden_name { get; set; }
        public string mailing_name { get; set; }
        public string marital_status { get; set; }
        public string middle_name { get; set; }
        public string national_id { get; set; }
        public string national_id_type { get; set; }
        public string nationality { get; set; }
        public string notes { get; set; }
        public string occupation { get; set; }
        public string occupation_code { get; set; }
        public string occupation_description { get; set; }
        public string phone_number_fax { get; set; }
        public string phone_number_general { get; set; }
        public string phone_number_mobile { get; set; }
        public string place_of_birth { get; set; }
        public string salutation_name { get; set; }
        public string service_type { get; set; }
        public string tax_id_number { get; set; }
        public string title { get; set; }
        public Nullable<System.DateTime> Modified { get; set; }
        public Nullable<System.Guid> UserID { get; set; }
        public string UserName { get; set; }
    
        public virtual Company Company { get; set; }
    }
}
