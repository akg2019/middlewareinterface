﻿using System.Collections.Generic;
using Newtonsoft.Json.Linq;

namespace Ucon.Models
{

    public class SubmitQuotation

    {
        public static List<phone_numbers> Phone_Numbers = new List<phone_numbers>();
        public static List<risk> Risk = new List<risk>();
        public static List<drivers> Drivers = new List<drivers>();
        public static List<manual_rates> ManualRates = new List<manual_rates>();
        public static vehicle_location VehLoc = new vehicle_location();
        public static address Adds = new address();
        public static resquestkey r = new resquestkey();
        public static List<JObject> DropDownListData = new List<JObject>();   
    }


    public class resquestkey
    {
        public string key { set; get; }
    }

    public class quotation
    {
        //Quotation Section
        public string success { set; get; }
        public string quotation_number { set; get; }
        public string policy_prefix { set; get; }
        public string first_name { set; get; }
        public string middle_name { set; get; }
        public string last_name { set; get; }
        public string national_id { set; get; }
        public string national_id_type { set; get; }
        public string email_address { set; get; }
        public string company_name { set; get; }
        public string start_date { set; get; }
        public bool  accidents_in_last_few_years { set; get; }
        public string occupation { set; get; }
        public string currency { set; get; }
        public bool  is_motor_cat_perils_covered { set; get; }
        public List<phone_numbers> phone_numbers { set; get; }
        public address address { set; get; }
        public risk risk { set; get; }
    }

    //Risk
    public class risk
    {
        public string sum_insured { set; get; }
        public string usage_code { set; get; }
        public string authorized_driver_wording { set; get; }
        public string year_for_rating { set; get; }
        public string year { set; get; }
        public string make { set; get; }
        public string model { set; get; }
        public string model_type { set; get; }
        public string extension { set; get; }
        public string body_type { set; get; }
        public string body_shape { set; get; }
        public string hp_cc_rating { set; get; }
        public string hp_cc_unit_type { set; get; }
        public string chassis_number { set; get; }
        public string colour { set; get; }
        public string number_of_cyclinders { set; get; }
        public string engine_modified { set; get; }
        public string engine_number { set; get; }
        public string engine_type { set; get; }
        public string has_electric_doors { set; get; }
        public string has_electric_side_mirror { set; get; }
        public string has_electric_window { set; get; }
        public string has_power_steering { set; get; }
        public string import_type { set; get; }
        public string ncd_percent { set; get; }
        public string left_hand_drive { set; get; }
        public string main_driver_dob { set; get; }
        public string main_driver_licence_first_issued { set; get; }
        public string mileage { set; get; }
        public string mileage_type { set; get; }
        public string number_of_doors { set; get; }
        public string number_of_engines { set; get; }
        public string registration_number { set; get; }
        public string roof_type { set; get; }
        public string seat_type { set; get; }
        public string seating { set; get; }
        public string tonnage { set; get; }
        public string transmission_type { set; get; }
        public vehicle_location vehicle_locations { set; get; }
       
    }


    //Phone numbers
    public class phone_numbers
        {
            public string type { set; get; }
            public string carrier { set; get; }
            public string description { set; get; }
            public string extension_or_range { set; get; }
            public string phone_number { set; get; }
        }


        //Address section
        public class address
        {
            public string altitude { set; get; }
            public string block { set; get; }
            public string building_number { set; get; }
            public string categorization { set; get; }
            public string complex_name { set; get; }
            public string country { set; get; }
            public string general_area { set; get; }
            public string international_area { set; get; }
            public string is_international_address { set; get; }
            public string island { set; get; }
            public string latitude { set; get; }
            public string locaction_class { set; get; }
            public string longitude { set; get; }
            public string parish { set; get; }
            public string postal_zip_code { set; get; }
            public string street_name { set; get; }
            public string street_number { set; get; }
            public string street_type { set; get; }
            public string town { set; get; }
            public string unit_number { set; get; }
        }

    //Drivers 
    public class driver
    {
        public string is_excluded { set; get; }
        public string is_included_by_exception { set; get; }
        public string national_id { set; get; }
        public string is_main_driver { set; get; }
        public string national_id_type { set; get; }
        public string relation_to_insured { set; get; }

    }

    public class drivers
        {
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string driver_dob { set; get; }
            public string drivers_licence_first_issued { set; get; }
            public string drivers_licence_number { set; get; }
            public string is_main_driver { set; get; }
                
        }

        //Vehicle Location
        public class vehicle_location
        {
            public string altitude { set; get; }
            public string block { set; get; }
            public string building_number { set; get; }
            public string categorization { set; get; }
            public string complex_name { set; get; }
            public string country { set; get; }
            public string general_area { set; get; }
            public string international_area { set; get; }
            public string is_international_address { set; get; }
            public string island { set; get; }
            public string latitude { set; get; }
            public string locaction_class { set; get; }
            public string longitude { set; get; }
            public string parish { set; get; }
            public string postal_zip_code { set; get; }
            public string street_name { set; get; }
            public string street_number { set; get; }
            public string street_type { set; get; }
            public string town { set; get; }
            public string unit_number { set; get; }
        }

        //Manual Rates
        public class manual_rates
        {
            public string code { set; get; }
            public float value { set; get; }
        
        }
    
}
