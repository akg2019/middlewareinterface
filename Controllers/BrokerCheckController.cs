﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class BrokerCheckController : Controller
    {
        // GET: BrokerCheck
        public ActionResult Index()
        {
            BrokerCheck Broker = new BrokerCheck();
            return View(Broker);
        }

        [HttpPost]
        public ActionResult Index(BrokerCheck Broker)
        {
            JObject ReqObject = new JObject();
            ReqObject.Add("key", KeyManagement.ApiKey());
            Broker.resquestkey = ReqObject;

            string UnderWriterResponse = KeyManagement.PerformRequest("epic_mwIsBroker",
            KeyManagement.RequestType.TwoParameters, "All", Broker);
            JObject MyResponse = JObject.Parse(UnderWriterResponse);

            if(MyResponse["global_name_number"] != null)
            {
            Broker.GlobalNameNumber = int.Parse(MyResponse["global_name_number"].ToString());
            Broker.Success = (bool)(MyResponse["success"]);
            }

            return RedirectToAction("IndexResult",Broker);
            
        }

        public ActionResult IndexResult(BrokerCheck Broker)
        {
           return View(Broker);
        }



        // GET: BrokerCheck/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: BrokerCheck/Create
        public ActionResult Create()
        {
            var Broker = new Models.BrokerCheck();

            return View(Broker);
        }

        // POST: BrokerCheck/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here
                return View("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BrokerCheck/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BrokerCheck/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BrokerCheck/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BrokerCheck/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
