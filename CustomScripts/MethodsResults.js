﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.

var Tasks;
var SearchTitle;
var LoggedInUsername, LoggedInPassword;



document.addEventListener('deviceready', onDeviceReady.bind(this), false);

function onDeviceReady() {
    // Handle the Cordova pause and resume events
    document.addEventListener('pause', onPause.bind(this), false);
    document.addEventListener('resume', onResume.bind(this), false);
    UsernameID.addEventListener('click', DisplayUser(), false);
    ShowResult.addEventListener('click', DrawOutPut(), false);

    // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
};

function onPause() {
    // TODO: This application has been suspended. Save application state here.
};

function onResume() {
    // TODO: This application has been reactivated. Restore application state here.
};



$(document).on('click', ".hm", function () {
    window.location.href = "index.html?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword");
});

function DisplayUser() {
    document.getElementById("UsernameID").innerHTML = "Welecome\n" + getParameterByName("LoggedInUsername");
}


var Logins;
var ChangePassword = { ChangePassword: { username: "string", password: "string", newpassword: "string" } };


function DrawOutPut() {
    LoggedInUsername = getParameterByName("LoggedInUsername");
    LoggedInPassword = getParameterByName("LoggedInPassword");
    Logins = { "Login": { "username": LoggedInUsername, "password": LoggedInPassword } };
    Tasks = getParameterByName("Tasks");
    var ReqBody;
    if (getParameterByName("RequestBody") == "") {
        ReqBody = JSON.stringify(Logins);
    }
    document.getElementById("Titles").innerHTML = LoadANimation;
    SubmitQuote(getParameterByName("Method"), ReqBody);

}

//Gets QueryStrings
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    //alert(results);

    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

}

$(document).on('click', ".ForgetMe", function () {
    document.cookie = "username=username; expires=Thu, 01 Jan 1970 00:00:00 UTC";
    document.cookie = "password=password; expires=Thu, 01 Jan 1970 00:00:00 UTC";
})

$(document).on('click', ".LogOut", function () {
    LoggedInPassword = "";
    LoggedInUsername = "";
    window.location.href = "index.html";
})


//Requesting a Motor Vehicle Quotation
function MotorQuotes() {

}

//Requesting a Property Quotation
function PropertiesQuotes() {

}

//Quotation number that is passed to return an existing quotation
function QuoteByNumber() {

}

//This method generates a quotation in Underwriter using the data passed through the JSON string and returns a calculated premium.
function epic_mwSubmitQuotation() {

}

//This method returns a JSON string containing an array of usages for the policy prefix that is passed.
function epic_mwGetUsages() {

}

//This method returns a JSON string containing an array of all makes and models for the vehicle Type passed.
function epic_mwGetVehMakeModels() {

}

//This method returns an array of all vehicle roof types.
function epic_mwGetVehRoofType() {

}

//This method returns a JSON string containing an array of all authorized driver wordings for the policy_prefix that is passed.
function epic_mwGetVehAuthDrivers() {

}

//This method returns a JSON string containing an array of all transmission types in the system.
function epic_mwGetVehTransmissionType() {

}

//This method returns a JSON string containing an array of all the import types in the system.
function epic_mwGetVehImportType() {

}

//This method returns a JSON string containing an array of all Vehicle colours in the system.
function epic_mwGetVehColour() {

}

//This method returns JSON data for a previously submitted quotation.
function epic_mwGetQuotation() {

}

//This method converts a quotation to a policy by using the quotation_number supplied.
function epic_mwConvertQuotation() {

}

var LoadANimation = "<div class=\"cssload-dots\">"
+ "<div class=\"cssload-dot\"></div>"
+ "<div class=\"cssload-dot\"></div>"
+ "<div class=\"cssload-dot\"></div>"
+ "<div class=\"cssload-dot\"></div>"
+ "<div class=\"cssload-dot\"></div>"
+ "</div>"
+ "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
+ "<defs>"
  + "<filter id=\"goo\">"
      + "<feGaussianBlur in=\"SourceGraphic\" result=\"blur\" stdDeviation=\"12\" ></feGaussianBlur>"
      + "<feColorMatrix in=\"blur\" mode=\"matrix\" values=\"1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7\" result=\"goo\" ></feColorMatrix>"
      + "<!--<feBlend in2=\"goo\" in=\"SourceGraphic\" result=\"mix\" ></feBlend>-->"
  + "</filter>"
+ "</defs>"
+ "</svg>"
+ "<div class=\"ClearFix\"></div>"
+ "<div class=\"ClearFix\"></div>"
+ "<div class=\"ClearFix\"></div>"
+ "<div class=\"ClearFix\"></div>"
+ "<div class=\"ClearFix\"></div>"
+ "<div class=\"ClearFix\"></div>"
+ "<div class=\"ClearFix\"></div>";


//Make the Requests

function SubmitQuote(Method, RequestBody) {
    var SendRequest = { MethodName: "" };
    SendRequest.MethodName = Method;
    $.ajax({
        type: "POST",
        url: "AllMethods/" + Method.replace("epic_mw", ""),
        data: JSON.stringify(SendRequest),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessRequest,
        error: ErrorRequest
    });

}

var Badges = "<div class=\"pull-right pusshSmall PaddingRight\"><a href=\"#\"><span class=\"badge BadgeColor pull-right\">@@#42#$%</span></a></div>"
var DisplayRows = "<div class=\"alert alert-success\" role=\"alert\">@#$%^&!</div>";
var Header = "<div class=\"alert alert-info AlertOverride\" role=\"alert\">*()%$#@<a href=\"#\"><span  class=\"badge BadgeColor pull-right\">@@#42#$%</span></a></div><hr>";
var ClearFixes = "<div class=\"ClearFixes\"></div>";

var Colors, Usages, RoofTypes, AuthDrivers, TransmissionTypes, ImportTypes, Countries, GeneralAreas, Islands, Classes, ParishTowns, StreetNames, StreetTypes, ZipCodes, Occupation, PropertyRoofTypes, PropertyWallTypes;
var Models = "";
var ModelType = "";
var Extension = "";
var BodyType = "";
var BodyShape = "";
var Makes = "";
var CountryOption = "";
var DrawDocument = "";
Colors = "";
Usages = "";
RoofTypes = "";
AuthDrivers = "";
TransmissionTypes = "";
ImportTypes = "";
Countries = "";
GeneralAreas = "";
Islands = "";
Classes = "";
ParishTowns = "";
StreetNames = "";
StreetTypes = "";
ZipCodes = "";
Occupation = "";
PropertyRoofTypes = "";
PropertyWallTypes = "";

var MakeModelDisplay = "<div class=\"list-group\">"
                         + "<a href=\"#\" class=\"list-group-item active\">"
                         + "$#@%^Title*&^%$"
                         + "</a>"
                         + "$#@Rows(*&^"
                         + "</div>";
var RowsMakeModels = "<a href=\"#\" class=\"list-group-item\">#$%RowContents&^%@#$</a>";


function SuccessRequest(response) {

    //Gets Colors
    if (Tasks == "Colors") {
        for (var a = 0; a < response.data.length; a++) {
            Colors += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[a]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Colors").replace("$#@Rows(*&^", Colors);
        DrawDocument = Header.replace("*()%$#@", "Colors").replace("@@#42#$%", a) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", a);

    }

    //Gets Usages
    if (Tasks == "Usages") {
        for (var b = 0; b < response.data.length; b++) {
            Usages += RowsMakeModels.replace("#$%RowContents&^%@#$", "<p class=\"VaribaleLables\">Code</p>"
                                    + response.data[b].code + "<hr><p class=\"VaribaleLables\">Description</p>"
                                    + response.data[b].description)


        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Usages").replace("$#@Rows(*&^", Usages);
        DrawDocument = Header.replace("*()%$#@", "Usages").replace("@@#42#$%", b) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", b);
    }

    //Gets Make and Model

    if (Tasks == "MakeAndModel") {

        for (var i = 0; i < response.data.length; i++) {

            if (response.data[i].make !== "") { Makes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[i].make); }

            for (var j = 0; j < response.data[i].models.length; j++) {

                if (response.data[i].models[j].model !== "") { Models += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[i].models[j].model); }
                if (response.data[i].models[j].model_type !== "") { ModelType += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[i].models[j].model_type); }
                if (response.data[i].models[j].extension !== "") { Extension += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[i].models[j].extension); }
                if (response.data[i].models[j].body_type !== "") { BodyType += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[i].models[j].body_type); }
                if (response.data[i].models[j].body_shape !== "") { BodyShape += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[i].models[j].body_shape); }

            }

            var FinishedMake, FinishedModel, FinishedModelType, FinishedExtension, FInishedBodyType, FinishedBodyShape;

            FinishedMake = MakeModelDisplay.replace("$#@%^Title*&^%$", "Makes").replace("$#@Rows(*&^", Makes);
            FinishedModel = MakeModelDisplay.replace("$#@%^Title*&^%$", "Models").replace("$#@Rows(*&^", Models);
            FinishedModelType = MakeModelDisplay.replace("$#@%^Title*&^%$", "Model Types").replace("$#@Rows(*&^", ModelType);
            FinishedExtension = MakeModelDisplay.replace("$#@%^Title*&^%$", "Extensions").replace("$#@Rows(*&^", Extension);
            FInishedBodyType = MakeModelDisplay.replace("$#@%^Title*&^%$", "Body Types").replace("$#@Rows(*&^", BodyType);
            FinishedBodyShape = MakeModelDisplay.replace("$#@%^Title*&^%$", "Body Shapes").replace("$#@Rows(*&^", BodyShape);


            //if (i != response.data.response.data.length) {
            //    Makes += "\"" + response.data[i].make + "\",";
            //} else {
            //    Makes += "\"" + response.data[i].make + "\"";
            //}
        }
        SearchTitle = Badges.replace("@@#42#$%", i);
        DrawDocument = Header.replace("*()%$#@", "Make and Models").replace("@@#42#$%", i) + FinishedMake + ClearFixes
                                                                    + FinishedModel + ClearFixes
                                                                    + FinishedModelType + ClearFixes
                                                                    + FinishedExtension + ClearFixes
                                                                    + FInishedBodyType + ClearFixes
                                                                    + FinishedBodyShape + ClearFixes;
    }


    //Gets Roof Types
    if (Tasks == "RoofTypes") {
        for (var c = 0; c < response.data.length; c++) {
            RoofTypes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[c]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Roof Types").replace("$#@Rows(*&^", RoofTypes);
        DrawDocument = Header.replace("*()%$#@", "Roof Types").replace("@@#42#$%", c) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", c);
    }


    //Gets AuthorizedDriver
    if (Tasks == "AuthDrivers") {
        for (var d = 0; d < response.data.length; d++) {
            AuthDrivers += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[d]);
        }
        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Authorized Drivers").replace("$#@Rows(*&^", AuthDrivers);
        DrawDocument = Header.replace("*()%$#@", "Authorized Drivers").replace("@@#42#$%", d) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", d);
    }


    //Gets Transmission Types
    if (Tasks == "TransmissionTypes") {
        for (var e = 0; e < response.data.length; e++) {
            TransmissionTypes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[e]);
        }
        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Transmission Types").replace("$#@Rows(*&^", TransmissionTypes);
        DrawDocument = Header.replace("*()%$#@", "Transmission Types").replace("@@#42#$%", e) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", e);
    }

    //Gets Import Types
    if (Tasks == "ImportTypes") {
        for (var f = 0; f < response.data.length; f++) {
            ImportTypes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[f]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Import Types").replace("$#@Rows(*&^", ImportTypes);
        DrawDocument = Header.replace("*()%$#@", "Import Types").replace("@@#42#$%", f) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", f);
    }
    //@@###

    //Gets All Countries
    if (Tasks == "Countries") {
        for (var g = 0; g < response.data.length; g++) {
            Countries += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[g]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Countries").replace("$#@Rows(*&^", Countries);
        DrawDocument = Header.replace("*()%$#@", "Countries").replace("@@#42#$%", g) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", g);
    }

    if (Tasks == "Countries") {
        for (var h = 0; h < response.data.length; h++) {
            CountryOption += "<option>" + DisplayRows.replace("@#$%^&!", response.data[h]) + "</option>";
        }

    }


    //Gets All General Areas
    if (Tasks == "GeneralArea") {
        for (var k = 0; k < response.data.length; k++) {
            if (response.data[k] != "") {
                GeneralAreas += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[k]);
            }
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "General Areas").replace("$#@Rows(*&^", GeneralAreas);
        DrawDocument = Header.replace("*()%$#@", "General Areas").replace("@@#42#$%", k) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", k);
    }

    //Gets Islands
    if (Tasks == "Islands") {
        for (var m = 0; m < response.data.length; m++) {
            Islands += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[m]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Islands").replace("$#@Rows(*&^", Islands);
        DrawDocument = Header.replace("*()%$#@", "Islands").replace("@@#42#$%", m) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", m);
    }

    //Gets Classes
    if (Tasks == "Classes") {
        for (var n = 0; n < response.data.length; n++) {
            Classes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[n]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Classes").replace("$#@Rows(*&^", Classes);
        DrawDocument = Header.replace("*()%$#@", "Classes").replace("@@#42#$%", n) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", n);
    }


    //Gets All Parishes and Towns
    var CompletedParish = "";
    if (Tasks == "ParishAndTown") {
        for (var o = 0; o < response.data.length; o++) {
            var Parish = "<strong>" + response.data[o].parish + "</strong>"
                       + "<p><strong><i>Towns<i></strong></p>";

            for (var z = 0; z < response.data[o].towns.length; z++) {
                ParishTowns += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[o].towns[z]);
            }

            CompletedParish += Parish + ParishTowns;
            ParishTowns = "";
        }



        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Parish and Towns").replace("$#@Rows(*&^", CompletedParish);
        DrawDocument = Header.replace("*()%$#@", "Parish and Towns").replace("@@#42#$%", o) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", o);
    }


    //Gets All Street Names
    if (Tasks == "StreetNames") {
        for (var p = 0; p < response.data.length; p++) {
            StreetNames += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[p]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Street Names").replace("$#@Rows(*&^", StreetNames);
        DrawDocument = Header.replace("*()%$#@", "Street Names").replace("@@#42#$%", p) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", p);
    }

    //Gets All Street Types
    if (Tasks == "StreetTypes") {
        for (var q = 0; q < response.data.length; q++) {
            StreetTypes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[q]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Street Types").replace("$#@Rows(*&^", StreetTypes);
        DrawDocument = Header.replace("*()%$#@", "Street Types").replace("@@#42#$%", q) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", q);
    }

    //Gets All Zip Codes
    if (Tasks == "ZipCodes") {
        for (var r = 0; r < response.data.length; r++) {
            ZipCodes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[r]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Zip Codes").replace("$#@Rows(*&^", ZipCodes);
        DrawDocument = Header.replace("*()%$#@", "Zip Codes").replace("@@#42#$%", r) + FinishedDisplay;
        SearchTitle = "List of Zip Codes " + Badges.replace("@@#42#$%", r);
    }

    //Gets All Occupation
    if (Tasks == "Occupation") {
        for (var s = 0; s < response.data.length; s++) {
            Occupation += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[s].occupation + " ; " + response.data[s].industry);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Occupation").replace("$#@Rows(*&^", Occupation);
        DrawDocument = Header.replace("*()%$#@", "Occupation").replace("@@#42#$%", s) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", s);
    }

    //Gets All property roof types
    if (Tasks == "PropertyRoofTypes") {
        for (var t = 0; t < response.data.length; t++) {
            PropertyRoofTypes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[t]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Property Roof Types").replace("$#@Rows(*&^", PropertyRoofTypes);
        DrawDocument = Header.replace("*()%$#@", "Property Roof Types").replace("@@#42#$%", t) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", t);
    }

    //Gets All property wall types
    if (Tasks == "PropertyWallTypes") {
        for (var u = 0; u < response.data.length; u++) {
            PropertyWallTypes += RowsMakeModels.replace("#$%RowContents&^%@#$", response.data[u]);
        }

        var FinishedDisplay = MakeModelDisplay.replace("$#@%^Title*&^%$", "Property Wall Types").replace("$#@Rows(*&^", PropertyWallTypes);
        DrawDocument = Header.replace("*()%$#@", "Property Wall Types").replace("@@#42#$%", u) + FinishedDisplay;
        SearchTitle = Badges.replace("@@#42#$%", u);
    }

    //Check Global Names
    if (Tasks == "CheckGlobalNames") {

    }

    //Check Brokers
    if (Tasks == "CheckBroker") {

    }

    //Check Clients
    if (Tasks == "CheckClient") {

    }


    document.getElementById("Titles").innerHTML = DrawDocument;
    //document.getElementById("titlesearch").innerHTML = SearchTitle;
};

var ErrorWelll = "<div class=\"row\">"
+ "<div class=\"col-sm-6 col-md-4\">"
+ "<div class=\"thumbnail\">"
  + "<img src=\"images/sadface.png\" alt=\"Error in connection\">"
  + "<div class=\"caption\">"
+ "<h3>Ooops! Errors encountered</h3>"
+ "<p>@!#$%Error$%#^&</p>"
+ "<p><a href=\"index.html\" class=\"btn btn-primary\" role=\"button\">Go back to Home</a>"
+ "</div>"
+ "</div>"
+ "</div>"
+ "</div>";


function ErrorRequest(response) {
    document.getElementById("Titles").innerHTML = ErrorWelll.replace("@!#$%Error$%#^&", "<div class=\"alert alert-danger\" role=\"alert\">" + response.responseText + "</div>");
};


DrawOutPut();

