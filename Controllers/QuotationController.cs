﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class QuotationController : Controller
    {
        private JObject GetInputStream()
        {
            Stream ReqStream = Request.InputStream;
            ReqStream.Seek(0, SeekOrigin.Begin);
            string IncommingData = new StreamReader(ReqStream).ReadToEnd();
            return JObject.Parse(IncommingData);
        }


        quotation QuotationManager = new quotation();


        // GET: Quotation
        public ActionResult Index()
        {
            quotation Quotation = new quotation();
            return View(Quotation);
        }


       [HttpPost]
        public ActionResult Index(quotation Quote)
        {
            try
            {

                //Main 
                Tempquotation Tempquotations = new Tempquotation();
                Tempquotations.accidents_in_last_few_years = Quote.accidents_in_last_few_years;
                Tempquotations.company_name = Quote.company_name;
                Tempquotations.company_name = Quote.currency;
                Tempquotations.email_address = Quote.email_address;
                Tempquotations.first_name = Quote.first_name;
                Tempquotations.is_motor_cat_perils_covered = Quote.is_motor_cat_perils_covered;
                Tempquotations.last_name = Quote.last_name;
                Tempquotations.middle_name = Quote.middle_name;
                Tempquotations.national_id = Quote.national_id;
                Tempquotations.national_id_type = Quote.national_id_type;
                Tempquotations.occupation = Quote.occupation;
                Tempquotations.policy_prefix = Quote.policy_prefix;
                Tempquotations.quotation_number = Quote.quotation_number;
                Tempquotations.start_date = Quote.start_date;


                //Address Section
                address Address = new address();
                Address.altitude = Quote.address.altitude;
                Address.block = Quote.address.block;
                Address.building_number = Quote.address.building_number;
                Address.categorization = Quote.address.categorization;
                Address.complex_name = Quote.address.complex_name;
                Address.country = Quote.address.country;
                Address.general_area = Quote.address.general_area;
                Address.international_area = Quote.address.international_area;
                Address.island = Quote.address.island;
                Address.is_international_address = Quote.address.is_international_address;
                Address.latitude = Quote.address.latitude;
                Address.locaction_class = Quote.address.locaction_class;
                Address.longitude = Quote.address.longitude;
                Address.parish = Quote.address.parish;
                Address.postal_zip_code = Quote.address.postal_zip_code;
                Address.street_name = Quote.address.street_name;
                Address.street_number = Quote.address.street_number;
                Address.street_type = Quote.address.street_type;
                Address.town = Quote.address.town;
                Address.unit_number = Quote.address.unit_number;


                //Risk Section
                QuotationManager.risk.authorized_driver_wording = Quote.risk.authorized_driver_wording;
                QuotationManager.risk.body_shape = Quote.risk.body_shape;
                QuotationManager.risk.body_type = Quote.risk.body_type;
                QuotationManager.risk.chassis_number = Quote.risk.chassis_number;
                QuotationManager.risk.colour = Quote.risk.colour;
                QuotationManager.risk.engine_modified = Quote.risk.engine_modified;
                QuotationManager.risk.engine_number = Quote.risk.engine_number;
                QuotationManager.risk.engine_type = Quote.risk.engine_type;
                QuotationManager.risk.extension = Quote.risk.extension;
                QuotationManager.risk.has_electric_doors = Quote.risk.has_electric_doors;
                QuotationManager.risk.has_electric_side_mirror = Quote.risk.has_electric_side_mirror;
                QuotationManager.risk.has_electric_window = Quote.risk.has_electric_window;
                QuotationManager.risk.has_power_steering = Quote.risk.has_power_steering;
                QuotationManager.risk.hp_cc_rating = Quote.risk.hp_cc_rating;
                QuotationManager.risk.hp_cc_unit_type = Quote.risk.hp_cc_unit_type;
                QuotationManager.risk.import_type = Quote.risk.import_type;
                QuotationManager.risk.left_hand_drive = Quote.risk.left_hand_drive;
                QuotationManager.risk.main_driver_dob = Quote.risk.main_driver_licence_first_issued;
                QuotationManager.risk.make = Quote.risk.make;
                QuotationManager.risk.mileage = Quote.risk.mileage;
                QuotationManager.risk.mileage_type = Quote.risk.mileage_type;
                QuotationManager.risk.model = Quote.risk.model;
                QuotationManager.risk.model_type = Quote.risk.model_type;
                QuotationManager.risk.ncd_percent = Quote.risk.ncd_percent;
                QuotationManager.risk.number_of_cyclinders = Quote.risk.number_of_cyclinders;
                QuotationManager.risk.number_of_doors = Quote.risk.number_of_doors;
                QuotationManager.risk.number_of_engines = Quote.risk.number_of_engines;
                QuotationManager.risk.registration_number = Quote.risk.registration_number;
                QuotationManager.risk.roof_type = Quote.risk.roof_type;
                QuotationManager.risk.seating = Quote.risk.seating;
                QuotationManager.risk.seat_type = Quote.risk.seat_type;
                QuotationManager.risk.sum_insured = Quote.risk.sum_insured;
                QuotationManager.risk.tonnage = Quote.risk.tonnage;
                QuotationManager.risk.transmission_type = Quote.risk.transmission_type;
                QuotationManager.risk.usage_code = Quote.risk.usage_code;
                QuotationManager.risk.year = Quote.risk.year;
                QuotationManager.risk.year_for_rating = Quote.risk.year_for_rating;



                //Vehicle Location Section
                QuotationManager.risk.vehicle_locations.altitude = Quote.risk.vehicle_locations.altitude;
                QuotationManager.risk.vehicle_locations.block = Quote.risk.vehicle_locations.block;
                QuotationManager.risk.vehicle_locations.building_number = Quote.risk.vehicle_locations.building_number;
                QuotationManager.risk.vehicle_locations.categorization = Quote.risk.vehicle_locations.categorization;
                QuotationManager.risk.vehicle_locations.complex_name = Quote.risk.vehicle_locations.complex_name;
                QuotationManager.risk.vehicle_locations.country = Quote.risk.vehicle_locations.country;
                QuotationManager.risk.vehicle_locations.general_area = Quote.risk.vehicle_locations.general_area;
                QuotationManager.risk.vehicle_locations.international_area = Quote.risk.vehicle_locations.international_area;
                QuotationManager.risk.vehicle_locations.island = Quote.risk.vehicle_locations.island;
                QuotationManager.risk.vehicle_locations.is_international_address = Quote.risk.vehicle_locations.is_international_address;
                QuotationManager.risk.vehicle_locations.latitude = Quote.risk.vehicle_locations.latitude;
                QuotationManager.risk.vehicle_locations.locaction_class = Quote.risk.vehicle_locations.locaction_class;
                QuotationManager.risk.vehicle_locations.longitude = Quote.risk.vehicle_locations.longitude;
                QuotationManager.risk.vehicle_locations.parish = Quote.risk.vehicle_locations.parish;
                QuotationManager.risk.vehicle_locations.postal_zip_code = Quote.risk.vehicle_locations.postal_zip_code;
                QuotationManager.risk.vehicle_locations.street_name = Quote.risk.vehicle_locations.street_name;
                QuotationManager.risk.vehicle_locations.street_number = Quote.risk.vehicle_locations.street_number;
                QuotationManager.risk.vehicle_locations.street_type = Quote.risk.vehicle_locations.street_type;
                QuotationManager.risk.vehicle_locations.town = Quote.risk.vehicle_locations.town;
                QuotationManager.risk.vehicle_locations.unit_number = Quote.risk.vehicle_locations.unit_number;




                JObject QuotationObject = new JObject();
                QuotationObject.Add("quotation", JsonConvert.SerializeObject(Tempquotations));
                QuotationObject.Add("phone_numbers", JsonConvert.SerializeObject(SubmitQuotation.Phone_Numbers));
                QuotationObject.Add("address", JsonConvert.SerializeObject(QuotationManager.address));
                QuotationObject.Add("address", JsonConvert.SerializeObject(QuotationManager.address));

                JObject RiskObject = new JObject();
                RiskObject.Add(null, JsonConvert.SerializeObject(QuotationManager.risk));
                RiskObject.Add("drivers", JsonConvert.SerializeObject(SubmitQuotation.Drivers));
                RiskObject.Add("manual_rates", JsonConvert.SerializeObject(SubmitQuotation.ManualRates));

                QuotationObject.Add("risk", RiskObject);



                return RedirectToAction("Index");
            }
            catch
            {
               
            }


            return View(Quote);
        }

        private class Tempquotation
        {
            public string quotation_number { set; get; }
            public string policy_prefix { set; get; }
            public string first_name { set; get; }
            public string middle_name { set; get; }
            public string last_name { set; get; }
            public string national_id { set; get; }
            public string national_id_type { set; get; }
            public string email_address { set; get; }
            public string company_name { set; get; }
            public string start_date { set; get; }
            public bool accidents_in_last_few_years { set; get; }
            public string occupation { set; get; }
            public string currency { set; get; }
            public bool is_motor_cat_perils_covered { set; get; }
        }


        [HttpPost]
        public ActionResult AddPhoneNumbers(phone_numbers Quote)
        {

            return View(Quote);
        }


        [HttpPost]
        public void AddPhoneNumbers()
        {
            try
            {
                Stream ReqStream = Request.InputStream;
                ReqStream.Seek(0, SeekOrigin.Begin);
                string Phone = new StreamReader(ReqStream).ReadToEnd();
                JObject PhoneObject = JObject.Parse(Phone);
                List<JToken> PhoneList = new List<JToken>();
                PhoneList = PhoneObject.Children().ToList();
                phone_numbers PhoneProperty = new phone_numbers();
                PhoneProperty.carrier = PhoneObject["carrier"].ToString();
                PhoneProperty.type = PhoneObject["type"].ToString();
                PhoneProperty.extension_or_range = PhoneObject["extension_or_range"].ToString();
                PhoneProperty.description = PhoneObject["description"].ToString();
                PhoneProperty.phone_number = PhoneObject["phone_number"].ToString();
                SubmitQuotation.Phone_Numbers.Add(PhoneProperty);
            }
            catch
            {

            }
        }



    }
}
