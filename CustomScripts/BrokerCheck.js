﻿var ReqKey;
var LoggedInUsername, LoggedInPassword;
var ClearFixes = "<div class=\"ClearFixes\"></div>";


    //Check Brokers
$(document).on("click", ".CheckBrokers", function () {
    document.getElementById("ModalContent").innerHTML = LoadANimation;
        GetRequestKey();
        window.location.href = "#"
})
       
  
    // Get Data from form controls
    function GrabData() {
        var Brokers = {
            "resquestkey": {
                "key": ReqKey
            },
            "company_name": document.getElementById("company_name").value,
            "first_name": document.getElementById("first_name").value,
            "last_name": document.getElementById("last_name").value,
            "email": document.getElementById("email").value,
            "national_id": document.getElementById("national_id").value,
            "national_id_type": document.getElementById("national_id_type").value,
            "policy_number_1": document.getElementById("policy_number_1").value,
            "policy_number_2": document.getElementById("policy_number_2").value,
            "policy_number_3": document.getElementById("policy_number_3").value
        };

        GetBrokerResponse(Brokers);
    };

    function GetBrokerResponse(SubmitData) {
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwSubmitQuotation",
            data: JSON.stringify(SubmitData),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessBroker,
            error: ErrorBroker
        });

    };

    function SuccessBroker(response) {
        var GlobalNameResponse = "<p class=\"VaribaleLables\">Global Name#: </p><p>" + response.global_name_number + "</p>"
                                 + "<p class=\"VaribaleLables\">Success: </p><p>" + response.success + "</p>";
        document.getElementById("ModalContent").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">" + GlobalNameResponse + "</div>";
    }

    function ErrorBroker() {
        document.getElementById("ModalContent").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">Failed to connect to UnderWriter (:<p>" + response.responseText + "</p></div>";
    }

    //Loading Animation
    var LoadANimation = "<div class=\"cssload-dots\">"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                      + "<div class=\"cssload-dot\"></div>"
                    + "</div>"
                    + "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
                      + "<defs>"
                          + "<filter id=\"goo\">"
                              + "<feGaussianBlur in=\"SourceGraphic\" result=\"blur\" stdDeviation=\"12\" ></feGaussianBlur>"
                              + "<feColorMatrix in=\"blur\" mode=\"matrix\" values=\"1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7\" result=\"goo\" ></feColorMatrix>"
                              + "<!--<feBlend in2=\"goo\" in=\"SourceGraphic\" result=\"mix\" ></feBlend>-->"
                          + "</filter>"
                      + "</defs>"
                    + "</svg>"
                    + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>"
                       + "<div class=\"ClearFix\"></div>";