﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 
// and then run "window.location.reload()" in the JavaScript Console.


 var LoggedInUsername, LoggedInPassword;
(function () {
    "use strict";

    document.addEventListener( 'deviceready', onDeviceReady.bind( this ), false );

    function onDeviceReady() {
        // Handle the Cordova pause and resume events
        document.addEventListener( 'pause', onPause.bind( this ), false );
        document.addEventListener( 'resume', onResume.bind( this ), false );
        
        LoadCheck.addEventListener("click", CheckAuthentication(), false);

        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
    };
   
    $(document).on('click', ".nb", function () {
       window.location.href = "index.html?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword");
    });

    function Navigate() {
        window.location.href = "index.html?LoggedInUsername=" + LoggedInUsername + "&?LoggedInPassword=" + LoggedInPassword;
    }

    var AddedQuery = "LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword");
   
    function CheckAuthentication() {
        LoggedInPassword = getParameterByName("LoggedInPassword");
        LoggedInUsername = getParameterByName("LoggedInUsername");
        document.getElementById("UsernameID").innerHTML = "Welecome\n" +  LoggedInUsername;
        if (LoggedInUsername == "" || LoggedInUsername == "undefined") {
            window.location.href = "Login.html";
        }
    };

    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        //alert(results);

        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

    }

    $(document).on('click', ".ForgetMe", function () {
        document.cookie = "username=username; expires=Thu, 01 Jan 1970 00:00:00 UTC" ;
        document.cookie = "password=password; expires=Thu, 01 Jan 1970 00:00:00 UTC" ;
    })

    $(document).on('click', ".LogOut", function () {
        LoggedInPassword = "";
        LoggedInUsername = "";
        window.location.href = "index.html";
    })

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.
    };

    function NavigateToPage(PageName, MethodName) {
        window.location.href = PageName + "?" + "Method=" + MethodName + "&?Tasks=Colors&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    };




    $(document).on('click', ".mtq", function () {
        window.location.href = "MotorQuote.html?Method=epic_mwGetVehColour&?Tasks=Colors&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on('click', ".Colors", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehColour&?Tasks=Colors&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".Usages", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetUsages&?Tasks=Usages&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on('click', ".MakeAndModel", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehMakeModels&?Tasks=MakeAndModel&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".RoofTypes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehRoofType&?Tasks=RoofTypes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".Countries", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCCountries&?Tasks=Countries&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".GeneralArea", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCGeneralAreas&?Tasks=GeneralArea&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".AuthDrivers", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehAuthDrivers&?Tasks=AuthDrivers&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".TransmissionTypes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehTransmissionType&?Tasks=TransmissionTypes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".ImportTypes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehImportType&?Tasks=ImportTypes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".Islands", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCIslands&?Tasks=Islands&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".Classes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCLocationClasses&?Tasks=Classes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".ParishAndTown", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCParishTowns&?Tasks=ParishAndTown&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".StreetNames", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCStreetNames&?Tasks=StreetNames&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".PropertyRoofTypes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetPropRoofType&?Tasks=PropertyRoofTypes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".StreetTypes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCStreetTypes&?Tasks=StreetTypes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".ZipCodes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetLOCZipCodes&?Tasks=ZipCodes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".Occupation", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetOccupations&?Tasks=Occupation&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".PropertyWallTypes", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetPropWallType&?Tasks=PropertyWallTypes&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });
    $(document).on("click", ".Colors", function () {
        window.location.href = "ResultPage.html?Method=epic_mwGetVehColour&?Tasks=Colors&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".SubmitGlobalNames", function () {
        window.location.href = "GlobalNamesSubmit.html?Method=epic_mwGlobalNameSubmit&?Tasks=SubmitGlobalNames&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".CheckGlobalName", function () {
        window.location.href = "GlobalNameSearch.html?Method=epic_mwGlobalNameGet&?Tasks=CheckGlobalNames&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".CheckBroker", function () {
        window.location.href = "ResultPage.html?Method=epic_mwIsBroker&?Tasks=CheckBroker&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

    $(document).on("click", ".CheckClient", function () {
        window.location.href = "ResultPage.html?Method=epic_mwIsClient&?Tasks=CheckClient&?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword") + "&?RequestBody=";
    });

} )();