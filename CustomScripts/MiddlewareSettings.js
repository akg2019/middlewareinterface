﻿//Supporting Objects

//IPLIST
var IPLIST = {
    IPID: "",
    IP: "",
    IPDescription: "",
    IsBlocked:false
}

//SettingsObject
var SettingsObject = {
    SettingsID: "",
    EnableSSL: false,
    EnableIPWhiteList:false
}

//Settings
var Settings = {
    SettingsID: 0,
    EnableSSL: false,
    EnableIPWhiteList: false,
    AMPlusURL:"",
    AmUsername: "",
    AmPassword: "",
    DocumentUploadURL:"",
    AmPlusTextNotificationURL: "",
    GlobalDefaultDestination: ""
}



//New Post
//$("button").click(function () {
//    $.post("MiddleWareHome/AddIP",
//   JSON.stringify(IPLIST),
//    function (data, status) {
//        alert("Data: " + data + "\nStatus: " + status);
//    });
//});



$(function () {
    try {
         if (document.getElementById("EnableListIP").checked == true) {
                $("#btnCheck").show();
            } else {
                $("#btnCheck").hide();
            }
    } catch (e) {

    }
   

});


$(document).on('click', "#GetSettings", function () {
    try {
         if (document.getElementById("EnableListIP").checked == true) {
                $("#btnCheck").show();
            } else {
                $("#btnCheck").hide();
            }
    } catch (e) {

    }
   

})


$(document).on('change', "#EnableListIP", function () {
    try {
        if (document.getElementById("EnableListIP").checked == true) {
                $("#btnCheck").show();
            } else {
                $("#btnCheck").hide();
            }
    } catch (e) {

    }
    
  
})

//Sets selected IP Item
function SetIDIP(IPID) {
    document.getElementById("SelectedIP").innerHTML = IPID;
    EditIP(IPID);
}

$(document).on('click', "#AddSaveIP", function () {
    if (document.getElementById("AddIPAddress").value.trim() == "") {
        alert("IP address cannot be empty");
    }
    AddIP();
})


//Search for IP
$(document).on('click', "#SearchIP", function () {
    location.href = "/MiddleWareHome/IPWhiteList?IP=" + document.getElementById("IPSearch").value;
})


//Adds an IP
function AddIP() {

    IPLIST.IP = document.getElementById("AddIPAddress").value
    IPLIST.IPDescription = document.getElementById("AddIPDescription").value;
    IPLIST.IsBlocked = document.getElementById("AddBlockIP").checked;
  
    $.ajax({
        type: "POST",
        url: "AddIP",
        data: JSON.stringify(IPLIST),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessAddIP,
        error: ErrorAddIP
    });
    location.href = "/MiddleWareHome/IPWhiteList";
};

function SuccessAddIP(response) {}
function ErrorAddIP() { }



//Get IP for editing
function EditIP(IPADDRESS) {
    $.ajax({
        type: "POST",
        url: "EditIP?IPID=" + IPADDRESS,
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessEditID,
        error: ErrorEditID
    });

};

function SuccessEditID(response) {
    document.getElementById("IPAddress").value = response["IP"];
    document.getElementById("IPDescription").value = response["IPDescription"];
    document.getElementById("BlockIP").checked = response["IsBlocked"];
    
}
function ErrorEditID() { }


//Saves IP
$(document).on('click', "#SaveIP", function () {
    SaveIPAddess();
})

function SaveIPAddess() {
    IPLIST.IP = document.getElementById("IPAddress").value
    IPLIST.IPDescription = document.getElementById("IPDescription").value;
    IPLIST.IsBlocked = document.getElementById("BlockIP").checked;
    IPLIST.IPID = document.getElementById("SelectedIP").innerHTML;

    $.ajax({
        type: "POST",
        url: "SaveIP",
        data: JSON.stringify(IPLIST),
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessSaveIP,
        error: ErrorSaveIP
    });

    location.href = "/MiddleWareHome/IPWhiteList";
};

function SuccessSaveIP(response) {}
function ErrorSaveIP() { }


//Delete IP Settings

function InitiateDelete(IP) {
    var x;
    if (confirm("Are you sure you want to delete this settings?") == true) {
        DeleteIPAddess(IP);
    }
}

function DeleteIPAddess(IP) {
      $.ajax({
        type: "POST",
        url: "RemoveIP?IPID=" + IP,
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessDeleteIP,
        error: ErrorDeleteIP
    });

    location.href = "/MiddleWareHome/IPWhiteList";
};

function SuccessDeleteIP(response) { }
function ErrorDeleteIP() { }

//Get Settings
$(document).on('click', "#GetSettings", function () {
    GetSettings();
})

//Save Settings
$(document).on('click', "#SaveSettings", function () {
    GetSettingsSave();
})

//Gets Settings Data
function GetSettings() {
    $.ajax({
        type: "POST",
        url: "/MiddleWareHome/GetSettings",
        data: "{}",
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessGetSettings,
        error: ErrorGetSettings
    });
  };

function SuccessGetSettings(response) {
    document.getElementById("EnableSSLIP").checked = response[0].EnableSSL;
    document.getElementById("EnableListIP").checked = response[0].EnableIPWhiteList;
    document.getElementById("SelectedSettingsID").innerHTML = response[0].SettingsID;
    $("#AMPLUSURL").val(response[0].AmPlusURL);
    $("#AmUsername").val(response[0].AmUsername);
    $("#AmPassword").val(response[0].AmPassword);
    $("#DocumentUploadURL").val(response[0].DocumentUploadURL);
    $("#AmPlusTextNotificationURL").val(response[0].AmPlusTextNotificationURL);
    $("#globalreg").val(response[0].GlobalDefaultDestination);

}
function ErrorGetSettings() { }


//Save Settings
function GetSettingsSave() {

    Settings.SettingsID = document.getElementById("SelectedSettingsID").innerHTML;
    Settings.EnableIPWhiteList = document.getElementById("EnableListIP").checked;
    Settings.EnableSSL = document.getElementById("EnableSSLIP").checked;
    Settings.AMPlusURL = $("#AMPLUSURL").val();
    Settings.AmUsername = $("#AmUsername").val();
    Settings.AmPassword = $("#AmPassword").val();
    Settings.DocumentUploadURL = $("#DocumentUploadURL").val();
    Settings.AmPlusTextNotificationURL = $("#AmPlusTextNotificationURL").val();
    Settings.GlobalDefaultDestination = $("#globalreg").val();


    $.ajax({
        type: "POST",
        url: "/MiddleWareHome/SaveSettings",
        data:JSON.stringify(Settings) ,
        contentType: "application/json; charset-utf-8",
        dataType: "json",
        success: SuccessGetSettingsSave,
        error: ErrorGetSettingsSave
    });
};

function SuccessGetSettingsSave(response) {}
function ErrorGetSettingsSave() { }


//Sends an Email