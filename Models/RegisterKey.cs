﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ucon.Models
{
  public  class RegisterKey
    {
        public string RKey { set; get; }
        public JObject RKeyResult { set; get; }
    }
}
