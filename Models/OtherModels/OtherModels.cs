﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Ucon.Models
{
    public class OtherModels
    {
        public class drivers
        {
            public string is_excluded { set; get; }
            public string is_included_by_exception { set; get; }
            public string national_id { set; get; }
            public string is_main_driver { set; get; }
            public string national_id_type { set; get; }
            public string relation_to_insured { set; get; }

        }
    }
}