﻿using System.Collections.Generic;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;


namespace Ucon.Models
{
    public class GlobalData
    {
        public static string GetConnectionString()
        {
            return System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        }

        //Returns Motor AUthorized Drivers List
        
        public static JObject GetNamedDrivers(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehAuthDrivers", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }


        //Get version Info
        public static JArray GetVersions()
        {
            return JArray.Parse(KeyManagement.PerformRequest("GetVersion", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        //Returns Converted Quotation

        public static JObject QuotationConvert(string QuotationNumber)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwQuotationConvert", KeyManagement.RequestType.SignleParameter, QuotationNumber, new object()));
        }


        //Returns Motor Vehicle Color

        public static JObject GetVehicleColor()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehColour", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        //Returns User Sources from 4D

        public static JObject epic_mwGetRequestSources()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetRequestSources", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        public static JObject epic_mwGetDepartments()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetDepartments", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        //Returns Motor Vehicle Color

        public static JObject GetUsages(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetUsages", KeyManagement.RequestType.SignleParameter, PolicyPrefix.ToUpper(), new object()));
        }

        //Returns Motor Vehicle Color
        
        public static JObject GetOccupations(string Group)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetOccupations", KeyManagement.RequestType.SignleParameter, Group, new object()));
        }


        //Returns Motor Vehicle Import Types
        
        public static JObject VehImportType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehImportType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Returns Motor Vehicle Roof Types
        
        public static JObject VehRoofType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehRoofType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Returns Motor Vehicle Roof Types
        
        public static JObject PropRoofType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPropRoofType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Returns Motor Vehicle Roof Types
        
        public static JObject Parishes(string Country)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCParishTowns", KeyManagement.RequestType.SignleParameter, Country, new object()));
        }

        public static List<string> Parish()
        {

            int ParishCount = 0;
            List<string> _Parish = new List<string>();
            JObject Parish = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCParishTowns", KeyManagement.RequestType.SignleParameter, "Jamaica", new object()));
            foreach (JToken P in Parish["data"].Children())
            {
                _Parish.Add(Parish["data"][ParishCount]["parish"].ToString()); ParishCount += 1;
            }

            return _Parish;
        }

        //Returns Motor Vehicle Transmission Types
        
        public static JObject VehTransmissionType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehTransmissionType", KeyManagement.RequestType.NoParameters, "", new object()));
        }
        
        
        public static JObject GetDropDownData(string Make)
        {
            SubmitQuotation.DropDownListData.Clear();

            //Gets Makes and Models
            JObject MM = JObject.Parse(JsonConvert.SerializeObject(GetModels("Motor",false)));
            JObject DropData = new JObject();
            int MakeCount, ModelCount;
            MakeCount = 0; ModelCount = 0;
            List<string> Makes = new List<string>();
            JArray Model = new JArray();
            JArray ModelType = new JArray();
            JArray BodyShape = new JArray(); 
            JArray BodyTypes = new JArray();
            JArray Extensions = new JArray();
            try
            {
                foreach (JToken T in MM["data"].Children())
                {
                    if (MM["data"][MakeCount]["make"].ToString() == Make)
                    {
                        //Gets Models
                        foreach (JToken Models in MM["data"][MakeCount]["models"].Children())
                        {
                            Model.Add(MM["data"][MakeCount]["models"][ModelCount]["model"].ToString());
                            ModelType.Add(MM["data"][MakeCount]["models"][ModelCount]["model_type"].ToString());
                            BodyShape.Add(MM["data"][MakeCount]["models"][ModelCount]["body_shape"].ToString());
                            BodyTypes.Add(MM["data"][MakeCount]["models"][ModelCount]["body_type"].ToString());
                            Extensions.Add(MM["data"][MakeCount]["models"][ModelCount]["extension"].ToString());

                            ModelCount += 1;
                        }
                    }
                    MakeCount += 1;
                }


                DropData.Add("Models", Model);
                DropData.Add("ModelTypes", ModelType);
                DropData.Add("BodyShapes", BodyShape);
                DropData.Add("BodyTypes", BodyTypes);
                DropData.Add("Extensions", Extensions);

            }
            catch (System.Exception)
            {

                throw;
            }


            return DropData;
        }


        public static JObject GetModels(string VehicleType, bool ConcatList)
        {
            if(ConcatList == true)
            {
                Parameters Concat = new Parameters();
                Concat.concatenatedList = true;
                Concat.vehicleType = "Motor";
                return JObject.Parse(KeyManagement.PerformRequest("GetContactenatedMakesAndModels", KeyManagement.RequestType.Parameter, "", Concat));
            }
                if(ConcatList == false)
            {
                return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetVehMakeModels", KeyManagement.RequestType.SignleParameter, VehicleType, new object()));
     
            }
            return new JObject();
        }


        //CocatMakes and Models
        public class Parameters
        {
         public string vehicleType { set; get; }
         public bool concatenatedList { set; get; }

        }

        //Get a list of Countries
       
        public static JObject GetLOCCountries()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCCountries", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get General Areas
       
        public static JObject GetLOCGeneralAreas(string Country)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCGeneralAreas", KeyManagement.RequestType.SignleParameter, Country, new object()));
        }

               
        //Get a list of Islands
       
        public static JObject GetLOCIslands(string Country)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCIslands", KeyManagement.RequestType.SignleParameter, Country, new object()));
        }

        //Get Vehicle Classes
       
        public static JObject GetLOCLocationClasses()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCLocationClasses", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Parish and Towns
       
        public static JObject GetLOCParishTowns(string Country)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCParishTowns", KeyManagement.RequestType.SignleParameter, Country, new object()));
        }

        //Get Street Names
        
        public static JObject GetLOCStreetNames(string Country)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCStreetNames", KeyManagement.RequestType.SignleParameter, Country, new object()));
        }

       
        //Get Street Types
       
        public static JObject GetLOCStreetTypes()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCStreetTypes", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Zip Codes
       
        public static JObject GetLOCZipCodes(string Country)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCZipCodes", KeyManagement.RequestType.SignleParameter, Country, new object()));
        }

       
        //Get Property Wall Types
       
        public static JObject GetPropWallType()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPropWallType", KeyManagement.RequestType.NoParameters, "", new object()));
        }

       
        //Check Global Names
       
        public static JObject GlobalNameSearch(string GlobalNameID)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGlobalNameGet", KeyManagement.RequestType.SignleParameter, GlobalNameID, new object()));
        }

       
        //Checks Broker's Status
        public static JObject BrokerChecking(object BrokerInfo)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwIsBroker", KeyManagement.RequestType.Quotation, "", BrokerInfo));
        }


        //Submits a Global Name
        public static JObject SubmitGlobalName(object GlobalNames)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGlobalNameSubmit", KeyManagement.RequestType.Quotation, "", GlobalNames));
        }

       
        //Create a Policy using the Policy Data
        public static JObject epic_mwPolicyCreate(object PolicyData)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwPolicyCreate", KeyManagement.RequestType.Quotation, "", PolicyData));
        }


        //Checks Client's Status
        public static JObject ClientChecking(object ClientInfo)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwIsClient", KeyManagement.RequestType.Quotation, "", ClientInfo));
        }

        //Checks Client's Status
        public static JObject ConcatMakesAndModels(string PolicyPrefix,bool Concat)
        {
        
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwIsClient", KeyManagement.RequestType.SignleParameter, "", new object()));
        }

        //Gets Global Name Policies
        public static JObject GlobalNamePolicies(string PolicyID)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGlobalNamePolicies", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }


        //Gets a list of Extensions
        public static JObject PolicyExtensions(string PolicyID)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyExtensions", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }


        //Gets a list of Policy Profile Extensions
        public static JObject epic_mwGetPolicyProfExt(string PolicyPrefix)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfExt", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }


        //Gets a list of Profile Limits of Liability
        public static JObject epic_mwGetPolicyProfLOL(string PolicyPrefix)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfLOL", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }




        //Gets a list of limits
        public static JObject PolicyLimits(string PolicyID)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyLimits", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }


        //Gets a list of Transactions
        public static JObject PolicyTrans(string PolicyID)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyTrans", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }


        //Gets a list of Policy Risks
        public static JObject PolicyRisks(string PolicyID)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyRisks", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }


        //Gets a list of Policy Claims
        public static JObject PolicyClaims(string PolicyID)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyClaims", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }


        //Gets Policy Prefix List
        public static JObject PolicyPrefixLists = new JObject();
        public static JObject GetPrefixMapping()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetAllPolicyPrefixes", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        public static JObject GetUsageMapping()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetAllUsagesCodes", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        public static JObject GetDriverWordingsMapping()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetAllVehAuthDrivers", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        public static JObject epic_mwGetTransWording()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetTransWording", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        public static JObject epic_mwGetAllRatingCodes()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetAllRatingCodes", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Gets a list of Actions
        public static JObject epic_mwPolicyModActions()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwPolicyModActions", KeyManagement.RequestType.NoParameters, "", new object()));
        }


        //Checks Broker's Status
        public static JObject PolicyRenew(object PolicyInfo)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwPolicyRenew", KeyManagement.RequestType.ObjectBody, "", PolicyInfo));
        }

        //Policy Profile Ext
        public static JObject PolicyProfExt(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfExt", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }

        //Policy Profile LOL
        public static JObject PolicyProfLOL(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfLOL", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }

        //Policy Profile 
        public static JObject PolicyProfile(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfiles", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }

        //Policy Profile Rating Codes
        public static JObject PolicyProfileRatingCodes(string PolicyPrefix)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfRatingCodes", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }

        //Policy Profile Range Values
        public static JObject PolicyProfileRangeVals(string PolicyPrefix) {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfRangeVals", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }

        //Policy Profile Ratings
        public static JObject PolicyProfileRatings(string PolicyPrefix)
        {

            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicyProfRatings", KeyManagement.RequestType.SignleParameter, PolicyPrefix, new object()));
        }

        //Submits a file to UnderWriter
        public static JObject SubmitFile(object FileData)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwSubmitFile", KeyManagement.RequestType.ObjectBody, "", FileData));
        }

        //Gets Policies
        public static JObject GetPolicy(string PolicyID)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetPolicy", KeyManagement.RequestType.SignleParameter, PolicyID, new object()));
        }

        //Submits a file to UnderWriter
        public static JObject RegisterMiddleWare(object KeyData)
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwRegisterLic", KeyManagement.RequestType.ObjectBody, "", KeyData));
        }

        //Get Broker Codes
        public static JObject GetBrokerCodes()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetBrokerCodes", KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get available sources

        public static JObject epic_mwGetSources()
        {
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwGetSources", KeyManagement.RequestType.NoParameters, "", new object()));
        }
    }
}