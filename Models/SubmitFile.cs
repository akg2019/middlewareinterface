﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
  public  class FileSubmit
    {
        public string table  {get; set;}
        public string record_id  {get; set;}
        public string file_id  {get; set;}
        public string file_name  {get; set;}
        public string file_type  {get; set;}
        public string file_data  {get; set;}
        public string description {get; set;}
        public JObject FileData { get; set; }
        public HttpPostedFileBase Files { get; set; }
    }
}
