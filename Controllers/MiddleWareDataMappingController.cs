﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class MiddleWareDataMappingController : Controller
    {
        Models.DBModel.MiddleAccsEntities DB = new Models.DBModel.MiddleAccsEntities();
        // GET: MiddleWareDataMapping
        public ActionResult Index(int CompanyID)
        {
          
            var CompData = from Comps in DB.Companies
                           where Comps.CompanyID == CompanyID
                           select Comps;
                        
            return View(CompData.SingleOrDefault());
        }

     
        #region LocationMapping
        public ActionResult LocationMapping(int CompanyID)
        {
          
            var AddressToUpdate = from p in DB.Addresses
                                  where p.CompanyID == CompanyID
                                  select p;


            Models.DBModel.Address NewAddressMapping = AddressToUpdate.SingleOrDefault();
            return View(NewAddressMapping);
        }


        [HttpPost]
        public ActionResult LocationMapping(Models.DBModel.Address AddressMappings)
        {
           

            var AddressToUpdate = from p in DB.Addresses
                                  where p.CompanyID == AddressMappings.CompanyID
                                  select p;


            Models.DBModel.Address NewAddressMapping = AddressToUpdate.Single();

            NewAddressMapping.altitude = AddressMappings.altitude;
            NewAddressMapping.block = AddressMappings.block;
            NewAddressMapping.building_number = AddressMappings.building_number;
            NewAddressMapping.categorization = AddressMappings.categorization;
            NewAddressMapping.complex_name = AddressMappings.complex_name;
            NewAddressMapping.country = AddressMappings.country;
            NewAddressMapping.general_area = AddressMappings.general_area;
            NewAddressMapping.international_area = AddressMappings.international_area;
            NewAddressMapping.island = AddressMappings.island;
            NewAddressMapping.is_international_address = AddressMappings.is_international_address;
            NewAddressMapping.latitude = AddressMappings.latitude;
            NewAddressMapping.location_class = AddressMappings.location_class;
            NewAddressMapping.longitude = AddressMappings.longitude;
            NewAddressMapping.parish = AddressMappings.parish;
            NewAddressMapping.postal_zip_code = AddressMappings.postal_zip_code;
            NewAddressMapping.street_name = AddressMappings.street_name;
            NewAddressMapping.street_number = AddressMappings.street_number;
            NewAddressMapping.street_type = AddressMappings.street_type;
            NewAddressMapping.town = AddressMappings.town;
            NewAddressMapping.unit_number = AddressMappings.unit_number;
            NewAddressMapping.UserName = User.Identity.Name;
            NewAddressMapping.Modified = DateTime.Now;
            DB.SaveChanges();
            
            return RedirectToAction("LocationMapping", new {CompanyID = AddressMappings.CompanyID });
        }
        #endregion


        #region Brokers

        public ActionResult Brokers(int CompanyID)
        {
            Models.DBModel.MiddleAccsEntities Broker = new Models.DBModel.MiddleAccsEntities();
            var broker = from B in Broker.Brokers
                         where B.CompanyID == CompanyID
                         select B;

            return View(broker.SingleOrDefault());

        }


        [HttpPost]
        public ActionResult Brokers(Models.DBModel.Broker BrokerMapping)
        {
            
            var BrokerToUpdate = from B in DB.Brokers
                         where B.CompanyID == BrokerMapping.CompanyID
                         select B;

            Models.DBModel.Broker NewBroker = BrokerToUpdate.Single();

            NewBroker.company_name = BrokerMapping.company_name;
            NewBroker.email = BrokerMapping.email;
            NewBroker.first_name = BrokerMapping.first_name;
            NewBroker.last_name = BrokerMapping.last_name;
            NewBroker.national_id = BrokerMapping.national_id;
            NewBroker.national_id_type = BrokerMapping.national_id_type;
            NewBroker.policy_number_1 = BrokerMapping.policy_number_1;
            NewBroker.policy_number_2 = BrokerMapping.policy_number_2;
            NewBroker.policy_number_3 = BrokerMapping.policy_number_3;
            NewBroker.UserName = User.Identity.Name;
            NewBroker.Modified = DateTime.Now;
            DB.SaveChanges();
            return RedirectToAction("Brokers", new { CompanyID = BrokerMapping.CompanyID });

        }
        #endregion


        #region Clients

        public ActionResult Clients(int CompanyID)
        {
            var ClientTopUpdate = from NewClient in DB.Clients
                                  where NewClient.CompanyID == CompanyID
                                  select NewClient;

            return View(ClientTopUpdate.SingleOrDefault());
             
        }


        [HttpPost]
        public ActionResult Clients(Models.DBModel.Client ClientMapping)
        {
            var ClientTopUpdate = from NewClient in DB.Clients
                                  where NewClient.CompanyID == ClientMapping.CompanyID
                                  select NewClient;


            Models.DBModel.Client NewClentMapping = ClientTopUpdate.Single();

            NewClentMapping.company_name = ClientMapping.company_name;
            NewClentMapping.email = ClientMapping.email;
            NewClentMapping.first_name = ClientMapping.first_name;
            NewClentMapping.last_name = ClientMapping.last_name;
            NewClentMapping.national_id = ClientMapping.national_id;
            NewClentMapping.national_id_type = ClientMapping.national_id_type;
            NewClentMapping.policy_number = ClientMapping.policy_number;
            NewClentMapping.UserName = User.Identity.Name;
            NewClentMapping.Modified = DateTime.Now;
            DB.SaveChanges();
            return RedirectToAction("Clients", new { CompanyID = ClientMapping.CompanyID });

        }


        #endregion


        #region Drivers

        public ActionResult Drivers(int CompanyID)
        {
            var CurrentDrivers = from Driver in DB.Drivers
                                 where Driver.CompanyID == CompanyID
                                 select Driver;

            return View(CurrentDrivers.SingleOrDefault());
        }

        [HttpPost]
        public ActionResult Drivers(Models.DBModel.Driver IncommingDriver)
        {
            var CurrentDrivers = from Driver in DB.Drivers
                                 where Driver.CompanyID == IncommingDriver.CompanyID
                                 select Driver;

            Models.DBModel.Driver NewDriver = CurrentDrivers.Single();

            NewDriver.drivers_licence_number = IncommingDriver.drivers_licence_number;
            NewDriver.driver_dob = IncommingDriver.driver_dob;
            NewDriver.driver_licence_first_issued = IncommingDriver.driver_licence_first_issued;
            NewDriver.first_name = IncommingDriver.first_name;
            NewDriver.is_excluded = IncommingDriver.is_excluded;
            NewDriver.is_included_by_exception = IncommingDriver.is_included_by_exception;
            NewDriver.is_main_driver = IncommingDriver.is_main_driver;
            NewDriver.last_name = IncommingDriver.last_name;
            NewDriver.middle_name = IncommingDriver.middle_name;
            NewDriver.national_id = IncommingDriver.national_id;
            NewDriver.national_id_type = IncommingDriver.national_id_type;
            NewDriver.relation_to_insured = IncommingDriver.relation_to_insured;
            NewDriver.UserName = User.Identity.Name;
            NewDriver.Modified = DateTime.Now;
            DB.SaveChanges();
            return RedirectToAction("Drivers", new { CompanyID = IncommingDriver.CompanyID});
        }
        #endregion

        #region GlobalName

        public ActionResult GlobalName(int CompanyID)
        {
            var CurrentGlobalName = from CurrentNames in DB.Global_Names
                                    where CurrentNames.CompanyID == CompanyID
                                    select CurrentNames;

            return View(CurrentGlobalName.SingleOrDefault());
        }


        [HttpPost]
        public ActionResult GlobalName(Models.DBModel.Global_Names IncommingGlobalNames)
        {
            var CurrentGlobalName = from CurrentNames in DB.Global_Names
                                    where CurrentNames.CompanyID == IncommingGlobalNames.CompanyID
                                    select CurrentNames;


            Models.DBModel.Global_Names GlobalNamesToUpdate = CurrentGlobalName.Single();
            GlobalNamesToUpdate.company_name = IncommingGlobalNames.company_name;
            GlobalNamesToUpdate.dob = IncommingGlobalNames.dob;
            GlobalNamesToUpdate.drivers_licence_country = IncommingGlobalNames.drivers_licence_country;
            GlobalNamesToUpdate.drivers_licence_date_issued= IncommingGlobalNames.drivers_licence_date_issued;
            GlobalNamesToUpdate.drivers_licence_first_issued = IncommingGlobalNames.drivers_licence_first_issued;
            GlobalNamesToUpdate.drivers_licence_number = IncommingGlobalNames.drivers_licence_number;
            GlobalNamesToUpdate.email_address = IncommingGlobalNames.email_address;
            GlobalNamesToUpdate.employment_type = IncommingGlobalNames.employment_type;
            GlobalNamesToUpdate.first_name = IncommingGlobalNames.first_name;
            GlobalNamesToUpdate.gender = IncommingGlobalNames.gender;
            GlobalNamesToUpdate.Global_NamesID = IncommingGlobalNames.Global_NamesID;
            GlobalNamesToUpdate.global_name_number = IncommingGlobalNames.global_name_number;
            GlobalNamesToUpdate.is_a_company = IncommingGlobalNames.is_a_company;
            GlobalNamesToUpdate.is_a_service_provider = IncommingGlobalNames.is_a_service_provider;
            GlobalNamesToUpdate.last_name = IncommingGlobalNames.last_name;
            GlobalNamesToUpdate.maiden_name = IncommingGlobalNames.maiden_name;
            GlobalNamesToUpdate.mailing_name = IncommingGlobalNames.mailing_name;
            GlobalNamesToUpdate.marital_status = IncommingGlobalNames.marital_status;
            GlobalNamesToUpdate.middle_name = IncommingGlobalNames.middle_name;
            GlobalNamesToUpdate.nationality = IncommingGlobalNames.nationality;
            GlobalNamesToUpdate.national_id = IncommingGlobalNames.national_id;
            GlobalNamesToUpdate.national_id_type = IncommingGlobalNames.national_id_type;
            GlobalNamesToUpdate.notes = IncommingGlobalNames.notes;
            GlobalNamesToUpdate.occupation = IncommingGlobalNames.occupation;
            GlobalNamesToUpdate.occupation_code = IncommingGlobalNames.occupation_code;
            GlobalNamesToUpdate.occupation_description = IncommingGlobalNames.occupation_description;
            GlobalNamesToUpdate.phone_number_fax = IncommingGlobalNames.phone_number_fax;
            GlobalNamesToUpdate.phone_number_general = IncommingGlobalNames.phone_number_general;
            GlobalNamesToUpdate.phone_number_mobile = IncommingGlobalNames.phone_number_mobile;
            GlobalNamesToUpdate.place_of_birth = IncommingGlobalNames.place_of_birth;
            GlobalNamesToUpdate.salutation_name = IncommingGlobalNames.salutation_name;
            GlobalNamesToUpdate.service_type = IncommingGlobalNames.service_type;
            GlobalNamesToUpdate.tax_id_number = IncommingGlobalNames.tax_id_number;
            GlobalNamesToUpdate.title = IncommingGlobalNames.title;
            GlobalNamesToUpdate.UserName = User.Identity.Name;
            GlobalNamesToUpdate.Modified = DateTime.Now;
            DB.SaveChanges();
            return RedirectToAction("GlobalName", new { CompanyID = IncommingGlobalNames.CompanyID});
        }
        #endregion



    }
}
