﻿using System.Collections.Generic;

namespace Ucon.Models
{
    public class QuotationResponseObject
    {
        public bool success { set; get; }
        public int quotation_number { set; get; }
        public double net_premium { set; get; }
        public double stamp_duty { set; get; }
        public double tax { set; get; }
        public double total_premium { set; get; }
        public List<limits> Limits = new List<limits>();
        public List<extensions> extensions = new List<Models.extensions>();
        public string CustomerName { set; get; }
        public static object ErrorCode { set; get; }
        public string QuoteError { set; get; }
        
    }

    public class limits
    {
        public string code { set; get; }
        public string heading { set; get; }
        public string limit { set; get; }
        public string description { set; get; }

    }

    public class extensions
    {
        public string type { set; get; }
        public string code { set; get; }
        public string heading { set; get; }
        public string description { set; get; }
    }

    public class ErrorQuoteResponse
    {
        public bool success { set; get; }
        public string error_message { set; get; }
    }
}