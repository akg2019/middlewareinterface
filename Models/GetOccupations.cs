﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace Ucon.Models
{
    public class Company
    {
        public string Industry { set; get; }
        public string Occupation { set; get; }
        public bool Do_Not_Insure { set; get;}
        public Company(string industry, string occupation, bool do_not_insure)
        {
            Industry = industry;
            Occupation = occupation;
            Do_Not_Insure = do_not_insure;
        }
    }
    public class GetOccupations
    {
        public string Groups { set; get; }
        public JObject OccupationList = new JObject();
        public List<Company> Lists = new List<Company>();
        
    }
}