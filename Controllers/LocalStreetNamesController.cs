﻿using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Ucon.Models;

namespace Ucon.Controllers
{
    public class LocalStreetNamesController : Controller
    {
        // GET: LocalStreetNames
        public ActionResult Index()
        {
            GetLocalStreetNames StreetNames = new GetLocalStreetNames();
            

            JObject O = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCStreetNames",
                KeyManagement.RequestType.SignleParameter, "Jamaica",
                null));


            foreach (JToken T in O["data"])
            {
                if(T.ToString() != "")
                {
                    StreetNames.Lists.Add(T.ToString());
                }
               
            }
            return View(StreetNames);
        }

        [HttpPost]
        public ActionResult Index(GetLocalStreetNames response)
        {
           
            JObject O = JObject.Parse(KeyManagement.PerformRequest("epic_mwGetLOCStreetNames",
                KeyManagement.RequestType.SignleParameter, response.SearchCriteria,
                null));


            foreach (JToken T in O["data"])
            {
                if (T.ToString() != "")
                {
                    response.Lists.Add(T.ToString());
                }

            }
            return View(response);
        }

        // GET: LocalStreetNames/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: LocalStreetNames/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: LocalStreetNames/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                // TODO: Add insert logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LocalStreetNames/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: LocalStreetNames/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: LocalStreetNames/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: LocalStreetNames/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
