﻿using Newtonsoft.Json.Linq;

namespace Ucon.Models
{
    public class BrokerCheck
    {
        public JObject BrokerResponse = new JObject();
        public object resquestkey   { set; get; }
        public string company_name  {set; get;}
        public string first_name { set; get;}
        public string last_name { set; get;}
        public string email { set; get;}
        public string national_id { set; get;}
        public string national_id_type { set; get;}
        public string policy_number_1 { set; get;}
        public string policy_number_2 { set; get;}
        public string policy_number_3 { set; get; }
        public bool Success { set; get; }
        public int GlobalNameNumber { set; get; }
        
    }


}