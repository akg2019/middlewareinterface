﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Newtonsoft.Json.Linq;
using System.IO;
using Ucon.Models;

namespace Ucon.Controllers
{

    public class AllMethodsController : Controller
    {
        //Gets input stream
        private JObject GetInputStream()
        {
            Stream ReqStream = Request.InputStream;  

            ReqStream.Seek(0, SeekOrigin.Begin);
            string IncommingData = new StreamReader(ReqStream).ReadToEnd();
            return JObject.Parse(IncommingData);
        }
        
        // GET: AllMethods
        public ActionResult Index()
        {
            return View();
        }

        //Get Vehicle Colors
        [HttpPost]
        public object GetVehColour()
        {
          return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters,"",new object()));
        }

        //Get Vehicle Colors
        [HttpPost]
        public object GetUsages()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Vehicle Makes and Models
        [HttpPost]
        public object GetVehMakeModels()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "Motor", new object()));
        }

        //Get Vehicle Roof Types
        [HttpPost]
        public object GetVehRoofType()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get a list of Countries
        [HttpPost]
        public object GetLOCCountries()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get General Areas
        [HttpPost]
        public object GetLOCGeneralAreas()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "Jamaica", new object()));
        }

        //Get Vehicle Authorized Drivers
        [HttpPost]
        public object GetVehAuthDrivers()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "MPCC", new object()));
        }

        //Get Vehicle Transmission Types
        [HttpPost]
        public object GetVehTransmissionType()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Vehicle Import Types
        [HttpPost]
        public object GetVehImportType()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get a list of Islands
        [HttpPost]
        public object GetLOCIslands()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "Jamaica", new object()));
        }

        //Get Vehicle Classes
        [HttpPost]
        public object GetLOCLocationClasses()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Parish and Towns
        [HttpPost]
        public object GetLOCParishTowns()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "Jamaica", new object()));
        }

        //Get Street Names
        [HttpPost]
        public object GetLOCStreetNames()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "Jamaica", new object()));
        }

        //Get Property Roof Types
        [HttpPost]
        public object GetPropRoofType()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Street Types
        [HttpPost]
        public object GetLOCStreetTypes()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Zip Codes
        [HttpPost]
        public object GetLOCZipCodes()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Get Occupations
        [HttpPost]
        public object GetOccupations()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.SignleParameter, "All", new object()));
        }

        //Get Property Wall Types
        [HttpPost]
        public object GetPropWallType()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Submit Global Names
        [HttpPost]
        public object GlobalNameSubmit()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }

        //Check Global Names
        [HttpPost]
        public object GlobalNameSearch()
        {
           return JObject.Parse(KeyManagement.PerformRequest("epic_mwGlobalNameGet", KeyManagement.RequestType.SignleParameter,GetInputStream()["GlobalNameNumber"].ToString(), new object()));
        }
        
        //Get Check Brokers
        [HttpPost]
        public object IsBroker()
        {
            resquestkey Key = new resquestkey();
            BrokerCheck Broker = new BrokerCheck();
            Key.key = KeyManagement.ApiKey();
            Broker.company_name = GetInputStream()["company_name"].ToString();
            Broker.email = GetInputStream()["email"].ToString();
            Broker.first_name = GetInputStream()["first_name"].ToString();
            Broker.last_name = GetInputStream()["last_name"].ToString();
            Broker.national_id = GetInputStream()["national_id"].ToString();
            Broker.national_id_type = GetInputStream()["national_id_type"].ToString();
            Broker.policy_number_1 = GetInputStream()["policy_number_1"].ToString();
            Broker.policy_number_2 = GetInputStream()["policy_number_2"].ToString();
            Broker.policy_number_3 = GetInputStream()["policy_number_3"].ToString();
            Broker.resquestkey = Key;
            
            return JObject.Parse(KeyManagement.PerformRequest("epic_mwIsBroker", KeyManagement.RequestType.Quotation, "",
                   Broker));
        }
        
        //Check Brokers Status
        [HttpPost]
        public ActionResult BrokerStatus( BrokerCheck Brokers)
        {
            resquestkey Key = new resquestkey();
            BrokerCheck Broker = new BrokerCheck();
            Key.key = KeyManagement.ApiKey();
            Broker.company_name = Brokers.company_name;
            Broker.email = Brokers.email;
            Broker.first_name = Brokers.first_name;
            Broker.last_name = Brokers.last_name;
            Broker.national_id = Brokers.national_id;
            Broker.national_id_type = Brokers.national_id_type;
            Broker.policy_number_1 = Brokers.policy_number_1;
            Broker.policy_number_2 = Brokers.policy_number_2;
            Broker.policy_number_3 = Brokers.policy_number_3;
            Broker.resquestkey = Key;
            Broker.BrokerResponse = JObject.Parse(KeyManagement.PerformRequest("epic_mwIsBroker", KeyManagement.RequestType.Quotation, "",
                   Broker));
            return View(Broker);
        }

        //Check Broker Status
        public ActionResult BrokerStatus()
        {
           BrokerCheck Broker = new BrokerCheck();
           return View(Broker);
        }
                
        //Check Clients Status
        [HttpPost]
        public ActionResult ClientStatus(ClientCheck Clients)
        {
            resquestkey Key = new resquestkey();
            ClientCheck Client = new ClientCheck();
            Key.key = KeyManagement.ApiKey();
            Client.company_name = Clients.company_name;
            Client.email = Clients.email;
            Client.first_name = Clients.first_name;
            Client.last_name = Clients.last_name;
            Client.national_id = Clients.national_id;
            Client.national_id_type = Clients.national_id_type;
            Client.policy_number = Clients.policy_number;
            Client.resquestkey = Key;
            Client.ClientResponse = JObject.Parse(KeyManagement.PerformRequest("epic_mwIsClient", KeyManagement.RequestType.Quotation, "",
                  Client));
            return View(Client);
        }

        public ActionResult ClientStatus()
        {
            ClientCheck Client = new ClientCheck();
            return View(Client);
        }
        
        //Get Check Clients
        [HttpPost]
        public object IsClient()
        {
            return JObject.Parse(KeyManagement.PerformRequest(GetInputStream()["MethodName"].ToString(), KeyManagement.RequestType.NoParameters, "", new object()));
        }
     
               
    }
      
}
