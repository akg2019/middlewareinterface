﻿// For an introduction to the Blank template, see the following documentation:
// http://go.microsoft.com/fwlink/?LinkID=397704
// To debug code on page load in Ripple or on Android devices/emulators: launch your app, set breakpoints, 

// and then run "window.location.reload()" in the JavaScript Console.
var Task;
var Makes = "[";
var LoggedInUsername, LoggedInPassword;
var ReqKey;
(function () {
    "use strict";

    document.addEventListener('deviceready', onDeviceReady.bind(this), false);

    function onDeviceReady() {

        UsernameID.addEventListener('click', DisplayUser(), false);
        // Handle the Cordova pause and resume events
        document.addEventListener('pause', onPause.bind(this), false);
        document.addEventListener('resume', onResume.bind(this), false);
        //document.addEventListener('deviceready', epic_mwGetVehColour("Motor"), false);
        // TODO: Cordova has been loaded. Perform any initialization that requires Cordova here.
               
        $(document).on("click", ".SubmitQuote", function () {
            GetRequestKey();
           window.location.href = "#"
        })

        $(document).on('click', ".ForgetMe", function () {
            document.cookie = "username=username; expires=Thu, 01 Jan 1970 00:00:00 UTC";
            document.cookie = "password=password; expires=Thu, 01 Jan 1970 00:00:00 UTC";
        });


        $(document).on('click', ".LogOut", function () {
            window.location.href = "index.html?LoggedInUsername=" + "" + "&?LoggedInPassword=" + "";
        })

        //Adds ColorList Items
        $(document).on("click", ".ColorLoad", function () {
            Task = "Color";
            epic_mwGetVehColour();
            RequestUsages();
            RequestModels();
            RequestImportTypes();
            RequestTransmissionTypes();
            RequestRoofTypes();
        })

        $(document).on('click', ".hm", function () {
            //window.location.href = "index.html?LoggedInUsername=" + getParameterByName("LoggedInUsername") + "&?LoggedInPassword=" + getParameterByName("LoggedInPassword");
        });

        

        function DisplayUser() {
            document.getElementById("UsernameID").innerHTML = "Welecome\n" + getParameterByName("LoggedInUsername");
        }

        //Gets Motor Vehicle Colours
        $(document).on("click", ".MVCs", function () {
            epic_mwGetVehColour();
        })
       
        $(document).on('click', ".ClickButton1", function () {
            ClickButton(1);
        });

        $(document).on('click', '.ClickButton2', function () {
            ClickButton(2);
        });

        $(document).on('click', '.ClickButton3', function () {
            ClickButton(3);
        });

        $(document).on('click', '.ClickButton4', function () {
            ClickButton(4);
        });

        $(document).on('click', '.ClickButton5', function () {
            ClickButton(5);
        });

        $(document).on('click', '.ClickButton0', function () {
            ClickButton(0);
        });
    };

    function onPause() {
        // TODO: This application has been suspended. Save application state here.
    };

    function onResume() {
        // TODO: This application has been reactivated. Restore application state here.

    };
      
    //Requesting a Motor Vehicle Quotation
    function MotorQuotes() {

    }

    //Requesting a Property Quotation
    function PropertiesQuotes() {

    }

    //Quotation number that is passed to return an existing quotation
    function QuoteByNumber() {

    }

    //This method generates a quotation in Underwriter using the data passed through the JSON string and returns a calculated premium.
    function epic_mwSubmitQuotation() {

    }

    //This method returns a JSON string containing an array of usages for the policy prefix that is passed.
    function epic_mwGetUsages() {

    }

    //This method returns a JSON string containing an array of all makes and models for the vehicle Type passed.
    function epic_mwGetVehMakeModels() {

    }

    //This method returns an array of all vehicle roof types.
    function epic_mwGetVehRoofType() {

    }

    //This method returns a JSON string containing an array of all authorized driver wordings for the policy_prefix that is passed.
    function epic_mwGetVehAuthDrivers() {

    }

    //This method returns a JSON string containing an array of all transmission types in the system.
    function epic_mwGetVehTransmissionType() {

    }

    //This method returns a JSON string containing an array of all the import types in the system.
    function epic_mwGetVehImportType() {

    }

    //This method returns a JSON string containing an array of all Vehicle colours in the system.
    function epic_mwGetVehColour() {
        RequestQuote("epic_mwGetVehColour","")
    }

    //This method returns JSON data for a previously submitted quotation.
    function epic_mwGetQuotation() {

    }

    //This method converts a quotation to a policy by using the quotation_number supplied.
    function epic_mwConvertQuotation() {

    }


    var LoadANimation = "<div class=\"cssload-dots\">"
   + "<div class=\"cssload-dot\"></div>"
   + "<div class=\"cssload-dot\"></div>"
   + "<div class=\"cssload-dot\"></div>"
   + "<div class=\"cssload-dot\"></div>"
   + "<div class=\"cssload-dot\"></div>"
+ "</div>"
+ "<svg version=\"1.1\" xmlns=\"http://www.w3.org/2000/svg\">"
   + "<defs>"
       + "<filter id=\"goo\">"
           + "<feGaussianBlur in=\"SourceGraphic\" result=\"blur\" stdDeviation=\"12\" ></feGaussianBlur>"
           + "<feColorMatrix in=\"blur\" mode=\"matrix\" values=\"1 0 0 0 0	0 1 0 0 0	0 0 1 0 0	0 0 0 18 -7\" result=\"goo\" ></feColorMatrix>"
           + "<!--<feBlend in2=\"goo\" in=\"SourceGraphic\" result=\"mix\" ></feBlend>-->"
       + "</filter>"
   + "</defs>"
+ "</svg>"
+"<div class=\"ClearFix\"></div>"
    +"<div class=\"ClearFix\"></div>"
    +"<div class=\"ClearFix\"></div>"
    +"<div class=\"ClearFix\"></div>"
    +"<div class=\"ClearFix\"></div>"
    +"<div class=\"ClearFix\"></div>"
    +"<div class=\"ClearFix\"></div>";


    //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };

    function SubmitRequest() {
        
        var RequestMethodBody;

        SubmitQuote("epic_mwQuotationSubmit", JSON.stringify(MotorQuote));
    };

    


    function GetRequestKey() {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwSubmitQuotation",
            data: JSON.stringify(Logins),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessKey,
            error: ErrorKey
        });

    }

    function SuccessKey(response) {
        ReqKey = response[0].Response[0].key;
        GrabData();
    };

    function ErrorKey(response) {
        alert("Login Failed");
    }

    function SubmitQuote(Method, RequestBody) {
        $.ajax({
            type: "POST",
            url: "RequestQuotation/GetQresponse",
            data: "{}",
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessQuote,
            error: ErrorQuote
        });

    }
    
    //Quotation Main
    var Limits, PremiumCalculation, Extensions, MainHeader, ExtensionHeader, LimitsHeader, PremiumCalculationHeader, ManualRates;
    Limits = "";
    PremiumCalculation = "";
    Extensions = "";
    MainHeader = "";
    ExtensionHeader = "";
    LimitsHeader = "";
    ManualRates = "";
    PremiumCalculationHeader = "";
    //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
   



    function SuccessQuote(response) {
        //alert(response.premium_calculation.net_premium);

        //Quotation Number
        var QuotationHeader = "<p class=\"VaribaleLables\">Quotation#: </p><p>" + response.quotation_number + "</p>";


        //Extensions
        for (var i = 0; i < response.extensions.length; i++) {
            Extensions += "<hr><p class=\"VaribaleLables\">Code </p><p>" + response.extensions[i].code + "</p>"
                + "<p class=\"VaribaleLables\">Description </p><p>" + response.extensions[i].description + "</p>"
                + "<p class=\"VaribaleLables\">Heading </p><p>" + response.extensions[i].heading + "</p>"
                + "<p class=\"VaribaleLables\">Extension </p><p>" + response.extensions[i].type + "</p>"

        };

        //Limits
        var LimitsRow = "<div class=\"alert alert-warning\" role=\"alert\">%$#LIMITS&^%$</div>"; var LimitCount = 1;
        for (var l = 0; l < response.limits.length; l++) {
            Limits += LimitsRow.replace("%$#LIMITS&^%$", "#" + LimitCount + "<hr><p class=\"VaribaleLables\">Code </p><p>" + response.limits[l].code + "</p>"
                + "<p class=\"VaribaleLables\">Description </p><p>" + response.limits[l].description + "</p>"
                + "<p class=\"VaribaleLables\">Heading </p><p>" + response.limits[l].heading + "</p>"
                + "<p class=\"VaribaleLables\">Limit </p><p>" + response.limits[l].limit + "</p><hr>")
                LimitCount += 1;
        };
        LimitCount = 0;


        //Premium Calculation
        var PremiumRow = "<div class=\"alert alert-info\" role=\"alert\">%$#LIMITS&^%$</div>";
        PremiumCalculation += PremiumRow.replace("%$#LIMITS&^%$", "<p class=\"VaribaleLables\">Net Premium </p><p>" + response.premium_calculation.net_premium + "</p>"
                 + "<p class=\"VaribaleLables\">Stamp Duty </p><p>" + response.premium_calculation.stamp_duty + "</p>"
                 + "<p class=\"VaribaleLables\">Tax </p><p>" + response.premium_calculation.tax + "</p>"
                 + "<p class=\"VaribaleLables\">Total Premium </p><p>" + response.premium_calculation.total_premium + "</p>");

        MainHeader = QuotationHeader ;
        ExtensionHeader = Extensions;
        LimitsHeader =  Limits;
        PremiumCalculationHeader = PremiumCalculation;

        //Manual Rates
        //ManualRates +=


        var ClearFixes = "<div class=\"ClearFixes\"></div>";

        document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">" + MainHeader + "</div>" 
												+ ExtensionHeader + ClearFixes 
												+ LimitsHeader + ClearFixes
												+ PremiumCalculationHeader + ClearFixes + "<hr>";

    }


    function ErrorQuote(response) {
        //alert(response.statusText);
        document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">Failed to connect to UnderWriter (:</div>";
    }

    for (var i = 0; i < length; i++) {

    }

    function RequestQuote(Method, RequestBody) {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        if (RequestBody == "") { RequestBody = JSON.stringify(Logins) };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/" + Method,
            data: RequestBody,
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessMethod,
            error: ErrorMethod
        });

    }

   

    function SuccessMethod(response) {
        //Populates Colors on Motor Quote Page
        if (Task == "Color") {
            var ColorList = "<option>Select Vehicle Color</option>";
            var Options = "";
        for (var i = 0; i < response.data.length; i++) {
            Options = "<option>" + response.data[i] + "</option>";
            ColorList += Options;
        }
        document.getElementById("colour").innerHTML = ColorList;
                   
        }
       
        //alert(response.data);
      
    }

    function ErrorMethod(response) {
        //alert(response);
        document.getElementById("ResultSet").innerHTML = "<div class=\"alert alert-warning\" role=\"alert\">Failed to connect to UnderWriter (:</div>";
    }



    //Get Usages

    function RequestUsages() {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwGetUsages",
            data: JSON.stringify(Logins),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessUsages,
            error: ErrorUsages
        });

    }

    function SuccessUsages(response) {
        var ColorList = "<option>Select Usage Code</option>";
        var Options = "";
        for (var i = 0; i < response.data.length; i++) {
            Options = "<option>" + response.data[i].code + " <" + response.data[i].description + ">" + "</option>";
            ColorList += Options;
        }
        document.getElementById("usage_code").innerHTML = ColorList;
    }

    function ErrorUsages(response) {
        //alert(response.statusText);
    }


    //Get Import Types
    function RequestImportTypes() {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwGetVehImportType",
            data: JSON.stringify(Logins),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessImportTypes,
            error: ErrorImportTypes
        });

    }

    function SuccessImportTypes(response) {
        var ColorList = "<option>Select Import Type</option>";
        var Options = "";
        for (var i = 0; i < response.data.length; i++) {
            Options = "<option>" + response.data[i] + "</option>";
            ColorList += Options;
        }
        document.getElementById("import_type").innerHTML = ColorList;
    }

    function ErrorImportTypes(response) {
        //alert(response.statusText);
    }


    //Get Roof Types
    function RequestRoofTypes() {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwGetVehRoofType",
            data: JSON.stringify(Logins),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessRoofTypes,
            error: ErrorRoofTypes
        });

    }

    function SuccessRoofTypes(response) {
        var ColorList = "<option>Select Roof Type</option>";
        var Options = "";
        for (var i = 0; i < response.data.length; i++) {
            Options = "<option>" + response.data[i] + "</option>";
            ColorList += Options;
        }
        document.getElementById("roof_type").innerHTML = ColorList;
    }

    function ErrorRoofTypes(response) {
        //alert(response.statusText);
    }


    //Gets Transmission Types
    function RequestTransmissionTypes() {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwGetVehTransmissionType",
            data: JSON.stringify(Logins),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessTransmissionTypes,
            error: ErrorTransmissionTypes
        });

    }

    function SuccessTransmissionTypes(response) {
        var ColorList = "<option>Select Transmission Type</option>";
        var Options = "";
        for (var i = 0; i < response.data.length; i++) {
            Options = "<option>" + response.data[i] + "</option>";
            ColorList += Options;
        }
        document.getElementById("transmission_type").innerHTML = ColorList;
    }

    function ErrorTransmissionTypes(response) {
        //alert(response.statusText);
    }



    //Get Vehicle Models
    function RequestModels() {
        //var Logins = { "Login": { "username": getParameterByName("LoggedInUsername"), "password": getParameterByName("LoggedInPassword") } };
        $.ajax({
            type: "POST",
            url: "http://192.168.100.20:8089/RequestQuote/epic_mwGetVehMakeModels",
            data: JSON.stringify(Logins),
            contentType: "application/json; charset-utf-8",
            dataType: "json",
            success: SuccessModels,
            error: ErrorModels
        });

    }

    function SuccessModels(response) {

        var ColorList = "<option>Select Make</option>";
        var Options = "";
        var Models = "<option>Select Model</option>";
        var ModelType = "<option>Select Model Type</option>";
        var Extension = "<option>Select Extension</option>";
        var BodyType = "<option>Select Body Type</option>";
        var BodyShape = "<option>Select Body Shape</option>";

        for (var i = 0; i < response.data.length; i++) {
           
            if (response.data[i].make !== ""){ColorList += "<option>" + response.data[i].make + "</option>";}          

            for (var j = 0; j < response.data[i].models.length; j++) {

                if (response.data[i].models[j].model !== ""){Models += "<option>" + response.data[i].models[j].model + "</option>";}
                if (response.data[i].models[j].model_type !== ""){ ModelType += "<option>" + response.data[i].models[j].model_type + "</option>";}
                if (response.data[i].models[j].extension !== ""){Extension += "<option>" + response.data[i].models[j].extension + "</option>";}
                if (response.data[i].models[j].body_type !== ""){BodyType += "<option>" + response.data[i].models[j].body_type + "</option>";}
                if (response.data[i].models[j].body_shape !== ""){BodyShape += "<option>" + response.data[i].models[j].body_shape + "</option>";}
                
            }
            
            if (i != response.data.length) {
                Makes += "\"" + response.data[i].make + "\",";
            } else{
            Makes += "\"" + response.data[i].make + "\"";}
        }

        document.getElementById("make").innerHTML = ColorList;
        document.getElementById("model").innerHTML = Models;
        document.getElementById("model_type").innerHTML = ModelType;
        document.getElementById("extension").innerHTML = Extension;
        document.getElementById("body_type").innerHTML = BodyType;
        document.getElementById("body_shape").innerHTML = BodyShape;

        Makes += "]";

    };
   
    //##############################################
   

    //##############################################


    function ErrorModels(response) {
        //alert(response.statusText);
    }

    
    //Handles process completed for the wizard
    function ClickButton(Button) {
        var ProgressBar = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"2\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em; width: 2%;\">2%</div>";

        if (Button == 1) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"40\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 40em; width: 2%;\">40% Complete</div>";
        } else if (Button == 2) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"55\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 55em; width: 2%;\">55% Complete</div>";
        } else if (Button == 3) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"70\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 70em; width: 2%;\">70% Complete</div>";
        } else if (Button == 4) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"85\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 85em; width: 2%;\">85% Complete</div>";
        } else if (Button == 5) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"100\" aria-valuemin=\"0\" aria-valuemax=\"100\" style=\"min-width: 2em; width: 100%;\">100% Complete. Finised!</div>";
        } else if (Button == 0) {
            document.getElementById("dp").innerHTML = "<div class=\"progress-bar\" role=\"progressbar\" aria-valuenow=\"25\" aria-valuemin=\"0\" aria-valuemax=\"25\" style=\"min-width: 2em; width: 25%;\">25% Complete</div>";
        }
        window.location.href = "#"
    };

    var MotorQuote;
    function GrabData() {
        //Quotation Request
        
        var quotation_number = 14;
        var policy_prefix = "MPCC";
        var first_name = document.getElementById("first_name").value;
        var middle_name = document.getElementById("middle_name").value;
        var last_name = document.getElementById("last_name").value;
        var national_id = document.getElementById("national_id").value;
        var national_id_type = document.getElementById("national_id_type").value;
        var start_date = document.getElementById("start_date").value;
        var accidents_in_last_few_years = document.getElementById("accidents_in_last_few_years").value;
        var occupation = document.getElementById("occupation").value;
        var currency = document.getElementById("currency").value;
        var is_motor_cat_perils_covered = document.getElementById("is_motor_cat_perils_covered").value;

        //Phone Numbers
        var type = document.getElementById("type").value;
        var carrier = document.getElementById("carrier").value;
        var description = document.getElementById("description").value;
        var extension_or_range = document.getElementById("extension_or_range").value;
        var phone_number = document.getElementById("phone_number").value;
        var email_address = document.getElementById("email_address").value;
        var company_name = document.getElementById("company_name").value;

        //Address
        var aaltitude = document.getElementById("aaltitude").value;
        var ablock = document.getElementById("ablock").value;
        var abuilding_number = document.getElementById("abuilding_number").value;
        var acomplex_name = document.getElementById("acomplex_name").value;
        var acountry = document.getElementById("acountry").value;
        var ageneral_area = document.getElementById("ageneral_area").value;
        var ainternational_area = document.getElementById("ainternational_area").value;
        var ais_international_address = document.getElementById("ais_international_address").value;
        var aisland = document.getElementById("aisland").value;
        var alatitude = document.getElementById("alatitude").value;
        var alongitude = document.getElementById("alongitude").value;
        var aparish = document.getElementById("aparish").value;
        var apostal_zip_code = document.getElementById("apostal_zip_code").value;
        var astreet_name = document.getElementById("astreet_name").value;
        var astreet_number = document.getElementById("astreet_number").value;
        var astreet_type = document.getElementById("astreet_type").value;
        var atown = document.getElementById("atown").value;
        var aunit_number = document.getElementById("aunit_number").value;

        //Risks
        var sum_insured = document.getElementById("sum_insured").value;
        var usage_code = document.getElementById("usage_code").value;
        var authorized_driver_wording = document.getElementById("authorized_driver_wording").value;
        var year_for_rating = document.getElementById("year_for_rating").value;
        var year = document.getElementById("year").value;
        var make = document.getElementById("make").value;
        var model = document.getElementById("model").value;
        var model_type = document.getElementById("model_type").value;
        var extension = document.getElementById("extension").value;
        var body_type = document.getElementById("body_type").value;
        var body_shape = document.getElementById("body_shape").value;
        var hp_cc_rating = document.getElementById("hp_cc_rating").value;
        var hp_cc_unit_type = document.getElementById("hp_cc_unit_type").value;
        //var cert_type = document.getElementById("cert_type").value;
        var chassis_number = document.getElementById("chassis_number").value;
        var colour = document.getElementById("colour").value;
        var number_of_cyclinders = document.getElementById("number_of_cyclinders").value;
        var engine_modified = document.getElementById("engine_modified").value;
        var engine_number = document.getElementById("engine_number").value;
        var engine_type = document.getElementById("engine_type").value;
        var has_electric_doors = document.getElementById("has_electric_doors").value;
        var has_electric_side_mirror = document.getElementById("has_electric_side_mirror").value;
        var has_electric_window = document.getElementById("has_electric_window").value;
        var has_power_steering = document.getElementById("has_power_steering").value;
        var import_type = document.getElementById("import_type").value;
        var ncd_percent = document.getElementById("ncd_percent").value;
        var left_hand_drive = document.getElementById("left_hand_drive").value;
        var mileage = document.getElementById("mileage").value;
        var mileage_type = document.getElementById("mileage_type").value;
        var number_of_doors = document.getElementById("number_of_doors").value;
        var number_of_engines = document.getElementById("number_of_engines").value;
        var registration_number = document.getElementById("registration_number").value;
        var roof_type = document.getElementById("roof_type").value;
        var seat_type = document.getElementById("seat_type").value;
        var seating = document.getElementById("seating").value;
        var tonnage = document.getElementById("tonnage").value;
        var transmission_type = document.getElementById("transmission_type").value;

        //Vehicle Location

        var altitude = document.getElementById("altitude").value;
        var block = document.getElementById("block").value;
        var building_number = document.getElementById("building_number").value;
        var complex_name = document.getElementById("complex_name").value;
        var country = document.getElementById("country").value;
        var general_area = document.getElementById("general_area").value;
        var international_area = document.getElementById("international_area").value;
        var is_international_address = document.getElementById("is_international_address").value;
        var island = document.getElementById("island").value;
        var latitude = document.getElementById("latitude").value;
        var longitude = document.getElementById("longitude").value;
        var parish = document.getElementById("parish").value;
        var postal_zip_code = document.getElementById("postal_zip_code").value;
        var street_name = document.getElementById("street_name").value;
        var street_number = document.getElementById("street_number").value;
        var street_type = document.getElementById("street_type").value;
        var town = document.getElementById("town").value;
        var unit_number = document.getElementById("unit_number").value;

        //Drivers

        var dfirst_name = document.getElementById("dfirst_name").value;
        var dmiddle_name = document.getElementById("dmiddle_name").value;
        var dlast_name = document.getElementById("dlast_name").value;
        var driver_dob = document.getElementById("driver_dob").value;
        var drivers_licence_first_issued = document.getElementById("drivers_licence_first_issued").value;
        var drivers_licence_number = document.getElementById("drivers_licence_number").value;
        var is_main_driver = document.getElementById("is_main_driver").value;

        //Manual Rates
        var code = document.getElementById("code").value;
        var value = document.getElementById("value").value;

        MotorQuote = {
            "resquestkey" : {"key" : ReqKey},
            "quotation": {
                "quotation_number": 0,
                "policy_prefix": policy_prefix,
                "first_name": first_name,
                "middle_name": middle_name,
                "last_name": last_name,
                "national_id": national_id,
                "national_id_type": national_id_type,
                "phone_numbers": [
                    {
                        "type": type,
                        "carrier": carrier,
                        "description": description,
                        "extension_or_range": extension_or_range,
                        "phone_number": phone_number
                    }
                ],
                "email_address": email_address,
                "company_name": company_name,
                "address": {
                    "altitude": aaltitude,
                    "block": ablock,
                    "building_number": abuilding_number,
                    "complex_name": acomplex_name,
                    "country": acountry,
                    "general_area": ageneral_area,
                    "international_area": ainternational_area,
                    "is_international_address": ais_international_address,
                    "island": aisland,
                    "latitude": alatitude,
                    "longitude": alongitude,
                    "parish": aparish,
                    "postal_zip_code": apostal_zip_code,
                    "street_name": astreet_name,
                    "street_number": astreet_number,
                    "street_type": astreet_type,
                    "town": atown,
                    "unit_number": aunit_number
                },
                "start_date": start_date,
                "accidents_in_last_few_years": accidents_in_last_few_years,
                "occupation": occupation,
                "currency": currency,
                "is_motor_cat_perils_covered": is_motor_cat_perils_covered
            },
            "risk": [
                {
                    "sum_insured": sum_insured,
                    "usage_code": usage_code,
                    "authorized_driver_wording": authorized_driver_wording,
                    "year_for_rating": year_for_rating,
                    "year": year,
                    "make": make,
                    "model": model,
                    "model_type": model_type,
                    "extension": extension,
                    "body_type": body_type,
                    "body_shape": body_shape,
                    "hp_cc_rating": hp_cc_rating,
                    "hp_cc_unit_type": hp_cc_unit_type,
                    "cert_type": "",
                    "chassis_number": chassis_number,
                    "colour": colour,
                    "number_of_cyclinders": number_of_cyclinders,
                    "engine_modified": engine_modified,
                    "engine_number": engine_number,
                    "engine_type": engine_type,
                    "has_electric_doors": has_electric_doors,
                    "has_electric_side_mirror": has_electric_side_mirror,
                    "has_electric_window": has_electric_window,
                    "has_power_steering": has_power_steering,
                    "import_type": import_type,
                    "ncd_percent": ncd_percent,
                    "left_hand_drive": left_hand_drive,
                    "mileage": mileage,
                    "mileage_type": mileage_type,
                    "number_of_doors": number_of_doors,
                    "number_of_engines": number_of_engines,
                    "registration_number": registration_number,
                    "roof_type": roof_type,
                    "seat_type": seat_type,
                    "seating": seating,
                    "tonnage": tonnage,
                    "transmission_type": transmission_type,
                    "vehicle_location": {
                        "altitude": altitude,
                        "block": block,
                        "building_number": building_number,
                        "country": country,
                        "general_area": general_area,
                        "international_area": international_area,
                        "is_international_address": is_international_address,
                        "island": island,
                        "latitude": latitude,
                        "longitude": longitude,
                        "parish": parish,
                        "postal_zip_code": postal_zip_code,
                        "street_name": street_name,
                        "street_number": street_number,
                        "street_type": street_type,
                        "town": town,
                        "unit_number": unit_number
                    },
                    "drivers": [
                        {
                            "first_name": dfirst_name,
                            "middle_name": dmiddle_name,
                            "last_name": dlast_name,
                            "driver_dob": driver_dob,
                            "drivers_licence_first_issued": drivers_licence_first_issued,
                            "drivers_licence_number": drivers_licence_number,
                            "is_main_driver": is_main_driver
                        }
                    ],
                    "manual_rates": [{
                        "code": code,
                        "value":value
                    }]
                }
            ]
        };

        //alert(JSON.stringify(MotorQuote));
        document.getElementById("ResultSet").innerHTML = LoadANimation;
        SubmitQuote("epic_mwSubmitQuotation", JSON.stringify(MotorQuote));

    };

    //Gets QueryStrings
    function getParameterByName(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
        //alert(results);

        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));

    };
   
})();


SubmitQuote(null, null);